<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Repository\SalesInvoiceRepository;

class DataControlSalesInvoice extends Command 
{
    protected static $defaultName = 'app:data-control-salesInvoice:send';

    private $salesInvoiceRepository;

    public function __construct(SalesInvoiceRepository $salesInvoiceRepository)
    {
        parent::__construct(null);
        $this->salesInvoiceRepository = $salesInvoiceRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Send daily reports Data Control sales invoice')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
       
        $io = new SymfonyStyle($input, $output);
        $salesInvoiceOutputs = $this->salesInvoiceRepository->calculateAmountDuSalesInvoice();
       
        $io->progressStart(1);
 
        $to = 'rbouasker@advences.com,ssouissi@advences.com';
        //$to = 'amal@advences.com';        
        $subject = "ERP - CRON contrôle sales_invoice";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= "From: no-reply@advences.io" . "\r\n";
        $headers .= "Bcc: amal@advences.com" . "\r\n";
  
    $message = '
    <html>
    <head>
    <title>Data Control</title>
    </head>
    <body>
    <p><b>Check Point n°1 : Reste dû des factures</b></p>
    <table style="table-layout:fixed; word-wrap:break-word;">
    <tr>
    <th style="width:120px;text-align:left;">Tenant</th>
    <th style="width:140px;text-align:left;">Client</th>
    <th style="width:100px;text-align:left;">Facture n°</th>
    <th style="width:120px;text-align:left;">Montant TTC</th>
    <th style="width:120px;text-align:left;">Reste dû calculé</th>
    <th style="width:30px;text-align:left;">Contrôle</th>
    </tr>';

       

        foreach($salesInvoiceOutputs as $salesInvoice) {
        

       
            $nameTenant = $salesInvoice['tenant_name'];
            $nameCustomer = $salesInvoice['customer_name'];
            $invoiceNumber = $salesInvoice['number'];
            $totalAmountInclTax = $salesInvoice['total_amount_incl_tax'];
            $amountDueCalculate = $salesInvoice['amount_due_calculate'];
            $remainingAmountDue = $salesInvoice['remaining_amount_due'];
            $currencyCode = $salesInvoice['currency_code'];
           
    
            if($amountDueCalculate != $remainingAmountDue){
                $controle ='KO';
                $ctrl = '<td style="color:red;">KO</td>';
            }else{
                $controle ='OK';
                $ctrl = '<td style="color:green;">OK</td>'; 
            }
            
            if ( $controle == 'KO'){
            $message .= '<tr><td>'.$nameTenant.'</td>';
            $message .= '<td>'.$nameCustomer.'</td>';
            $message .= '<td>'.$invoiceNumber.'</td>';
            $message .= '<td style="text-align:right;">'.number_format($totalAmountInclTax, 2, '.', ' ').' '.$currencyCode.'</td>';
            $message .= '<td style="text-align:right;">'.number_format($amountDueCalculate, 2, '.', ' ').' '.$currencyCode.'</td>';
            $message .= $ctrl.'</tr>';
            }
           
        }

         $message .= '</table></body></html>';
       
        mail($to,$subject,$message,$headers);

        $io->progressFinish();
        $io->success('Daily reports were sent successfully!');
        return 0;
    }
}
