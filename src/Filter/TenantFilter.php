<?php


namespace App\Filter;

use App\Annotation\TenantAware;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;
use Doctrine\Common\Annotations\Reader;


final class TenantFilter extends SQLFilter
{
    protected $reader;

    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        if (null === $this->reader) {
            throw new \RuntimeException(sprintf('An annotation reader must be provided.Be sure to call "%s::setAnnotationReader()".', __CLASS__));
        }

        // The Doctrine filter is called for any query on any entity
        // Check if the current entity is "tenant aware" (marked with an annotation)
        $tenantAware = $this->reader->getClassAnnotation($targetEntity->getReflectionClass(), TenantAware::class); //'App\\Annotation\\TenantAware'
        if (!$tenantAware) {
            return '';
        }

        $fieldName = $tenantAware->tenantFieldName;
        try {
            // Don't worry, getParameter automatically escapes parameters
            $tenantCode = $this->getParameter('code');
        } catch (\InvalidArgumentException $e) {
            // No tenant code has been defined
            return '';
        }
        try {
            $tenantMaster = $this->getParameter('masterTenant');
        } catch (\InvalidArgumentException $e) {
            // No Master Tenant code has been defined
            $tenantMaster = 'xxx';
        }

        if (empty($fieldName) || empty($tenantCode)) {
            return '';
        }
        
        if($tenantMaster == 'xxx'){ 
            return sprintf('%s.%s = %s', $targetTableAlias, $fieldName, $tenantCode);
        }else{ 
            return sprintf('%s.%s = %s OR %s.%s = %s', $targetTableAlias, $fieldName, $tenantCode, $targetTableAlias, $fieldName, $tenantMaster);
        }
        
    }

    public function setAnnotationReader(Reader $reader): void
    {
        $this->reader = $reader;
    }
}
