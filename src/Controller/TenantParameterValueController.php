<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TenantParameterValueRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class TenantParameterValueController extends AbstractController
{
    private $tenantRepo;
    private $tokenStorage;
    private $jwtManager;

    public function __construct(TenantParameterValueRepository $tenantMpvRepo, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tenantRepo = $tenantMpvRepo;
        $this->tokenStorage = $tokenStorage;
        $this->jwtManager = $jwtManager;
    }
    /**
     * @Route("/api/min_tenant_parameter_values", name="min_tenant_parameter_value")
     */
    public function getMinTenantMasterParameterValue(Request $request)
    {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorage->getToken()); 
        $tenant = $decodedJwtToken['tenant'];
        $parameter = $request->get('parameter');
        $entity = $request->get('entity');
        $locale = $request->get('locale');   
        $response = $this->tenantRepo->getMinTenantMasterParameterValue($tenant, $parameter, $entity, $locale);

        return new JsonResponse($response);
    }
}
