<?php
namespace App\Controller;


use App\Repository\ThirdPartyRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;


class ThirdPartyController extends ApiController
{

    /**
     * @Route("api/min_third_parties", name="min_third_parties")
     */
    public function getMinThirdParties(Request $request, ThirdPartyRepository $thirdPartyRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;
        if($request->get('type_code') == null){
            $type_code = null;
        }else{
            $type_code= $request->get('type_code');
        }
		
		if($request->get('name') == null){
            $name = null;
        }else{
            $name = $request->get('name');
        }

        if($request->get('first_name') == null){
            $first_name = null;
        }else{
            $first_name = $request->get('first_name');
        }
		
        $decodedJwtToken = $jwtManager->decode($tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
	    $response = $thirdPartyRepository->getMinThirdParties($type_code, $name, $tenant_code,$first_name);
        return new JsonResponse($response);
    }

 /**
     * @Route("api/dash_nb_record", name="dash_nb_record")
     */
    public function getDashNbRecord(Request $request, ThirdPartyRepository $thirdPartyRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;
        $decodedJwtToken = $jwtManager->decode($tokenStorage->getToken()); 
	    $response = $thirdPartyRepository->getDashNbRecord();
        return new JsonResponse($response);
    }
    
    /**
     * @Route("api/calculate_customer_balance", name="calculate_customer_balance")
     */
    public function calculateCustomerBalance(Request $request, ThirdPartyRepository $thirdPartyRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;
        if($request->get('customer_id') == null){
            $customer_id = null;
        }else{
            $customer_id= $request->get('customer_id');
        }
        $decodedJwtToken = $jwtManager->decode($tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
	    $response = $thirdPartyRepository->calculateCustomerBalance($customer_id,$tenant_code);
        return new JsonResponse($response);
    }
}
