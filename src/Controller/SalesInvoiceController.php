<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Manager\SalesInvoiceManager;
use App\Entity\SalesInvoice;
use App\Repository\CurrencyRepository;
use App\Repository\TenantRepository;
use App\Repository\SalesInvoiceRepository;
use App\Repository\ThirdPartyRepository;
use Symfony\Component\HttpFoundation\Response ;
use App\Services\TokenInformation;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class SalesInvoiceController extends AbstractController
{
    private $currencyRepo;
    private $bankAccountRepo;
    private $tenantRepo;
    private $SalesInvoiceRepo;
    private $thirdPartyRepository;
    public function __construct(ThirdPartyRepository $thirdRepo,CurrencyRepository $currencyRepository, TenantRepository $tenantRepository, SalesInvoiceRepository $SalesInvoiceRepo)
    {
        $this->currencyRepo = $currencyRepository;
        $this->tenantRepo = $tenantRepository;
        $this->SalesInvoiceRepo = $SalesInvoiceRepo;
        $this->thirdPartyRepository = $thirdRepo;
    }
   
     
    /**
     * @Route("api/reporting", name="reporting")
     */
    public function getReporting(Request $request, SalesInvoiceRepository $SalesInvoiceRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;

        $decodedJwtToken = $jwtManager->decode($this->tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
        $response = $SalesInvoiceRepository->getReporting($tenant_code);
        return new JsonResponse($response);

    }
    
    /**
     * @Route("api/dash_turn_over", name="dash_turn_over")
     */
    public function getReportingCA(Request $request, SalesInvoiceRepository $SalesInvoiceRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;

        $decodedJwtToken = $jwtManager->decode($this->tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
        $response = $SalesInvoiceRepository->getCAByYearByMonth($tenant_code);
        return new JsonResponse($response);

    }
	
    /**
     * @Route("api/dash_unpaid_sales_invoice", name="dash_unpaid_sales_invoice")
     */
    public function getUnpaidSale(Request $request, SalesInvoiceRepository $SalesInvoiceRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;

        $decodedJwtToken = $jwtManager->decode($this->tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
        $response = $SalesInvoiceRepository->getUnpaid($tenant_code);
        return new JsonResponse($response);

    }
	
	/**
     * @Route("api/min_sales_invoices", name="min_salesinvoices")
     */
    public function getMinSalesInvoices(Request $request, SalesInvoiceRepository $SalesInvoiceRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;
		if($request->get('number') == null){
            $number = null;
        }else{
            $number = $request->get('number');
        }
		if($request->get('third_id') == null){
            $third_id = null;
        }else{
            $third_id = $request->get('third_id');
        }
		
        $decodedJwtToken = $jwtManager->decode($tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
	    $response = $SalesInvoiceRepository->getMinSalesInvoices($number,$third_id,$tenant_code);
        return new JsonResponse($response);
    }
}
