<?php


namespace App\Controller;
use App\Entity\MasterParameterValue;
use App\Repository\CurrencyRepository;
use App\Repository\TenantPreferenceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class CurrencyController
{
    private $currencyRepo;
    private $tenantPreference;

    public function __construct(CurrencyRepository $CurrencyRepository, TenantPreferenceRepository $tenantPreferenceRepository)
    {
        $this->currencyRepo = $CurrencyRepository;
        $this->tenantPreference = $tenantPreferenceRepository;

    }

    public function __invoke(Request $request)
    {
        $preference = $this->tenantPreference->findBy(["type" => "currency", "entity" => "currency", "tenant" => $request->get("tenant")], ["rank" => "asc"]);
        $responseCurrency = $this->currencyRepo->findBy([]);

        $response = [];
        if (count($preference) != 0) {
            foreach ($responseCurrency as $valtmp) {
                foreach ($preference as $valPref) {
                    if ($valtmp->getCode() == $valPref->getObject()) {
                        $response[] = $valtmp;
                    }
                }
            }
        }else{
            $response = $responseCurrency;
        }
        return $response;


    }
}