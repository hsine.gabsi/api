<?php
namespace App\Controller;


use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;


class ProductController extends ApiController
{

    /**
     * @Route("api/min_products", name="min_products")
     */
    public function getMinProducts(Request $request, ProductRepository $productRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;
        if($request->get('language_code') == null){
            $language_code = 'null';
        }else{
            $language_code= $request->get('language_code');
        }
		if($request->get('type_code') == null){
            $type_code = null;
        }else{
            $type_code= $request->get('type_code');
        }
		
		if($request->get('name') == null){
            $name = null;
        }else{
            $name = $request->get('name');
        }
		
        $decodedJwtToken = $jwtManager->decode($tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
	    $response = $productRepository->getMinProducts($language_code, $type_code, $name, $tenant_code);
        return new JsonResponse($response);
    }
}
