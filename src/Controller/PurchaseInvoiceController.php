<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Manager\PurchaseInvoiceManager;
use App\Entity\PurchaseInvoice;
use App\Repository\CurrencyRepository;
use App\Repository\TenantRepository;
use App\Repository\PurchaseInvoiceRepository;
use App\Repository\ThirdPartyRepository;
use Symfony\Component\HttpFoundation\Response ;
use App\Services\TokenInformation;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class PurchaseInvoiceController extends AbstractController
{
    /**
     * @Route("api/dash_to_be_paid", name="dash_to_be_paid")
     */
    public function gettobePaid(Request $request, PurchaseInvoiceRepository $PurchaseInvoiceRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;

        $decodedJwtToken = $jwtManager->decode($this->tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
        $response = $PurchaseInvoiceRepository->gettoBepaid($tenant_code);
        return new JsonResponse($response);

    }
	
}
