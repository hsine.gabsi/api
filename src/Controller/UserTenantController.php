<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\UserTenantRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserTenantController extends AbstractController
{
    /**
     * @Route("/api/min_user_tenants", name="tenant_users")
     */
    public function getMinUserTenants(Request $request, UserTenantRepository $userTenantRepository)
    {
        if($request->get('user') == null){
            $user = 'null';
        }else{
            $user= $request->get('user');
        }
        $response = $userTenantRepository->findBy(['user' => $user]);
        $rs = [];
        for($i = 0; $i < count($response); $i++){
            $rs[$i]['code'] = $response[$i]->getTenant()->getCode();
            $rs[$i]['name'] = $response[$i]->getTenant()->getName();
        }       
        
        return new JsonResponse($rs);
    }
}
