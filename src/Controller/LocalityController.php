<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response ;
use App\Repository\LocalityRepository;


class LocalityController extends AbstractController
{
    // /**
    //  * @Route("/api/country_calling_codes", name="country_calling_codes")
    //  */
    // public function getCallingCode(LocalityRepository $localityRepository)
    // {
    //     //$response = $localityRepository->findBy(["type" => "country"]);
    //     $response = $localityRepository->findCallingCode();
    //     dd($response);

    //     $response = $productManager->productSearch($request);
    //     return new JsonResponse($response, Response::HTTP_OK, [], true);
    // }

    /**
     * @Route("/api/min_localities", name="min_localities")
     */
    public function getMinLocality(Request $request,LocalityRepository $localityRepository)
    {
        if($request->get('language_code') == null){
            $language_code = 'null';
        }else{
            $language_code= $request->get('language_code');
        }
        if($request->get('name') == null){
            $name = null;
        }else{
            $name = $request->get('name');
        }
        if($request->get('type_codes') == null){
            $type_codes = null;
        }else{
            $type_codes = $request->get('type_codes');
        }
        if($request->get('tenant_code') == null){
            $tenant_code = 'null';
        }else{
            $tenant_code = $request->get('tenant_code');
        }
        //$response = $localityRepository->findBy(["type" => "country"]);
        $response = $localityRepository->getMinLocality($language_code,$name,$type_codes,$tenant_code);

        return new JsonResponse($response);
    }

    /**
     * @Route("/api/country_calling_code", name="calling_code")
     */
    public function getCountryCalling(Request $request, LocalityRepository $localityRepository)
    {
        if($request->get('locale') == null){
            $locale = "fr";
        }else{
            $locale = $request->get('locale');
        }
        $response = $localityRepository->countryCalling($locale);
        return new JsonResponse($response);
    }

}
