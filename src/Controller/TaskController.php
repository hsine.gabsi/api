<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\TaskRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use App\Repository\ObjectMemberRepository;
/**
 * Class TaskController
 * @package App\Controller
 */
class TaskController extends ApiController
{
    private $task;
    public function __construct(TaskRepository $taskRepository)
    {
        $this->task = $taskRepository;
    }
 
    /**
     * @Route("/api/min_tasks", name="min_tasks")
     */
    public function getMinTasks(Request $request,TaskRepository $taskRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        //$this->tokenStorage = $tokenStorage;
	   if($request->get('type_codes') == null){
            $type_codes = 'null';
        }else{
            $type_codes = $request->get('type_codes');
        }
        if($request->get('business_unit_code') == null){
            $business_unit_code = 'null';
        }else{
            $business_unit_code = $request->get('business_unit_code');
        }
		if($request->get('parent_id') == null){
            $parent_id = 'null';
        }else{
            $parent_id = $request->get('parent_id');
        }
		if($request->get('language_code') == null){
            $language_code = 'null';
        }else{
            $language_code= $request->get('language_code');
        }
		($request->get('name') == null)?$name=null:$name=$request->get('name');
        //$tenant_code = $this->tokenStorage->getToken()->getUser()->getTenant()->getCode();
        $decodedJwtToken = $jwtManager->decode($tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
        //dd($tenant_code);        
        $response = $taskRepository->getMinTasks($type_codes, $business_unit_code, $tenant_code, $parent_id, $language_code,$name);

        return new JsonResponse($response);
    }

    public function __invoke(Request $request,ObjectMemberRepository $objectMemberRepository)
    { 
        if($request->get('id') == null){
            $id = null;
        }else{
            $id= $request->get('id');
        }

        if($request->get('type') == null){
            $type = null;
        }else{
            $type= $request->get('type');
        }

        if($request->get('businessUnit') == null){
            $businessUnit = null;
        }else{
            $businessUnit= $request->get('businessUnit');
        }

        if($request->get('status') == null){
            $status = null;
        }else{
            $status= $request->get('status');
        }

        if($request->get('name') == null){
            $name = null;
        }else{
            $name= $request->get('name');
        }

        if($request->get('number') == null){
            $number = null;
        }else{
            $number= $request->get('number');
        }

        if($request->get('requester') == null){
            $requester = null;
        }else{
            $requester= $request->get('requester');
        }

        if($request->get('responsible') == null){
            $responsible = null;
        }else{
            $responsible= $request->get('responsible');
        }
		if($request->get('tag') == null){
            $tag = null;
        }else{
            $tag= $request->get('tag');
        }
		if($request->get('parent') == null){
            $parent = null;
        }else{
            $parent= $request->get('parent');
        }
		
		if($request->get('queue') == null){
            $queue = null;
        }else{
            $queue= $request->get('queue');
        }
        if($request->get('thirdParty') == null){
            $thirdParty = null;
        }else{
            $thirdParty= $request->get('thirdParty');
        }
        if($request->get('page') == null){
            $page = 1;
        }else{
            $page= $request->get('page');
        }

        $response = $this->task->getTaskBy($id, $type, $businessUnit, $status, $name, $number, $requester, $responsible,$tag,$parent, $queue,$thirdParty, $page);
        /*$listTasks=(array)$response;
        for($i=0;$i<count($listTasks["hydra:member"]);$i++){
            $members=[];
            $param=['objectId' => $listTasks["hydra:member"][$i]->getId(),'role'=>'responsible'];
        $members= $objectMemberRepository->findBy($param);
        if(count($members)>0){
        $member[0]=array("name"=>$members[0]->getUser()->getFirstName()." ".$members[0]->getUser()->getLastName(),"id"=>$members[0]->getUser()->getId());
            }
        $listTasks["hydra:member"][$i]->responsable=($member);
        }$response=$listTasks;*/
        return $response;
    }

    
    /**
     * @Route("/api/dash_my_task", name="dash_my_task")
     */
    public function getUserTask(Request $request,TaskRepository $taskRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage; 

		if($request->get('user_id') == null){
            $user_id = null;
        }else{
            $user_id = $request->get('user_id');
        }

        $decodedJwtToken = $jwtManager->decode($this->tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
        $response = $taskRepository->getuserTask( $user_id,$tenant_code);
        return new JsonResponse($response);
    }

}





