<?php
namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class ApiController.
 */
class ApiController extends AbstractController
{
    /**
     * Get a user from entity using user stored in the Security Token Storage.
     *
     * @return User|mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @final since version 3.4
     */
    protected function getUser()
    {
        $user = parent::getUser();

        if ($user && $user instanceof UserInterface) {
            $user = $this->getDoctrine()->getRepository(User::class)->find($user->getId());
        }

        return $user;
    }
}
