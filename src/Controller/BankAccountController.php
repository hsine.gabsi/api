<?php
namespace App\Controller;


use App\Repository\BankAccountRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;


class BankAccountController extends ApiController
{

    /**
     * @Route("api/min_bank_accounts", name="min_bank_accounts")
     */
    public function getMinBankAccounts(Request $request, BankAccountRepository $bankAccountRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;
		if($request->get('name') == null){
            $name = null;
        }else{
            $name = $request->get('name');
        }
		/*if($request->get('thirdParty') == null){
			$thirdParty = null;
		}else{
			$thirdParty = $request->get('thirdParty');
		}*/

        $decodedJwtToken = $jwtManager->decode($tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant']; 
	    $response = $bankAccountRepository->getMinBankAccounts($name, $tenant_code);//$thirdParty, 
        return new JsonResponse($response);
    }
}
