<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\SalesInvoicePaymentRepository;
use App\Repository\PurchaseInvoicePaymentRepository;
use Symfony\Component\HttpFoundation\Response ;
use App\Services\TokenInformation;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class SalesInvoicePaymentController extends AbstractController
{

	/**
     * @Route("/api/amountDueCalculateUpdate", name="amountDueCalculateUpdate")
     */
    public function amountDueCalculateUpdate(Request $request, SalesInvoicePaymentRepository $SalesInvoicePaymentRepository, PurchaseInvoicePaymentRepository $PurchaseInvoicePaymentRepository, TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;
		if($request->get('payment') == null){
            $paymentId = null;
        }else{
            $paymentId = $request->get('payment');
        }
        if($request->get('invoice') == null){
            $invoiceId = null;
        }else{
            $invoiceId = $request->get('invoice');
        }
        if($request->get('amount') == null){
            $amount = null;
        }else{
            $amount = $request->get('amount');
        }
        if($request->get('inbound') == null){
            $inbound = null;
        }else{
            $inbound = $request->get('inbound');
        }				
        $decodedJwtToken = $jwtManager->decode($tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant'];
        if($inbound == 'false' || $inbound == false){
            $response = $PurchaseInvoicePaymentRepository->amountDueCalculateUpdate($paymentId,$invoiceId,$amount,$tenant_code);
        }else{            
            $response = $SalesInvoicePaymentRepository->amountDueCalculateUpdate($paymentId,$invoiceId,$amount,$tenant_code);
        }	    
        return new JsonResponse($response);
    }
}
