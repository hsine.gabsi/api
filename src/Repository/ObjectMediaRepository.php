<?php

namespace App\Repository;

use App\Entity\ObjectMedia;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectMedia|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectMedia|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectMedia[]    findAll()
 * @method ObjectMedia[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectMediaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectMedia::class);
    }
    
    public function findByObjectID($objectId, $languageCode = 'fr', $entityCode = '', $tenantCode = '')
    {       
        $sql = "SELECT 	t1.id,
                        t1.rank,
                        t1.list,
                        t1.panoramic,
                        t1.gallery,
                        t1.url,
                        t2.text
                FROM object_media t1
                    LEFT JOIN object_seo_content t2 ON ( t2.object_Id = t1.object_Id and t2.type_code = 'alt_tag' and t2.locale='".$languageCode."' )
                WHERE t1.object_Id = ".$objectId
                . " and t1.tenant_code = '".$tenantCode."'"
                . " and t1.entity_code = '".$entityCode."'"
                ;

        $query = $this->getEntityManager()->getConnection()->query($sql);       
        $result = $query->fetchAll();  
        return $result;
        
    }

    // /**
    //  * @return ObjectMedia[] Returns an array of ObjectMedia objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectMedia
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
