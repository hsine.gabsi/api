<?php

namespace App\Repository;

use App\Entity\ObjectTask;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectTask|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectTask|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectTask[]    findAll()
 * @method ObjectTask[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectTaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectTask::class);
    }

    // /**
    //  * @return ObjectTask[] Returns an array of ObjectTask objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectTask
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
