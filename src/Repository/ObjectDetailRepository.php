<?php

namespace App\Repository;

use App\Entity\ObjectDetail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectDetail|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectDetail|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectDetail[]    findAll()
 * @method ObjectDetail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectDetailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectDetail::class);
    }
    
    public function findByObjectID($objectId, $languageCode = 'fr', $entityCode = '')
    {
        return $this->createQueryBuilder('t1')
            ->andWhere('t1.objectId = :objectId')
            ->setParameter('objectId', $objectId)
            ->andWhere('t1.locale = :_languageCode')
            ->setParameter('_languageCode', $languageCode)    
            ->andWhere('t1.entity = :_entityCode')
            ->setParameter('_entityCode', $entityCode)        
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return ObjectDetail[] Returns an array of ObjectDetail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectDetail
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
