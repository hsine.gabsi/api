<?php

namespace App\Repository;

use App\Entity\Task;
use App\Entity\Tenant;
use App\Entity\TenantParameterValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;


/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function getTaskBy($id=null, $type=null, $businessUnit=null, $status=null, $name=null, $number=null, $requester=null, $responsible=null,$tag=null, $parent=null, $queue=null,$thirdParty=null, $page=1){
        $queryBuilder = $this->createQueryBuilder('t');
        //->select("t,(select tg.id from App\Entity\ObjectTag tg where t.id = tg.objectId group by tg.objectId) as tags");
        /*->from('App\Entity\ObjectMember','m')
        ->where("m.objectId=t.id");*/
       
        if ($status !== null) {
            if($status == 'not_closed'){
                $queryBuilder->andWhere('t.status = :status1 OR t.status = :status2 OR t.status = :status3 OR t.status = :status4')
                ->setParameters(array('status1' => 'not_started', 'status2' => 'in_progress',  'status3' => 'stand_by',  'status4' => 'delivered'));
            }
            if($status == 'not_delivered'){    
                $queryBuilder->andWhere('t.status = :status1 OR t.status = :status2 OR t.status = :status3')
                ->setParameters(array('status1' => 'not_started', 'status2' => 'in_progress',  'status3' => 'stand_by'));
            }
            if($status != 'not_delivered' && $status != 'not_closed'){
                $queryBuilder->andWhere('t.status = :status')
                ->setParameter('status', $status);
            }
        }

        if ($id != null) {
            $queryBuilder->andWhere('t.id = :id')
            ->setParameter('id', $id);
        }

        if ($type != null) {
            $queryBuilder->andWhere('t.type = :type')
            ->setParameter('type', $type);
        }

        if ($businessUnit !== null) {
            $queryBuilder->andWhere('t.businessUnit = :businessUnit')
            ->setParameter('businessUnit', $businessUnit);
        }

        if ($name !== null) {
            $queryBuilder->andWhere('t.name LIKE :name')
            ->setParameter('name', "%{$name}%");
        }

        if ($number !== null) {
            $queryBuilder->andWhere('t.number LIKE :number')
            ->setParameter('number', "%{$number}%");
        } 
       // $requester=null, $=null

        if ($requester !== null) {
            $queryBuilder->innerJoin(
                'App\Entity\ObjectMember',
                't2',
                'WITH',
                't.id = t2.objectId'
            );
            $queryBuilder->andWhere('t2.entity = :entity')
                ->setParameter('entity', 'task');
            $queryBuilder->andWhere('t2.user = :user')
                ->setParameter('user', $requester);
            $queryBuilder->andWhere('t2.role = :role')
                ->setParameter('role', 'requester');
        }

        if ($responsible !== null) {
            $queryBuilder->innerJoin(
                'App\Entity\ObjectMember',
                't3',
                'WITH',
                't.id = t3.objectId'
            );
            $queryBuilder->andWhere('t3.entity = :entity')
                ->setParameter('entity', 'task');
            $queryBuilder->andWhere('t3.user = :user')
                ->setParameter('user', $responsible);
            $queryBuilder->andWhere('t3.role = :role')
                ->setParameter('role', 'responsible');
        }
		
		if ($parent !== null){
			$queryBuilder->andWhere('t.parent = :parent')
            ->setParameter('parent', $parent);
		}
		
		if ($queue !== null){
			$queryBuilder->andWhere('t.queue = :queue')
            ->setParameter('queue', $queue);
        }
        if ($thirdParty !== null){
			$queryBuilder->andWhere('t.thirdParty = :thirdParty')
            ->setParameter('thirdParty', $thirdParty);
        }
        if ($tag !== null) {
            $queryBuilder->innerJoin(
                'App\Entity\ObjectTag',
                't4',
                'WITH',
                't.id = t4.objectId'
            );
            $queryBuilder->andWhere('t4.entity = :entity')
                ->setParameter('entity', 'task');
            $queryBuilder->andWhere('t4.tag = :tag')
                ->setParameter('tag', $tag);
        }
		
        $limit = 30;
        $query = $queryBuilder->getQuery(); 
        $query->setFirstResult(($page - 1) * $limit)->setMaxResults($limit);
        $paginator = new Paginator($query);

        return $paginator;
    }
 
 
    /**
     * @param string $number
     * @param string $name
     * @param string $status_code
     * @param string $group_code
     * @param string $sub_group_code
     * @return mixed
     */
    public function findFilter($number = '', $name = '', $status_code = '', $group_code = '', $sub_group_code = '' )
    {
        $queryBuilder = $this->createQueryBuilder('t');
        
        if ($number) {
            $queryBuilder->andWhere('t.number = :number')
            ->setParameter('number', $number);
           }
        if ($name) {
           $queryBuilder->andWhere('t.name= :name')
           ->setParameter('name', $name);

           }       
        
        if ($status_code) {
           $queryBuilder->andWhere('t.status= :status')
           ->setParameter('status', $status_code);

           }
           
        if ($group_code) {
         $queryBuilder->innerJoin(
                'App\Entity\TenantParameterValue',
                't1',
                'WITH',
                't1.id = t.group'
            ) 
            ->andWhere('t1.valueCode = :group')  
            ->setParameter('group', $group_code);

           }
           
        if ($sub_group_code) {
                $queryBuilder->innerJoin(
                'App\Entity\TenantParameterValue',
                't2',
                'WITH',
                't2.id = t.subGroup'
            ) 
            ->andWhere('t2.valueCode = :subGroup')  
             ->setParameter('subGroup', $sub_group_code);

           }
        $result = $queryBuilder->getQuery()->getResult();
    
        return $result;
        
    }
			
  //Recuperer le numero du Task en fonction du tenant 
  
  /**
   * @param string $tenant_code
   * @return mixed
  */

     public function findNextNumber($tenant_code,$type_code,$parent_id,$business_unit_code){
       if ( ($type_code == 'assignment') || ($type_code == 'project') )
       {
        if ($type_code == 'assignment')
        $shortCode='m' ;
        elseif ($type_code == 'project')
        $shortCode='p' ; 
        
        $lastTicket= $this ->createQueryBuilder("t")
        ->select("t.number")
        ->where("t.tenant = :tenant_code")
        ->andWhere('t.type = :type_code')
        ->setParameter ("tenant_code", $tenant_code)
        ->setParameter ("type_code", $type_code)
        ->orderBy("t.id","DESC")
       ->setMaxResults(1)
       ->getQuery()
       ->getResult();

       if (count($lastTicket)>0)
       {
       $lastNumber=$lastTicket[0]["number"];
       $lastNumber=(int)str_replace($shortCode.'-','',$lastNumber);
       }
       else
       { 
       $lastNumber=0;
      
       }


    
    
    
    
    
      }
       elseif ($type_code == 'sub_task')
       { 
        
        $prefixe= $this->createQueryBuilder("t")
        ->select("t.number")
        ->where("t.id = :parent_id")
        ->andWhere('t.tenant = :tenant_code')
        ->setParameter ("parent_id", $parent_id)
        ->setParameter ("tenant_code", $tenant_code)
        ->orderBy("t.number","DESC")
        ->setMaxResults(1)
        ->getQuery()
        ->getSingleScalarResult();
         $lastNumber= $this ->createQueryBuilder("t")
                     ->select("t.number")
                     ->where("t.tenant = :tenant_code")
                     ->andWhere('t.type = :type_code')
                     ->andWhere('t.parent = :parent_id')
                     ->setParameter ("tenant_code", $tenant_code)
                     ->setParameter ("parent_id", $parent_id)
                     ->setParameter ("type_code", 'sub_task')
                     ->orderBy("t.number","DESC")
                     ->setMaxResults(1)
                     ->getQuery()
                     ->getSingleScalarResult();
             if (is_int($lastNumber))
             $number=$lastNumber+1;
             else 
             $number=1;

             
             $number=$prefixe+'/'+$number;

        }
        elseif (($type_code == 'task' ) || ($type_code == 'queue' ) )   
        {


        
         $params = [];
       
         if ($parent_id !='null')
         {
         
            $Parent = $this->createQueryBuilder("t")
            ->select("t.shortCode AS short_code")
            ->where("t.id = :parent_id")
            ->andWhere('t.tenant = :tenant_code')
            ->setParameter ("parent_id", $parent_id)
            ->setParameter ("tenant_code", $tenant_code)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
            $shortCode=$Parent[0]["short_code"];

            $lastTicketForParent= $this ->createQueryBuilder("t")
                     ->select("t.number")
                     ->where("t.tenant = :tenant_code")
                     //->andWhere('t.type = :type_code')
                     ->andWhere('t.parent = :parent_id')
                     //->andWhere("t.type ='queue' or  t.type ='queue'")
                     ->andWhere("t.type ='queue' or  t.type ='task'")
                     ->setParameter ("tenant_code", $tenant_code)
                     ->setParameter ("parent_id", $parent_id)
                     //->setParameters($params)
                   //  ->setParameter ("type_codes0", "task")
                    // ->setParameter ("type_codes1", "queue")
                     ->orderBy("t.id","DESC")
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getResult();

                    if (count($lastTicketForParent)>0)
                    {
                    $lastNumber=$lastTicketForParent[0]["number"];
                   // $shortCode=$lastTicketForParent[0]["short_code"];
                    $lastNumber=(int)str_replace($shortCode.'-','',$lastNumber);
                    }
                    else
                    { 
                    $lastNumber=0;
                   
                    }

         }else  //parent non passé => business_unit
         {
            $params = [];
            $params = array_merge($params,['business_unit_code' => $business_unit_code], ['tenant_code' => $tenant_code]);

            $lastTicketForBU= $this->createQueryBuilder('t')
                ->select("t.id AS id","t.number AS number","t.name AS name","mc.shortCode AS short_code")
                ->innerJoin(
                    'App\Entity\MasterParameterValue',
                    'mpv',
                    'WITH',
                    "t.businessUnit = mpv.code AND mpv.parameter='type' AND mpv.entity = 'business_unit'"
                )
                 ->innerJoin(
                    'App\Entity\MasterCode',
                    'mc',
                    'WITH',
                    "mc.code = mpv.code"
                )
                ->Where("t.businessUnit = :business_unit_code AND t.parent is null ")
                ->andWhere('t.tenant = :tenant_code')
                ->andWhere("t.type ='queue' or  t.type ='task'")
                //->andWhere("IDENTITY(t.parent)=:parent_id or :parent_id='null'")
                ->setParameters($params)
                ->orderBy("t.id","DESC")
                     ->setMaxResults(1)
                     ->getQuery()
                     ->getResult();
                     //->getSingleScalarResult();
                  
                     if (count($lastTicketForBU)>0)
                     {
                     $lastNumber=$lastTicketForBU[0]["number"];
                     $shortCode=$lastTicketForBU[0]["short_code"];
                     $lastNumber=(int)str_replace($shortCode.'-','',$lastNumber);
                     }
                     else
                     { 
                     $lastNumber=0;
                     $sql = "SELECT short_code FROM master_code WHERE code= '".$business_unit_code."'";
                     $query = $this->getEntityManager()->getConnection()->query($sql);            
                     $result = $query->fetchAll();
                     $shortCode=$result[0]["short_code"];
                     }

            }
            /* if (is_int($lastNumber))
             $number=$lastNumber+1;
             else 
             $number=1;*/
        }
        
       //récupération du short_code 
      
       $number=$lastNumber+1;
       if ($shortCode !='')
       $number=$shortCode.'-'.$number;
       
       return  $number;

    }

	  /**
	 * @param string $status_code
	 * @param string $tenant_code
     * @param string $type_code
     * @param string $business_unit_code
	 * @param integer $parent_id
	 * @param string $channel_code
	 * @param string $name
	 * @param string $description
	 * @param string $issue
	 * @param string $number
	 * @param string $process_id
	 * @param string $priority_type_code
	 
     * @return mixed
     */
	 
	  public function addTicket($status_code='', $tenant_code='', $type_code = '', $business_unit_code = '', $parent_id = '', $channel_code = '', $name = '', $description = '', $issue = 0, $process_id = '', $priority_type_code = '', $number = '')
       {
	
       $nextNumber = $this->findNextNumber($tenant_code);
		
//dd($nextNumber);
          $queryBuilder = $this->getEntityManager()->getConnection()->createQueryBuilder();
       
        $queryBuilder
            ->insert('task')
            ->values(
        array(
		    'status_code' => '?',
			'tenant_code' => '?',
            'type_code' => '?',
            'business_unit_code' => '?',
			'parent_id' => '?',
			'channel_code' => '?',
			'name' => '?',
			'description' => '?',
	        'issue' => '?',
			'process_id' => '?',
			'priority_type_code' => '?',
			'number' => '?',
			
			
    )
    )
	->setParameter(0, $status_code)
	->setParameter(1, $tenant_code)
	->setParameter(2, $type_code)
    ->setParameter(3, $business_unit_code)
	->setParameter(4, NULL)
	->setParameter(5, $channel_code) 
	->setParameter(6, $name) 
	->setParameter(7, $description) 
	->setParameter(8, $issue)
	->setParameter(9, NULL)
	->setParameter(10, $priority_type_code)
	->setParameter(11, $nextNumber);
	

	 $queryBuilder->execute();
	 $result = $this->getEntityManager()->getConnection()->lastInsertId();
	 //dd($result);
	 return $result;
	
	}
	
	
	   public function getMinTasks($type_codes, $business_unit_code, $tenant_code, $parent_id, $language_code,$name)
    {
	
        $type_codes_condition = "";
        $type_codes_parameters = [];
      
        if (isset($type_codes)) {
            $type_codes = explode(",", $type_codes);
		
            for ($i = 0; $i < count($type_codes); $i++) {
                //type_codes where condition
                $type_codes_condition = $type_codes_condition . "(t.type=:type_codes" . $i . ")";
				
                // set type_codes parameters
                $type_codes_parameters = array_merge($type_codes_parameters, ["type_codes" . $i => $type_codes[$i]]);
                if ($i + 1 < count($type_codes)) {
                    $type_codes_condition = $type_codes_condition . " or ";
				
                }
            }
        } else {
            $type_codes_condition = "(:type_codes='null')";
            $type_codes_parameters = ["type_codes" => 'null'];
        }

        $params = [];
        // $params concatenation
        if (count($type_codes_parameters) > 0) {
            $params = array_merge($params, $type_codes_parameters);
        }
        $params = array_merge($params,['business_unit_code' => $business_unit_code], ['tenant_code' => $tenant_code], ['parent_id' => $parent_id], ['loc' => $language_code],['name'=>"%{$name}%"]);

        return $this->createQueryBuilder('t')
            ->select("t.id AS id","t.number AS number","t.name AS name","IDENTITY(t.type) AS type_code","mct.name AS type_name","IDENTITY(t.parent) AS parent_id")
            ->innerJoin(
                'App\Entity\MasterParameterValue',
                'mpv',
                'WITH',
                "t.type = mpv.code AND mpv.parameter='type' AND mpv.entity = 'task'"
            )
			 ->innerJoin(
                'App\Entity\MasterCode',
                'mc',
                'WITH',
                "mc.code = mpv.code"
            )
			->innerJoin(
                'App\Entity\MasterCodeTranslation',
                'mct',
                'WITH',
                "mct.code = mc.code AND mct.locale = :loc"
            )
			
            ->Where("t.status <> 'closed'")
            ->andWhere($type_codes_condition)
			->andWhere("t.businessUnit = :business_unit_code or :business_unit_code='null'")
            ->andWhere('t.tenant = :tenant_code')
            ->andWhere('t.name like :name')
			//optional parameter sub_type_code
            ->andWhere("IDENTITY(t.parent)=:parent_id or :parent_id='null'")
            ->setParameters($params)
            ->getQuery()
            ->getResult();
    }

    
    public function getuserTask($user, $tenant){
        $sql = "SELECT task.number number, task.name name, task.status_code status_code, master_code_translation.name status, object_member.user_id FROM task 
        LEFT JOIN object_member ON object_member.object_id = task.id AND object_member.entity_code = 'task'
        LEFT JOIN master_code_translation ON master_code_translation.code = task.status_code AND master_code_translation.locale = 'fr'
        WHERE status_code <> 'closed' AND status_code <> 'delivered' AND object_member.user_id = ".$user." AND object_member.tenant_code = '".$tenant."'";
        
        $query = $this->getEntityManager()->getConnection()->query($sql);
        $result = $query->fetchAll(); 

        return $result;
    }
	
    // /**
    //  * @return Task[] Returns an array of Task objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Task
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
	
	
	
	
	
}
