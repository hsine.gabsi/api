<?php

namespace App\Repository;

use App\Entity\RecurringOperation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RecurringOperation|null find($id, $lockMode = null, $lockVersion = null)
 * @method RecurringOperation|null findOneBy(array $criteria, array $orderBy = null)
 * @method RecurringOperation[]    findAll()
 * @method RecurringOperation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecurringOperationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RecurringOperation::class);
    }

    // /**
    //  * @return RecurringOperation[] Returns an array of RecurringOperation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RecurringOperation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
