<?php

namespace App\Repository;

use App\Entity\ObjectComment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectComment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectComment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectComment[]    findAll()
 * @method ObjectComment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectCommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectComment::class);
    }

    // /**
    //  * @return ObjectComment[] Returns an array of ObjectComment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectComment
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
