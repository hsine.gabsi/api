<?php

namespace App\Repository;

use App\Entity\Quote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Quote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Quote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Quote[]    findAll()
 * @method Quote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Quote::class);
    }

    public function findNextNumberQuote($tenant, $yearOfSend, $quoteId){
        if($quoteId == 0){
            $lastQuote = $this->createQueryBuilder("t")
                                ->select("t.number")
                                ->where("t.tenant = :tenant")
                                ->setParameter ("tenant", $tenant)
                                ->orderBy("t.id","DESC")
                            ->setMaxResults(1)
                            ->getQuery()
                            ->getResult();
                            
            if (count($lastQuote) > 0){
                $lastNumber = $lastQuote[0]["number"];
                $lastNumber = (int)explode("-", $lastNumber)[1];
                $lastNumber = $lastNumber + 1;
            }else{ 
                $lastNumber = 0;        
            }
            
            $lastNumber = str_pad($lastNumber, 4, '0', STR_PAD_LEFT);
            return $yearOfSend . '-' . $lastNumber;
        }else{
            $lastQuote = $this->createQueryBuilder("t")
                                ->select("t.number")
                                ->where("t.id = :id")
                                ->setParameter ("id", $quoteId)
                            ->setMaxResults(1)
                            ->getQuery()
                            ->getResult();
            return $lastQuote[0]["number"];
        }
    }

    // /**
    //  * @return Quote[] Returns an array of Quote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Quote
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
