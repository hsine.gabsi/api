<?php

namespace App\Repository;

use App\Entity\TenantPreference;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TenantPreference|null find($id, $lockMode = null, $lockVersion = null)
 * @method TenantPreference|null findOneBy(array $criteria, array $orderBy = null)
 * @method TenantPreference[]    findAll()
 * @method TenantPreference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TenantPreferenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenantPreference::class);
    }

    // /**
    //  * @return TenantPreference[] Returns an array of TenantPreference objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TenantPreference
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
