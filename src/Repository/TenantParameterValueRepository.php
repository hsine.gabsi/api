<?php

namespace App\Repository;

use App\Entity\TenantParameterValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TenantParameterValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method TenantParameterValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method TenantParameterValue[]    findAll()
 * @method TenantParameterValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TenantParameterValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenantParameterValue::class);
    }

    public function getMinTenantMasterParameterValue($tenant, $parameter, $entity, $locale){ 
        $queryBuilder = $this->createQueryBuilder("t")
                            ->select("t.id, l.name")
                            ->innerJoin(
                                'App\Entity\TenantParameterValueTranslation',
                                'l',
                                'WITH',
                                't.id = l.tenantParameterValue'
                            );
        
        if($tenant == 'advences'){ 
            $queryBuilder->andWhere('t.tenant = :tenant')
                         ->setParameter ("tenant", $tenant);
        }else{ 
            $queryBuilder->andWhere('t.tenant = :tenant1 OR t.tenant = :tenant2')
                        ->setParameters(array('tenant1' => $tenant, 'tenant2' => 'advences'));
        }
        $queryBuilder->andWhere('t.parameter = :parameter')
                    ->setParameter ("parameter", $parameter)
                    ->andWhere('t.entity = :entity')
                    ->setParameter ("entity", $entity)
                    ->andWhere('l.locale = :locale')
                    ->setParameter ("locale", $locale);

        return $queryBuilder->orderBy("l.name","ASC")
                            ->getQuery()
                            ->getResult();
    }

    // /**
    //  * @return TenantParameterValue[] Returns an array of TenantParameterValue objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TenantParameterValue
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
