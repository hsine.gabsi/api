<?php

namespace App\Repository;

use App\Entity\TaskDate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TaskDate|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskDate|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskDate[]    findAll()
 * @method TaskDate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskDateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TaskDate::class);
    }

    // /**
    //  * @return TaskDate[] Returns an array of TaskDate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicketDate
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
