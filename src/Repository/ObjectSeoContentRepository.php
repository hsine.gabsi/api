<?php

namespace App\Repository;

use App\Entity\ObjectSeoContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectSeoContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectSeoContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectSeoContent[]    findAll()
 * @method ObjectSeoContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectSeoContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectSeoContent::class);
    }
    
    public function findByObjectID($objectId, $languageCode = 'fr', $entityCode = '', $tenantCode = '')
    {
        return $this->createQueryBuilder('t1')
            ->andWhere('t1.objectId = :objectId')
            ->setParameter('objectId', $objectId)
            ->andWhere('t1.locale= :_languageCode')
            ->setParameter('_languageCode', $languageCode)
            ->andWhere('t1.entity = :_entityCode')
            ->setParameter('_entityCode', $entityCode)        
            ->andWhere('t1.tenant = :_tenantCode')
            ->setParameter('_tenantCode', $tenantCode)        
            ->getQuery()
            ->getResult()
        ;
    }
    // /**
    //  * @return ObjectSeoContent[] Returns an array of ObjectSeoContent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectSeoContent
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
