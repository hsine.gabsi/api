<?php

namespace App\Repository;

use App\Entity\PurchaseInvoicePayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PurchaseInvoicePayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method PurchaseInvoicePayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method PurchaseInvoicePayment[]    findAll()
 * @method PurchaseInvoicePayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PurchaseInvoicePaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PurchaseInvoicePayment::class);
    }

    public function amountDueCalculateUpdate($paymentId, $invoiceId, $amount, $tenant_code){
        $sql = "SELECT p.total_amount_incl_tax AS total, c.decimals, p.currency_code, p.remaining_amount_due AS amountDue
				FROM purchase_invoice p INNER JOIN currency c ON p.currency_code = c.code
				WHERE p.tenant_code = '".$tenant_code."'
                    AND p.id = ".$invoiceId;
        $query = $this->getEntityManager()->getConnection()->query($sql);
        $result = $query->fetch();
        if(count($result) > 0){
            $amountDue = null;
            $status = 'unpaid';
            $decimal = $result['decimals'];
            $total = $result['total'];
            $amountDueOld = $result['amountDue'];
            $currency = $result['currency_code'];            
            $amountAssociate = str_replace(' ', '', $amount);
            $amountAssociate = str_replace(',', '.', $amountAssociate);
            if (is_numeric($amountAssociate)){
                $amountAssociate = $amountAssociate * pow(10, (int)$decimal);
                $pos = strpos((string)$amountAssociate, ".");
                if ($pos > 0) {
                    $amountAssociate = substr($amountAssociate, 0, $pos);
                }
                if($amountAssociate != 0){
                    if(!is_null($amountDueOld) && $amountDueOld < $total){
                        $amountDue = $amountDueOld-$amountAssociate;
                    }else{
                        $amountDue = $total-$amountAssociate;
                    }
                }                
            }
            if(!is_null($amountDue)){
                // partially_paid
                if($amountDue > 0 && $amountDue < $total){
                    $status = 'partially_paid';
                }else{// paid
                    $status = 'paid';
                }
                $upd = "UPDATE purchase_invoice SET remaining_amount_due=".$amountDue.", payment_status_type_code='".$status."'
                        WHERE id = ".$invoiceId;
                $this->getEntityManager()->getConnection()->query($upd);
            }else{
                $upd = "UPDATE purchase_invoice SET remaining_amount_due=total_amount_incl_tax, payment_status_type_code='unpaid'
                        WHERE id = ".$invoiceId;
                $this->getEntityManager()->getConnection()->query($upd);
            }
            $sqlPayment = "SELECT id FROM purchase_invoice_payment WHERE purchase_invoice_id=".$invoiceId." AND payment_id=".$paymentId;
            $queryPay = $this->getEntityManager()->getConnection()->query($sqlPayment);
            $resultPay = $queryPay->fetchAll();
            if(count($resultPay) > 0){
                if($status == 'unpaid'){
                    $delPay = "DELETE FROM purchase_invoice_payment WHERE purchase_invoice_id = ".$invoiceId." AND payment_id=".$paymentId;
                    $this->getEntityManager()->getConnection()->query($delPay);
                }else{
                    $updPay = "UPDATE purchase_invoice_payment SET amount=amount+".$amountAssociate."
                            WHERE purchase_invoice_id = ".$invoiceId." AND payment_id=".$paymentId;
                    $this->getEntityManager()->getConnection()->query($updPay);
                }
            }else{
                $insertPay = "INSERT INTO purchase_invoice_payment(purchase_invoice_id,payment_id,currency_code,amount)
                              VALUES(".$invoiceId.",".$paymentId.",'".$currency."',".$amountAssociate.")";
                $this->getEntityManager()->getConnection()->query($insertPay);
            }
        }
        $sqlFinal = "SELECT number, date, currency_code AS currency, REPLACE(FORMAT(ROUND(total_amount_incl_tax/POW(10, ".$decimal."), 2), ".$decimal."), ',', ' ') AS totalAmountInclTax, payment_status_type_code AS paymentStatus, REPLACE(FORMAT(ROUND(remaining_amount_due/POW(10, ".$decimal."), 2), ".$decimal."), ',', ' ') AS remainingAmountDue
                FROM purchase_invoice WHERE id = ".$invoiceId;
                
        $qryFinal = $this->getEntityManager()->getConnection()->query($sqlFinal);
        return $qryFinal->fetchAll();
    }
    // /**
    //  * @return PurchaseInvoicePayment[] Returns an array of PurchaseInvoicePayment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PurchaseInvoicePayment
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
