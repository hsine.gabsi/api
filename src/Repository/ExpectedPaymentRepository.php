<?php

namespace App\Repository;

use App\Entity\ExpectedPayment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ExpectedPayment|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExpectedPayment|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExpectedPayment[]    findAll()
 * @method ExpectedPayment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExpectedPaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExpectedPayment::class);
    }

    // /**
    //  * @return ExpectedPayment[] Returns an array of ExpectedPayment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ExpectedPayment
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
