<?php

namespace App\Repository;

use App\Entity\QuotePrice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method QuotePrice|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuotePrice|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuotePrice[]    findAll()
 * @method QuotePrice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuotePriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, QuotePrice::class);
    }

    // /**
    //  * @return QuotePrice[] Returns an array of QuotePrice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?QuotePrice
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
