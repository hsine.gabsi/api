<?php

namespace App\Repository;

use App\Entity\SalesInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Parser;

/**
 * @method SalesInvoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalesInvoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalesInvoice[]    findAll()
 * @method SalesInvoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalesInvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalesInvoice::class);
    }

    public function findNextNumberSalesInvoice($tenant, $yearOfSend){
        $lastsalesinvoice = $this->createQueryBuilder("t")
                            ->select("t.number")
                            ->where("t.tenant = :tenant")
                            ->setParameter ("tenant", $tenant)
                            ->orderBy("t.id","DESC")
                            ->setMaxResults(1)
                            ->getQuery()
                            ->getResult();

        if (count($lastsalesinvoice) > 0){
            $lastNumber = $lastsalesinvoice[0]["number"];
            $lastNumber = (int)explode("-", $lastNumber)[1];
            $lastNumber = $lastNumber + 1;
        }else{ 
            $lastNumber = 1;        
        }
        $lastNumber = str_pad($lastNumber, 4, '0', STR_PAD_LEFT);
        return $yearOfSend . '-' . $lastNumber;
    }
	
	public function getMinSalesInvoices($number,$third_id,$tenant_code){
		$sql = "SELECT s.date, s.number, ROUND(total_amount_incl_tax/POW(10, c.decimals), 2) AS total
				FROM sales_invoice s INNER JOIN currency c ON s.currency_code = c.code
				WHERE s.tenant_code = '".$tenant_code."'
					AND (s.number = '".$number."' or ".$number." is null)
					AND (s.third_party_id = ".$third_id." or ".$third_id." is null)
				ORDER BY s.date DESC";
		$query = $this->getEntityManager()->getConnection()->query($sql);
        $result = $query->fetchAll();		
		return $result;
	}
    
   
    public function getReporting($tenant){
        $curYear =date('Y');
        $fiscalYearFirstMonth = $this -> getFiscalier($tenant);
        if ( $fiscalYearFirstMonth==1)
        $fiscalier= $curYear;
        else 
        $fiscalier=$curYear.'-'.($curYear+1);
        $columns = $this ->getListMonth($fiscalYearFirstMonth);
        $columnsoutput=array("year_total" =>"Total");
        for ($i = 0; $i < count( $columns); $i++){
             $columnsoutput[$columns[$i]["code"]]=$columns[$i]["label"];
        }
        $products=$this ->getReportingCA($tenant,$curYear, $fiscalYearFirstMonth);
        $charges=  $this ->getReportingCharge($tenant,$curYear, $fiscalYearFirstMonth);
      
        $detailstotalyear=array("label" => 'Résultat');
        $detailstotalyear["year_total"]=$this ->getDelta($products["total"]["year_total"],$charges["total"]["year_total"]);
        $products["total"]["year_total"]["total"]=NUMBER_FORMAT( $products["total"]["year_total"]["total"],0,',',' ');
        $charges["total"]["year_total"]["total"]=NUMBER_FORMAT( $charges["total"]["year_total"]["total"],0,',',' ');
         $listmonth=$this->getListMonth($fiscalYearFirstMonth);
        for ($i = 0; $i < count($listmonth); $i++){
            $detailstotalyear[$listmonth[$i]["code"]]=$this ->getDelta($products["total"][$listmonth[$i]["code"]],$charges["total"][$listmonth[$i]["code"]]);
            if ($products["total"][$listmonth[$i]["code"]]!= null)
            $products["total"][$listmonth[$i]["code"]]["total"]=NUMBER_FORMAT($products["total"][$listmonth[$i]["code"]]["total"],0,',',' ');
            $charges["total"][$listmonth[$i]["code"]]["total"]=NUMBER_FORMAT($charges["total"][$listmonth[$i]["code"]]["total"],0,',',' ');
        } 
        $delta=array("total" => $detailstotalyear);


        //NUMBER_FORMAT($result[0]["total"],0,',',' ');
        $finalResult= array("Columns" => $columnsoutput, "Product" => $products, "Charge" =>$charges, "Delta" => $delta);

        // annéé -1
         $yearl =date('Y')-1;
        if ( $fiscalYearFirstMonth==1)
        $fiscalierl= $yearl;
        else 
        $fiscalierl=$yearl.'-'.($yearl+1);
        $columnsoutput=array("year_total" =>"Total");
        for ($i = 0; $i < count( $columns); $i++){
             $columnsoutput[$columns[$i]["code"]]=$columns[$i]["label"];
        }
        $productsl=$this ->getReportingCA($tenant,$yearl, $fiscalYearFirstMonth);
        $chargesl=  $this ->getReportingCharge($tenant,$yearl, $fiscalYearFirstMonth);
      
        $detailstotalyearl=array("label" => 'Résultat');
        $detailstotalyearl["year_total"]=$this ->getDelta($productsl["total"]["year_total"],$chargesl["total"]["year_total"]);
        $productsl["total"]["year_total"]["total"]=NUMBER_FORMAT( $productsl["total"]["year_total"]["total"],0,',',' ');
        $chargesl["total"]["year_total"]["total"]=NUMBER_FORMAT( $chargesl["total"]["year_total"]["total"],0,',',' ');
         $listmonth=$this->getListMonth($fiscalYearFirstMonth);
        for ($i = 0; $i < count($listmonth); $i++){
            $detailstotalyearl[$listmonth[$i]["code"]]=$this ->getDelta($productsl["total"][$listmonth[$i]["code"]],$chargesl["total"][$listmonth[$i]["code"]]);
            if ($productsl["total"][$listmonth[$i]["code"]]!= null)
            $productsl["total"][$listmonth[$i]["code"]]["total"]=NUMBER_FORMAT($productsl["total"][$listmonth[$i]["code"]]["total"],0,',',' ');
            $chargesl["total"][$listmonth[$i]["code"]]["total"]=NUMBER_FORMAT($chargesl["total"][$listmonth[$i]["code"]]["total"],0,',',' ');
        } 
        $deltal=array("total" => $detailstotalyearl);


        //NUMBER_FORMAT($result[0]["total"],0,',',' ');
        $finalResultl= array("Columns" => $columnsoutput, "Product" => $productsl, "Charge" =>$chargesl, "Delta" => $deltal);

        $final=array($fiscalierl => $finalResultl,$fiscalier => $finalResult);
        return $final ;
        
    }
    public function getDelta($ca, $charge)
    {
        if ($ca!=null && $charge != null)
       return array("total" =>  NUMBER_FORMAT($ca["total"]-$charge["total"],0,',',' '),"currency_code" => $ca["currency_code"],"percentage" => null,"error_code" => $ca["error_code"]);
    
    }
    public function getReportingCA($tenant,$year, $fiscalYearFirstMonth){
         
        $product_group_names = $this->getListProductGroup($tenant);
        $finalResult=[];
        $product_group_name=[];
        $totalyear=$this-> getReportingCAByYearByMonth($tenant, $year, $fiscalYearFirstMonth,  0,-1);
        $detailstotalyear=array("label" => 'Total');
        $detailstotalyear["year_total"]= $totalyear;
         $listmonth=$this->getListMonth($fiscalYearFirstMonth);
        for ($i = 0; $i < count($listmonth); $i++){
            $detailstotalyear[$listmonth[$i]["code"]]= $this->getReportingCAByYearByMonth($tenant, $year, $fiscalYearFirstMonth,  $listmonth[$i]["number"],-1 );          
        } 
        $details=array("total" => $detailstotalyear);
        for ($numero = 0; $numero < count($product_group_names); $numero++){
           $totalyear=$this-> getReportingCAByYearByMonth($tenant, $year, $fiscalYearFirstMonth,  0,$product_group_names[$numero]["product_group_id"]);
           $detailsyear=array("label" => $product_group_names[$numero]["product_group_name"]);
           $detailsyear["year_total"]= $totalyear;
             for ($i = 0; $i < count($listmonth); $i++){
                $detailsyear[$listmonth[$i]["code"]]= $this-> getReportingCAByYearByMonth($tenant, $year, $fiscalYearFirstMonth,  $listmonth[$i]["number"],$product_group_names[$numero]["product_group_id"]  );          
             }
             $details[$product_group_names[$numero]["product_group_code"]]=$detailsyear;
        }
        return  $details;
    }

   

    
    public function getCAByYearByMonth($tenant){
        $sql = "SELECT YEAR(DATE) year, 
        CASE WHEN Month(DATE) = 1 THEN CONCAT('Jan ',YEAR(DATE))
        WHEN Month(DATE) = 2 THEN CONCAT('Fév ',YEAR(DATE))
        WHEN Month(DATE) = 3 THEN CONCAT('Mar ',YEAR(DATE))
        WHEN Month(DATE) = 4 THEN CONCAT('Avr ',YEAR(DATE))
        WHEN Month(DATE) = 5 THEN CONCAT('Mai ',YEAR(DATE))
        WHEN Month(DATE) = 6 THEN CONCAT('Jun ',YEAR(DATE))
        WHEN Month(DATE) = 7 THEN CONCAT('Jui ',YEAR(DATE))
        WHEN Month(DATE) = 8 THEN CONCAT('Aoû ',YEAR(DATE))
        WHEN Month(DATE) = 9 THEN CONCAT('Sep ',YEAR(DATE))
        WHEN Month(DATE) = 10 THEN CONCAT('Oct ',YEAR(DATE))
        WHEN Month(DATE) = 11 THEN CONCAT('Nov ',YEAR(DATE))
        WHEN Month(DATE) = 12 THEN CONCAT('Déc ',YEAR(DATE))
        END date, ROUND(SUM(total_amount_incl_tax  - vat_amount), 2) CA
        FROM sales_invoice
        where tenant_code = '" . $tenant . "'
        AND YEAR(DATE) BETWEEN YEAR(DATE_SUB(curdate(), INTERVAL 2 YEAR)) AND YEAR(CURDATE())
        GROUP BY Month(DATE), YEAR(DATE)
        order BY sales_invoice.DATE ASC";
        
        $query = $this->getEntityManager()->getConnection()->query($sql);
        $result = $query->fetchAll();

        /*$sql1 = "SELECT distinct YEAR(Date) year FROM sales_invoice
        where tenant_code = '" . $tenant . "'
        AND YEAR(DATE) BETWEEN YEAR(DATE_SUB(curdate(), INTERVAL 2 YEAR)) AND YEAR(CURDATE())
        GROUP BY Month(DATE), YEAR(DATE)";
        
        $query1 = $this->getEntityManager()->getConnection()->query($sql1);
        $resultyear = $query1->fetchAll();

        if (count($resultyear)>0)
        {
            $details = array();
            for ($numero = 0; $numero < count($resultyear); $numero++){
                $yearNow = $resultyear[$numero]["year"];
                //$details[$yearNow] = $resultyear[$numero]["year"];
                $j = 0;
                for ($i = 0; $i < count($result); $i++){
                    if($resultyear[$numero]["year"] == $result[$i]["year"]){
                        $details[$yearNow][$j]["date"] = $result[$i]["date"];
                        $details[$yearNow][$j]["CA"] = $result[$i]["CA"];
                        $j++;
                    }
                }
            }
            return $details;
        }
        else return null;*/
        return $result;
    }

    public function getUnpaid($tenant){
        $sql = "SELECT sales_invoice.third_party_id, third_party.name name, third_party.first_name firstname, SUM(remaining_amount_due)/100 unpaid, currency_code
        FROM sales_invoice 
        LEFT JOIN third_party ON third_party.id = sales_invoice.third_party_id
        WHERE due_date < CURRENT_DATE() AND sales_invoice.tenant_code = '" . $tenant . "'
        GROUP BY third_party_id ORDER BY SUM(remaining_amount_due) DESC";
        
        $query = $this->getEntityManager()->getConnection()->query($sql);
        $result = $query->fetchAll();
        if (count($result)>0)
        {
            $total = 0;
            $details = array();
            for ($numero = 0; $numero < count($result); $numero++){
                $total = $total + $result[$numero]["unpaid"];
                $details[$numero]["unpaid"] = NUMBER_FORMAT($result[$numero]["unpaid"],2,'.',' ');
                $details[$numero]["currency_code"] = $result[$numero]["currency_code"];
                $details[$numero]["name"] = $result[$numero]["name"];
                $details[$numero]["firstname"] = $result[$numero]["firstname"];
                $details[$numero]["third_party_id"] = $result[$numero]["third_party_id"];
            }
            return array("total" => NUMBER_FORMAT($total,2,'.',' '), "result" => $details);
        }
        else return null;
    }

    public function getReportingCAByYearByMonth($tenant, $year,$fiscalYearFirstMonth, $month,$product_group_id){
        if (strlen($fiscalYearFirstMonth)==1)
        $fiscalYearFirstMonth="0".$fiscalYearFirstMonth;
        $nextyear=$year+1;
        $PriceVatIsIncluded = $this -> getPriceVatIsIncluded($tenant);

        $sql =  "SELECT CASE (case when ".$month." != 0 then month(i.date) else 0 end)
        WHEN 1 THEN 'Jan'
        WHEN 2 THEN 'Fév'
        WHEN 3 THEN 'Mar'
        WHEN 4 THEN 'Avr'
        WHEN 5 THEN 'Mai'
        WHEN 6 THEN 'Jun'
        WHEN 7 THEN 'Jui'
        WHEN 8 THEN 'Aoû'
        WHEN 9 THEN 'Sep'
        WHEN 10 THEN 'Oct'
        WHEN 11 THEN 'Nov'
        WHEN 12 THEN 'Déc'
        ELSE Case $fiscalYearFirstMonth WHEN 1 then $year else CONCAT($year,'-',$year+1) end  END AS label, 
        (CASE  WHEN ($PriceVatIsIncluded=0 OR t.vat_exempt =1)  THEN  ROUND( SUM( (CASE WHEN i.credit_note=0 then id.unit_price else (-id.unit_price) END) * quantity) /100 ,0)  
        else ROUND( SUM( (CASE WHEN i.credit_note=0 then id.unit_price else (-id.unit_price) END)* (1/(1+p.vat_rate/100)) * quantity) /100 ,0) 
        end) as total
        ,i.currency_code as currency_code
        , null as percentage
        FROM sales_invoice i INNER JOIN third_party t ON (i.third_party_id = t.id)
        left JOIN sales_invoice_price id ON id.sales_invoice_id=i.id
        LEFT JOIN product p ON p.id=id.product_id
        LEFT JOIN tenant_parameter_value tp ON tp.id=p.group_id
        where i.tenant_code = '" . $tenant . "'  
        and ((month(i.date)=".$month." and  ".$month." != 0 ) or  ".$month." = 0 )
        and ((".$fiscalYearFirstMonth."= 1 and  year(i.date)=".$year."   )
        OR   (".$fiscalYearFirstMonth."!= 1 and  (i.date) >= '".$year."-".$fiscalYearFirstMonth."-01' and  (i.date)< '".$nextyear."-".$fiscalYearFirstMonth."-01' ))
        and  ((coalesce(p.group_id,0) =".$product_group_id." and ".$product_group_id."!=-1 ) OR ".$product_group_id."=-1) ";
        $sql =  $sql . "GROUP BY i.currency_code, year(i.date) ORDER by year(i.date),(case when ".$month." != 0 then month(i.date) else 0 end)";
        $query = $this->getEntityManager()->getConnection()->query($sql);
        $result = $query->fetchAll();
        if (count($result)>0)
        {
            if ($product_group_id==-1 )$total=$result[0]["total"];
            else $total= NUMBER_FORMAT($result[0]["total"],0,',',' ');
           $currency_code=$result[0]["currency_code"];
        $error_code=null;
        if (count($result)>1)
        {
        $total=null;
        $currency_code='several_currencies';
        $error_code ='several_currencies';
        }
        return array("total" => $total,"currency_code" => $currency_code,"percentage" => $result[0]["percentage"],"error_code" => $error_code);
        }
        else return null;
        }
        
        public function getForecastCAByYearByMonth($tenant, $year,$fiscalYearFirstMonth, $month,$product_group_id){
            if (strlen($fiscalYearFirstMonth)==1)
            $fiscalYearFirstMonth="0".$fiscalYearFirstMonth;
            $nextyear=$year+1;
            $PriceVatIsIncluded = $this -> getPriceVatIsIncluded($tenant);
    
            if (strlen($period) == 4){
                $sql=$sql." (len(i.period)= 4 and document_date between  concat(i.period,'-01-01') and  concat(i.period,'-12-31') ";
            }
            else
            {
                if (strpos($period, 't') === false)
                {
                $sql=$sql." and document_date between '".$period."-01' and '".$period."-31' ";
                }
                else
                {
                $startMonthDate= str_replace("t4","10",str_replace("t3","07",str_replace("t2","04",str_replace("t1", "01",$period))));
                $endMonthDate= str_replace("t4","12",str_replace("t3","09",str_replace("t2","06",str_replace("t1", "03",$period)))) ;
                $sql=$sql." and document_date between '".$startMonthDate."-01' and '". $endMonthDate."-31' ";
               }
            }


            $sql =  "SELECT CASE (case when ".$month." != 0 then month(i.date) else 0 end)
            WHEN 1 THEN 'Jan'
            WHEN 2 THEN 'Fév'
            WHEN 3 THEN 'Mar'
            WHEN 4 THEN 'Avr'
            WHEN 5 THEN 'Mai'
            WHEN 6 THEN 'Jun'
            WHEN 7 THEN 'Jui'
            WHEN 8 THEN 'Aoû'
            WHEN 9 THEN 'Sep'
            WHEN 10 THEN 'Oct'
            WHEN 11 THEN 'Nov'
            WHEN 12 THEN 'Déc'
            ELSE Case $fiscalYearFirstMonth WHEN 1 then $year else CONCAT($year,'-',$year+1) end  END AS label, 
            (ROUND( i.amount/100 ,0)) as total
            ,i.currency_code as currency_code
            , null as percentage
            FROM forecast i INNER JOIN third_party t ON (i.third_party_id = t.id)
            LEFT JOIN product p ON p.id=id.product_id
            LEFT JOIN tenant_parameter_value tp ON tp.id=p.group_id
            where i.tenant_code = '" . $tenant . "' and i.type_code='sales_forecast' 
            and ((month(i.date)=".$month." and  ".$month." != 0 ) or  ".$month." = 0 )
            and ((".$fiscalYearFirstMonth."= 1 and  year(i.date)=".$year."   )
            OR   (".$fiscalYearFirstMonth."!= 1 and  (i.date) >= '".$year."-".$fiscalYearFirstMonth."-01' and  (i.date)< '".$nextyear."-".$fiscalYearFirstMonth."-01' ))
            and  ((coalesce(p.group_id,0) =".$product_group_id." and ".$product_group_id."!=-1 ) OR ".$product_group_id."=-1) ";
            $sql =  $sql . "GROUP BY i.currency_code, year(i.date) ORDER by year(i.date),(case when ".$month." != 0 then month(i.date) else 0 end)";
            $query = $this->getEntityManager()->getConnection()->query($sql);
            $result = $query->fetchAll();
            if (count($result)>0)
            {
                if ($product_group_id==-1 )$total=$result[0]["total"];
                else $total= NUMBER_FORMAT($result[0]["total"],0,',',' ');
               $currency_code=$result[0]["currency_code"];
            $error_code=null;
            if (count($result)>1)
            {
            $total=null;
            $currency_code='several_currencies';
            $error_code ='several_currencies';
            }
            return array("total" => $total,"currency_code" => $currency_code,"percentage" => $result[0]["percentage"],"error_code" => $error_code);
            }
            else return null;
            }
            

    public function getReportingCharge($tenant,$year, $fiscalYearFirstMonth){
   

      $charge_group_names = $this->getListChargeGroup($tenant);
        $finalResult=[];
        $product_group_name=[];
        $totalyear=$this-> getReportingChargeByYearByMonth($tenant, $year,$fiscalYearFirstMonth,  0,-1);
        $detailstotalyear=array("label" => 'Total');
        $detailstotalyear["year_total"] = $totalyear;
         $listmonth=$this->getListMonth($fiscalYearFirstMonth);
        for ($i = 0; $i < count($listmonth); $i++){
            $detailstotalyear[$listmonth[$i]["code"]]= $this->getReportingChargeByYearByMonth($tenant, $year, $fiscalYearFirstMonth,  $listmonth[$i]["number"],-1 );          
        } 
        $details=array("total" => $detailstotalyear);
        for ($numero = 0; $numero < count($charge_group_names); $numero++){
           $totalyear=$this-> getReportingChargeByYearByMonth($tenant, $year, $fiscalYearFirstMonth,  0,$charge_group_names[$numero]["charge_group_id"]);
           $detailsyear=array("label" =>$charge_group_names[$numero]["charge_group_name"]);
           $detailsyear["year_total"] =$totalyear;
             for ($i = 0; $i < count($listmonth); $i++){
                $detailsyear[$listmonth[$i]["code"]]= $this-> getReportingChargeByYearByMonth($tenant, $year, $fiscalYearFirstMonth,  $listmonth[$i]["number"],$charge_group_names[$numero]["charge_group_id"]  );          
             }
             $details[$charge_group_names[$numero]["charge_group_code"]]=$detailsyear;
        }
        return  $details;
  }


    public function getReportingChargeByYearByMonth($tenant, $year, $fiscalYearFirstMonth, $month,$charge_group_id){
        if (strlen($fiscalYearFirstMonth)==1)
        $fiscalYearFirstMonth="0".$fiscalYearFirstMonth;
        $nextyear=$year+1;
        $sql =  "SELECT CASE (case when ".$month." != 0 then month(i.date) else 0 end)
        WHEN 1 THEN 'Jan'
        WHEN 2 THEN 'Fév'
        WHEN 3 THEN 'Mar'
        WHEN 4 THEN 'Avr'
        WHEN 5 THEN 'Mai'
        WHEN 6 THEN 'Jun'
        WHEN 7 THEN 'Jui'
        WHEN 8 THEN 'Aoû'
        WHEN 9 THEN 'Sep'
        WHEN 10 THEN 'Oct'
        WHEN 11 THEN 'Nov'
        WHEN 12 THEN 'Déc'
        ELSE  Case $fiscalYearFirstMonth WHEN 1 then $year else CONCAT($year,'-',$year+1) end END AS label,   ROUND( SUM( i.total_amount_incl_tax-i.vat_amount ) /100  ,0) as total,null as percentage,
        i.currency_code as currency_code
        FROM purchase_invoice i INNER JOIN third_party t ON (i.third_party_id = t.id)
        LEFT JOIN tenant_parameter_value tp ON tp.id=i.charge_group_id
        WHERE  i.tenant_code = '" . $tenant . "'
        and ((month(i.date)=".$month." and  ".$month." != 0 ) or  ".$month." = 0 )
        and ((".$fiscalYearFirstMonth."= 1 and  year(i.date)=".$year."   )
        OR   (".$fiscalYearFirstMonth."!= 1 and  (i.date) >= '".$year."-".$fiscalYearFirstMonth."-01' and  (i.date)< '".$nextyear."-".$fiscalYearFirstMonth."-01' ))
        and  ((coalesce(i.charge_group_id,0)  =".$charge_group_id." and ".$charge_group_id."!=-1 ) OR ".$charge_group_id."=-1) ";
      $sql =  $sql . "GROUP BY i.currency_code,year(i.date) ORDER by  year(i.date),(case when ".$month." != 0 then month(i.date) else 0 end)";
      //dd($sql);
      $query = $this->getEntityManager()->getConnection()->query($sql);
      $result = $query->fetchAll();
      if (count($result)>0)
      { 
        if ($charge_group_id==-1)$total=$result[0]["total"];
        else $total= NUMBER_FORMAT($result[0]["total"],0,',',' ');
        $currency_code=$result[0]["currency_code"];
        $error_code=null;
        if (count($result)>1)
        {
        $total=null;
        $currency_code='several_currencies';
        $error_code ='several_currencies';
        }
      return array("total" => $total ,"currency_code" =>$currency_code ,"percentage" => $result[0]["percentage"],"error_code" => $error_code);
      }
      else return null;
  }
    public function getListProductGroup($tenant){
        $sql =  "SELECT DISTINCT COALESCE(tp.id ,0) AS product_group_id,  COAlESCE(tp.value_code ,'not_qualified')AS product_group_code ,COALESCE(tpt.name,'Non qualifié') AS product_group_name  
        FROM tenant_parameter_value tp 
        LEFT JOIN tenant_parameter_value_translation tpt ON tpt.value_id=tp.id AND tpt.locale='fr'
        WHERE  tp.entity_code='product' and tp.parameter_code='group'  and tp.tenant_code = '" . $tenant . "'
        union 
        SELECT 0, 'not_qualified', 'Non qualifié' ";
   
    $query = $this->getEntityManager()->getConnection()->query($sql);
    $result = $query->fetchAll();
    return $result;
    }

    public function getListChargeGroup($tenant){
        $sql =  "SELECT DISTINCT COALESCE(tp.id ,0) AS charge_group_id,  COAlESCE(tp.value_code ,'not_qualified')AS charge_group_code ,COALESCE(tpt.name,'Non qualifié') AS charge_group_name  
        FROM tenant_parameter_value tp 
        LEFT JOIN tenant_parameter_value_translation tpt ON tpt.value_id=tp.id AND tpt.locale='fr'
        WHERE  tp.entity_code='charge' and tp.parameter_code='group'  and tp.tenant_code = '" . $tenant . "'
        union 
        SELECT 0, 'not_qualified', 'Non qualifié' ";
   
    $query = $this->getEntityManager()->getConnection()->query($sql);
    $result = $query->fetchAll();
    return $result;
    }

    function getListMonth($fiscalYearFirstMonth)
    {   $listMonth=[];
        for($m=1; $m<=12; ++$m){
        $listMonth[$m-1]["number"]=$m+$fiscalYearFirstMonth-1;
        $mois=$m;
        if ($fiscalYearFirstMonth!=1 && $m+$fiscalYearFirstMonth-1<=12)
           $mois=$m+$fiscalYearFirstMonth-1;
        else 
           $mois=$m+$fiscalYearFirstMonth-1-12;
        $listMonth[$m-1]["code"]=date('F', mktime(0, 0, 0, $mois, 1));
        switch ( $listMonth[$m-1]["code"]) {
        case "January": $label="Jan";break;
        case "February": $label='Fév' ;break;
        case "March": $label='Mar' ;break;
        case "April":$label= "Avr" ;break;
        case "May":$label= "Mai" ;break;
        case "June":$label= "Jun" ;break;
        case "July":$label= "Jui" ;break;
        case "August":$label= "Aoû" ;break;
        case "September":$label= "Sep" ;break;
        case "October":$label= "Oct" ;break;
        case "November":$label= "Nov" ;break;
        case "December":$label= "Déc" ;break;}

        $listMonth[$m-1]["label"]=$label;
    }
   return  $listMonth;
  }
  
  public function getFiscalier($tenant)
  {
    $sql =  "SELECT fiscal_year_first_month  FROM tenant
    WHERE code = '" . $tenant . "'";
    $query = $this->getEntityManager()->getConnection()->query($sql);
    $result = $query->fetchAll();
    return $result[0]["fiscal_year_first_month"];
  }
  public function getPriceVatIsIncluded($tenant)
  {
    $sql =  "SELECT price_vat_included  FROM tenant
    WHERE code = '" . $tenant . "'";
    $query = $this->getEntityManager()->getConnection()->query($sql);
    $result = $query->fetchAll();
    return $result[0]["price_vat_included"];
  }
  function unique_multidim_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();
   
    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}

public function calculateAmountDuSalesInvoice(){
    $sql = "SELECT si.id,t.name AS tenant_name, si.third_party_id, tp.name customer_name, si.number, 
    (COALESCE(si.total_amount_incl_tax,0) - ( COALESCE(SUM(sip.amount),0) + COALESCE(SUM(si1.total_amount_incl_tax),0) ))/100 As amount_due_calculate, 
    COALESCE(si.remaining_amount_due,0)/100 AS remaining_amount_due, COALESCE(si.total_amount_incl_tax,0)/100 AS total_amount_incl_tax, si.currency_code
    FROM sales_invoice si
    LEFT JOIN sales_invoice_payment sip ON sip.sales_invoice_id = si.id
    LEFT JOIN sales_invoice si1 ON si.related_sales_invoice_id = si1.id
    LEFT JOIN third_party tp ON tp.id = si.third_party_id
    LEFT JOIN tenant t ON t.code = si.tenant_code 
    GROUP BY si.tenant_code, si.third_party_id , si.id";
    $query = $this->getEntityManager()->getConnection()->query($sql);
    $result = $query->fetchAll();
    return $result;
}

}
