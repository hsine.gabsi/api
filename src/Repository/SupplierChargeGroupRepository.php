<?php

namespace App\Repository;

use App\Entity\SupplierChargeGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SupplierChargeGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method SupplierChargeGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method SupplierChargeGroup[]    findAll()
 * @method SupplierChargeGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SupplierChargeGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupplierChargeGroup::class);
    }

    // /**
    //  * @return SupplierChargeGroup[] Returns an array of SupplierChargeGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SupplierChargeGroup
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
