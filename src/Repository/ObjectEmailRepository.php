<?php

namespace App\Repository;

use App\Entity\ObjectEmail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectEmail|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectEmail|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectEmail[]    findAll()
 * @method ObjectEmail[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectEmailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectEmail::class);
    }

    // /**
    //  * @return ObjectEmail[] Returns an array of ObjectEmail objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectEmail
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
