<?php

namespace App\Repository;

use App\Entity\ObjectMember;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectMember|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectMember|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectMember[]    findAll()
 * @method ObjectMember[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectMemberRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectMember::class);
    }

    // /**
    //  * @return ObjectMember[] Returns an array of ObjectMember objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectMember
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
