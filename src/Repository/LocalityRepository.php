<?php

namespace App\Repository;

use App\Entity\Locality;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Locality|null find($id, $lockMode = null, $lockVersion = null)
 * @method Locality|null findOneBy(array $criteria, array $orderBy = null)
 * @method Locality[]    findAll()
 * @method Locality[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Locality::class);
    }

    public function countryCalling($locale)
    {
        return $this->createQueryBuilder('l')
            ->select("l.id AS countryId", "CONCAT('(', l.calling, ') ', t.name) AS calling")
            ->innerJoin(
                'App\Entity\LocalityTranslation',
                't',
                'WITH',
                'l.id = t.locality'
            )
            ->andWhere('l.type = :val')
            ->setParameter('val', 'country')
            ->andWhere('t.locale = :loc')
            ->setParameter('loc', $locale)
            ->orderBy('t.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getLocalityByCallingCode($callingCode)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.type = :val')
            ->setParameter('val', 'country')
            ->andWhere('l.calling = :callingCode')
            ->setParameter('callingCode', $callingCode)
            ->getQuery()
            ->getResult();
    }

    public function getMinLocality($language_code, $name, $type_codes, $tenant_code)
    {
        $type_codes_condition = "";
        $type_codes_parameters = [];

        if (isset($type_codes)) {
            $type_codes = explode(",", $type_codes);
            for ($i = 0; $i < count($type_codes); $i++) {
                //type_codes where condition
                $type_codes_condition = $type_codes_condition . "(l.type=:type_codes" . $i . ")";
                // set type_codes parameters
                $type_codes_parameters = array_merge($type_codes_parameters, ["type_codes" . $i => $type_codes[$i]]);
                if ($i + 1 < count($type_codes)) {
                    $type_codes_condition = $type_codes_condition . " or ";
                }
            }
        } else {
            $type_codes_condition = "(:type_codes='null')";
            $type_codes_parameters = ["type_codes" => 'null'];
        }

        $params = [];
        // $params concatenation
        if (count($type_codes_parameters) > 0) {
            $params = array_merge($params, $type_codes_parameters);
        }
        $params = array_merge($params, ['loc' => $language_code], ['name' => '%' . $name . '%'], ['tenant_code' => $tenant_code]);

        return $this->createQueryBuilder('l')
            ->select("l.id AS id", "t.name AS name","l.code AS code", "IDENTITY(l.type) AS type_code","IDENTITY(l.tenant) AS tenant_code", "tp.name AS parent_name")
            ->innerJoin(
                'App\Entity\LocalityTranslation',
                't',
                'WITH',
                'l.id = t.locality and t.locale=:loc'
            )
            ->leftJoin(
                'App\Entity\LocalityTranslation',
                'tp',
                'WITH',
                "l.parent = tp.locality AND tp.locale=:loc"
            )
            ->andWhere('t.name like :name')
            ->andWhere($type_codes_condition)
            ->andWhere("(l.tenant = :tenant_code OR l.tenant='advences') OR ( :tenant_code='null' and l.tenant='advences' )")
            ->setParameters($params)
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Locality[] Returns an array of Locality objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Locality
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
