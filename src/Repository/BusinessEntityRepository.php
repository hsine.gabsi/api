<?php

namespace App\Repository;

use App\Entity\BusinessEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BusinessEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method BusinessEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method BusinessEntity[]    findAll()
 * @method BusinessEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BusinessEntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BusinessEntity::class);
    }

    // /**
    //  * @return BusinessEntity[] Returns an array of BusinessEntity objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BusinessObject
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
