<?php

namespace App\Repository;

use App\Entity\ObjectTag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectTag[]    findAll()
 * @method ObjectTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectTagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectTag::class);
    }
    
    public function findByObjectID($objectId, $languageCode = 'fr', $entityCode = '', $tenantCode = '')        
    {
        return $this->createQueryBuilder('t1')
            ->innerJoin(
                'App\Entity\TenantParameterValue',
                't2',
                'WITH',
                't1.tag = t2.id'
            ) 
            ->andWhere('t1.objectId = :objectId')
            ->setParameter('objectId', $objectId)
            ->andWhere('t2.entity = :_entityCode')
            ->setParameter('_entityCode', $entityCode)        
            ->andWhere('t2.tenant = :_tenantCode')
            ->setParameter('_tenantCode', $tenantCode)        
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return ObjectTag[] Returns an array of ObjectTag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectTag
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
