<?php

namespace App\Repository;

use App\Entity\BankAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BankAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankAccount[]    findAll()
 * @method BankAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankAccountRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankAccount::class);
    }
	
	public function getMinBankAccounts($name, $tenant_code) {//$thirdParty, 
        $queryBuilder = $this->createQueryBuilder('b')
                            ->select("b.id", "b.name", "b.bankName", "b.iban", "b.bic", "c.code AS currency")
                            ->innerJoin(
                                'App\Entity\Currency',
                                'c',
                                'WITH',
                                'c.code = b.currency')
                            ->where('b.tenant = :tenant_code')
                            ->setParameter('tenant_code', $tenant_code);
							
        if(!is_null($name) and $name !== 'null') {
            $queryBuilder->andWhere('b.name = :name')
                        ->setParameter('name', $name);
        }
		/*if(!is_null($thirdParty) and $thirdParty !== 'null') {
            $queryBuilder->innerJoin(
                'App\Entity\ThirdParty',
                't',
                'WITH',
                't.id = b.thirdParty'
            )
            ->andWhere('b.thirdParty = :thirdParty')
            ->setParameter('thirdParty', $thirdParty);
        }else{
            $queryBuilder->andWhere("b.thirdParty is null");
		}*/
        return $queryBuilder->orderBy('b.name', 'ASC')
                            ->getQuery()
                            ->getResult();
    }
    // /**
    //  * @return BankAccount[] Returns an array of BankAccount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BankAccount
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
