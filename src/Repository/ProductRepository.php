<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }
	
	public function getMinProducts($language_code, $type_code, $name, $tenant_code)
    {
        $params=  ['language_code'=>$language_code,'type_code'=>$type_code,'name' => '%' . $name . '%','tenant_code'=>$tenant_code];
		
        $queryBuilder = $this->createQueryBuilder('p')
		->select("p.id as id","pt.name as name","p.vatRate","th.vatExempt as supplier_vatExempt","p.active","COALESCE(p.salesUnitPrice/100, 0) as salesUnitPrice","COALESCE(p.purchaseUnitPrice/100, 0) as purchaseUnitPrice","c1.code as salesCurrencyCode","c2.code as purchaseCurrencyCode")
		->innerJoin(
			'App\Entity\ProductTranslation',
			'pt',
			'WITH',
			"p.id=pt.product AND pt.locale=:language_code"
        )		
        ->leftJoin(
			'App\Entity\ThirdParty',
			'th',
			'WITH',
			"p.supplier=th.id"
        )
		->leftJoin(
			'App\Entity\Currency',
			'c1',
			'WITH',
			"c1.code=p.salesCurrency"
        )
		->leftJoin(
			'App\Entity\Currency',
			'c2',
			'WITH',
			"c2.code=p.purchaseCurrency"
        );
		/*->leftJoin(
			'App\Entity\ProductPrice',
			'pr',
			'WITH',
			"p.id=pr.product AND pr.type='exclude_taxes' AND pr.sales=true"
        );*/
		
		$queryBuilder->where("p.type =:type_code or :type_code='null'")// p.active = true
		->andWhere('pt.name like :name')
		// mandatory parameters (tenant and language_code)
		->andWhere('p.tenant=:tenant_code')		
        ->setParameters($params)
		->groupBy('p.id');

        return $queryBuilder->getQuery()->getResult();
	}
	
    // /**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
