<?php

namespace App\Repository;

use App\Entity\PaymentBankOperation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PaymentBankOperation|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentBankOperation|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentBankOperation[]    findAll()
 * @method PaymentBankOperation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentBankOperationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentBankOperation::class);
    }

    // /**
    //  * @return PaymentBankOperation[] Returns an array of PaymentBankOperation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaymentBankOperation
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
