<?php

namespace App\Repository;

use App\Entity\MasterCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MasterCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method MasterCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method MasterCode[]    findAll()
 * @method MasterCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MasterCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MasterCode::class);
    }

    // /**
    //  * @return MasterCode[] Returns an array of MasterCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MasterCode
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
