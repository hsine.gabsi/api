<?php

namespace App\Repository;

use App\Entity\ThirdParty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ThirdParty|null find($id, $lockMode = null, $lockVersion = null)
 * @method ThirdParty|null findOneBy(array $criteria, array $orderBy = null)
 * @method ThirdParty[]    findAll()
 * @method ThirdParty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ThirdPartyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ThirdParty::class);
    }


    public function getMinThirdParties($type_code, $name, $tenant_code,$first_name) {
        $queryBuilder = $this->createQueryBuilder('t')
                            ->select("t.id", "t.name AS name","t.firstName AS first_name")
                            ->where('t.active = true')
                            ->andWhere('t.tenant = :tenant_code')
                            ->setParameter('tenant_code', $tenant_code);
        if (!is_null($type_code)) {
            $queryBuilder->andWhere('t.type = :type_code')
                        ->setParameter('type_code', $type_code);
        }
        if (!is_null($name)) {
            $queryBuilder->andWhere('t.name like :name')
                        ->setParameter('name', '%'.$name.'%');
        }
        if (!is_null($first_name)) {
            $queryBuilder->andWhere('t.firstName like :first_name')
                        ->setParameter('first_name', '%'.$first_name.'%');
        }
        return $queryBuilder->orderBy('t.name' ,'ASC','t.firstName' ,'ASC')
                            ->getQuery()
                            ->getResult();

        /// Whenever you add an argument add it this params array
        // $params = ['type_code' => $type_code, 'tenant_code' => $tenant_code, 'name' => '%' . $name . '%'];
        // return $this->createQueryBuilder('t')
        //                 ->select("t.id", "t.name AS name")
        //                 ->where('t.active = true')
        //                 ->andWhere('t.type = :type_code')
        //                 ->andWhere('t.tenant = :tenant_code')
        //                 ->andWhere('t.name like :name')
        //                 ->setParameters($params)
        //                 ->orderBy('t.name', 'ASC')
        //                 ->getQuery()
        //                 ->getResult();
    }

    // /**
    //  * @return ThirdParty[] Returns an array of ThirdParty objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ThirdParty
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getNbRecordByTenantByTable($table_names ,$tenant_code) {
        if(in_array($table_names, array('payment-received','payment-sent','contract-sales','contract-purchase',
        'third_party-customer','third_party-supplier','third_party-other')))  { 
        
            if($table_names == 'payment-received') {
                $table_condition = "AND tmp.inbound=1";
            }
            elseif ($table_names == 'payment-sent') {
                $table_condition = "AND tmp.inbound=0";
            }
            elseif ($table_names == 'contract-sales') {
                $table_condition = "AND tmp.sales=0";
            }
            elseif ($table_names == 'contract-purchase') {
                $table_condition = "AND tmp.sales=1";
            }
            elseif ($table_names == 'third_party-customer') {
                $table_condition = "AND tmp.type_code='customer'";
            }
            elseif ($table_names == 'third_party-supplier') {
                $table_condition = "AND tmp.type_code='supplier'";
            }
            elseif ($table_names == 'third_party-other') {
                $table_condition = "AND tmp.type_code!='supplier' AND tmp.type_code!='customer'";
            }
            $table_names = explode("-", $table_names);
            $table_names =  $table_names[0];

        }else{
            $table_condition = "";
        }

            $sql = "SELECT COUNT(tmp.id) As nb_record
            FROM  tenant t 
            LEFT JOIN $table_names tmp ON t.code = tmp.tenant_code
            WHERE tmp.tenant_code = '$tenant_code' $table_condition";
            $sql =  $sql . " GROUP BY t.code ORDER by t.code";

          
            $query = $this->getEntityManager()->getConnection()->query($sql);
            $result = $query->fetchAll();
            if (count($result)>0)
            {
            $nb_record=$result[0]["nb_record"];
             return array("nb_record" => $nb_record);
            } else{
            $nb_record=0;
             return array("nb_record" => $nb_record);
            }
        }

    public function getDashNbRecord(){
    
         $tenant_codes = $this->getListeTenant();
         $table_names ="sales_invoice,purchase_invoice,payment-received,payment-sent,third_party-customer,third_party-supplier,third_party-other,contract-sales,contract-purchase,quote,sales_order,bank_operation";
         $table_codes ="sales_invoice,purchase_invoice,payment_received,payment_sent,third_party_customer,third_party_supplier,third_party_other,contract_sales,contract_purchase,quote,sales_order,bank_operation";
         $table_names = explode(",", $table_names); 
         $table_codes = explode(",", $table_codes); 
    
        for ($numero = 0; $numero < count($tenant_codes); $numero++){
         
            for ($i = 0; $i < count($table_names); $i++){
                
                $detailsyear[$table_codes[$i]]= $this-> getNbRecordByTenantByTable($table_names[$i],$tenant_codes[$numero]["tenant_code"] );          
            }
             $details[$tenant_codes[$numero]["tenant_code"]]=$detailsyear;
        }
        return  $details;

     }
 
    
public function getListeTenant(){
$sql =  "SELECT code As tenant_code, name As tenant_name FROM tenant";
$query = $this->getEntityManager()->getConnection()->query($sql);
$result = $query->fetchAll();
return $result;
}


public function calculateCustomerBalance($customer_id,$tenant_code) {
    $sql = "SELECT MAX(b.date) AS date_solde_initial, COALESCE(b.currency_code, 'EUR') AS currency_code
    FROM  balance b 
    WHERE b.object_code = 'customer' AND b.object_id = $customer_id AND b.tenant_code = '" . $tenant_code . "'";    
    $query = $this->getEntityManager()->getConnection()->query($sql);
    $result = $query->fetchAll();
    $date_solde_initial = $result[0]["date_solde_initial"];
    $currency_code = $result[0]["currency_code"];

    if($date_solde_initial !=null) {
        $conditionDateEgale = " AND date = '$date_solde_initial'";
        $conditionDateSupEgale = " AND date >= '$date_solde_initial'";
    }
    else{
        $conditionDateEgale = "";
        $conditionDateSupEgale="";
    }
        
    // 1- Prendre la ligne de la table balance qui correspond au client en input et qui a la date la plus récente => solde initial 
    $solde_initial = "SELECT COALESCE(b.value,0)/100 AS solde_initial
    FROM  balance b 
    WHERE b.object_code = 'customer' AND b.object_id = $customer_id AND  b.tenant_code = '" . $tenant_code . "' 
    $conditionDateEgale AND b.currency_code = '$currency_code'";    
    $query = $this->getEntityManager()->getConnection()->query($solde_initial);
    $solde_initial = $query->fetchAll();
    if ( count($solde_initial) > 0 ){
    $solde_initial = $solde_initial[0]["solde_initial"];
    }
    else{
    $solde_initial='0';
    }
    //dd( $solde_initial);
       
    //2- Prendre les factures de vente (sales_invoice) du tiers dont la date est >= A la date du solde initial 
    $total_sales_invoice = "SELECT COALESCE(SUM(si.total_amount_incl_tax),0)/100 AS total_sales_invoice
    FROM  sales_invoice si
    WHERE si.third_party_id = $customer_id $conditionDateSupEgale
    AND si.tenant_code = '" . $tenant_code . "' AND si.currency_code = '$currency_code'";    
    $query = $this->getEntityManager()->getConnection()->query($total_sales_invoice);
    $total_sales_invoice = $query->fetchAll();
    if ( count($total_sales_invoice) > 0 ){
        $total_sales_invoice = $total_sales_invoice[0]["total_sales_invoice"];
        }
    else{
        $total_sales_invoice='0';
        }
    //dd($total_sales_invoice);

    // 3- Prendre avoirs (table sales_invoice, credit_note =1) du tiers dont la date est >= A la date du solde initial 
    $total_credit = "SELECT COALESCE(SUM(si.total_amount_incl_tax),0)/100 AS total_credit
    FROM  sales_invoice si
    WHERE si.third_party_id = $customer_id $conditionDateSupEgale AND si.credit_note = 1
    AND   si.tenant_code = '" . $tenant_code . "' AND si.currency_code = '$currency_code'";    
    $query = $this->getEntityManager()->getConnection()->query($total_credit);
    $total_credit = $query->fetchAll();
    if ( count($total_credit) > 0 ){
        $total_credit = $total_credit[0]["total_credit"];
        }
        else{
        $total_credit='0';
        }
    // dd( $total_credit);

   // 4- Prendre les payments entrant du tiers dont la date est >= A la date du solde initial  
   $total_payment_received = "SELECT COALESCE(SUM(p.amount),0)/100 AS total_payment_received
   FROM  payment p
   WHERE p.third_party_id = $customer_id $conditionDateSupEgale AND p.inbound=1
   AND   p.tenant_code = '" . $tenant_code . "' AND p.currency_code = '$currency_code'";    
   $query = $this->getEntityManager()->getConnection()->query($total_payment_received);
   $total_payment_received = $query->fetchAll();
   if ( count($total_payment_received) > 0 ){
    $total_payment_received = $total_payment_received[0]["total_payment_received"];
    }
    else{
    $total_payment_received='0';
    }
   //dd($total_payment_received);

   // 5- Prendre les payments sortants du tiers dont la date est >= A la date du solde initial 
   $total_payment_sent = "SELECT COALESCE(SUM(p.amount),0)/100 AS total_payment_sent
   FROM  payment p
   WHERE p.third_party_id = $customer_id $conditionDateSupEgale AND p.inbound=0
   AND   p.tenant_code = '" . $tenant_code . "' AND p.currency_code = '$currency_code'";    
   $query = $this->getEntityManager()->getConnection()->query($total_payment_sent);
   $total_payment_sent = $query->fetchAll();
   if ( count($total_payment_sent) > 0 ){
    $total_payment_sent = $total_payment_sent[0]["total_payment_sent"];
    }
    else{
    $total_payment_sent='0';
    }
   //dd($total_payment_sent);

   //Solde client = Solde initial + Somme pt 4 + Somme pt 3 - Somme pt 2 - Somme pt 5
   $balance_amount = $solde_initial + $total_payment_received + $total_credit - $total_sales_invoice - $total_payment_sent;
   return array("customer_balance" => number_format($balance_amount, 2, '.', ' '),"currency_code" => $currency_code);
}

}