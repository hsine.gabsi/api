<?php

namespace App\Repository;

use App\Entity\ArticleTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ArticleTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticleTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticleTranslation[]    findAll()
 * @method ArticleTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArticleTranslation::class);
    }
    
    public function findFilter($tenantCode = '', $languageCode = 'fr', $articleId = '')
    {
        $queryBuilder = $this->createQueryBuilder('t1');
        $queryBuilder->select('t1')
            ->innerJoin(
                'App\Entity\Article',
                't2',
                'WITH',
                't2.id = t1.article'
            ) 
        ->andWhere('t2.public = 1')
        ->andWhere('t2.tenant = :_tenantCode')
        ->setParameter('_tenantCode', $tenantCode)          
        ->andWhere('t1.locale= :_languageCode')
        ->setParameter('_languageCode', $languageCode);
        if ($articleId) {
            $queryBuilder
            ->andWhere('t2.id = :_articleId')
            ->setParameter('_articleId', $articleId);
        };  

        $result = $queryBuilder->getQuery()->getResult();
        return $result;
    }

    // /**
    //  * @return ArticleTranslation[] Returns an array of ArticleTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ArticleTranslation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
