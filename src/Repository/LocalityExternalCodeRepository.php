<?php

namespace App\Repository;

use App\Entity\LocalityExternalCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LocalityExternalCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method LocalityExternalCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method LocalityExternalCode[]    findAll()
 * @method LocalityExternalCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocalityExternalCodeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LocalityExternalCode::class);
    }

    // /**
    //  * @return LocalityExternalCode[] Returns an array of LocalityExternalCode objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LocalityExternalCode
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
