<?php

namespace App\Repository;

use App\Entity\BankCreditReimbursement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BankCreditReimbursement|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankCreditReimbursement|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankCreditReimbursement[]    findAll()
 * @method BankCreditReimbursement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankCreditReimbursementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankCreditReimbursement::class);
    }

    // /**
    //  * @return BankCreditReimbursement[] Returns an array of BankCreditReimbursement objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BankCreditReimbursement
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
