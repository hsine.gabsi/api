<?php

namespace App\Repository;

use App\Entity\PurchaseInvoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PurchaseInvoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method PurchaseInvoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method PurchaseInvoice[]    findAll()
 * @method PurchaseInvoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PurchaseInvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PurchaseInvoice::class);
    }

    // /**
    //  * @return PurchaseInvoice[] Returns an array of PurchaseInvoice objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TicketDate
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function gettoBepaid($tenant){
        $sql = "SELECT DATE_FORMAT(date,'%d/%m/%Y') AS date, purchase_invoice.third_party_id, third_party.name name, third_party.first_name firstname,(remaining_amount_due)/100 amount, check_type_code, master_code_translation.name as check_type, currency_code  FROM purchase_invoice
        LEFT JOIN third_party ON third_party.id = purchase_invoice.third_party_id
        LEFT JOIN master_code_translation ON master_code_translation.code = purchase_invoice.check_type_code AND master_code_translation.locale = 'fr'
        WHERE purchase_invoice.tenant_code = '" . $tenant . "' 
        AND due_date <= CURDATE() + INTERVAL 7 DAY AND payment_status_type_code <> 'paid'
        ORDER BY purchase_invoice.date DESC";
        
        $query = $this->getEntityManager()->getConnection()->query($sql);
        $result = $query->fetchAll();
        if (count($result)>0)
        {
            $total = 0;
            $details = array();
            for ($numero = 0; $numero < count($result); $numero++){
                $details[$numero]["amount"] = NUMBER_FORMAT($result[$numero]["amount"],2,'.',' ');
                $details[$numero]["check_type_code"] = $result[$numero]["check_type_code"];
                $details[$numero]["check_type"] = $result[$numero]["check_type"];
                $details[$numero]["currency_code"] = $result[$numero]["currency_code"];
                $details[$numero]["name"] = $result[$numero]["name"];
                $details[$numero]["firstname"] = $result[$numero]["firstname"];
                $details[$numero]["third_party_id"] = $result[$numero]["third_party_id"];
                $details[$numero]["date"] = $result[$numero]["date"];
            }
            return $details;
        }
        else return null;
    }
}
