<?php

namespace App\Repository;

use App\Entity\SupplierChargeType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SupplierChargeType|null find($id, $lockMode = null, $lockVersion = null)
 * @method SupplierChargeType|null findOneBy(array $criteria, array $orderBy = null)
 * @method SupplierChargeType[]    findAll()
 * @method SupplierChargeType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SupplierChargeTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SupplierChargeType::class);
    }

    // /**
    //  * @return SupplierChargeType[] Returns an array of SupplierChargeType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SupplierChargeType
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
