<?php

namespace App\Repository;

use App\Entity\MasterCodeTranslation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MasterCodeTranslation|null find($id, $lockMode = null, $lockVersion = null)
 * @method MasterCodeTranslation|null findOneBy(array $criteria, array $orderBy = null)
 * @method MasterCodeTranslation[]    findAll()
 * @method MasterCodeTranslation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MasterCodeTranslationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MasterCodeTranslation::class);
    }

    // /**
    //  * @return MasterCodeTranslation[] Returns an array of MasterCodeTranslation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MasterCodeTranslation
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
