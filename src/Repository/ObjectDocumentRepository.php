<?php

namespace App\Repository;

use App\Entity\ObjectDocument;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectDocument|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectDocument|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectDocument[]    findAll()
 * @method ObjectDocument[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectDocumentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectDocument::class);
    }

    // /**
    //  * @return ObjectDocument[] Returns an array of ObjectDocument objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectDocument
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
