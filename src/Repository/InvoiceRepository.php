<?php

namespace App\Repository;

use App\Entity\Invoice;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Invoice|null find($id, $lockMode = null, $lockVersion = null)
 * @method Invoice|null findOneBy(array $criteria, array $orderBy = null)
 * @method Invoice[]    findAll()
 * @method Invoice[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Invoice::class);
    }

    public function findNextNumberInvoice($tenant, $yearOfSend){
        $lastinvoice =    $this->createQueryBuilder("t")
                            ->select("t.number")
                            ->where("t.tenant = :tenant")
                            ->andWhere('t.outbound = :outbound')
                            ->setParameter ("tenant", $tenant)
                            ->setParameter ("outbound", 1)
                            ->orderBy("t.id","DESC")
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getResult();

        if (count($lastinvoice) > 0){
            $lastNumber = $lastinvoice[0]["number"];
            $lastNumber = (int)explode("-", $lastNumber)[1];
            $lastNumber = $lastNumber + 1;
        }else{ 
            $lastNumber = 1;        
        }
        $lastNumber = str_pad($lastNumber, 4, '0', STR_PAD_LEFT);
        return $yearOfSend . '-' . $lastNumber;
    }
    public function getReporting($tenant){
        $finalResult=[];
        $finalResult[0]["Product"]= $this ->getReportingCA($tenant);
        $finalResult[0]["Charge"] = null;//$this ->getReportingCharge($tenant);
        return $finalResult;
    }
    
    public function getReportingCA($tenant){
          /*$sql =  "SELECT SUM( (CASE WHEN i.credit_note=0 then id.unit_price else (-id.unit_price) END) * quantity) /100  as total
           ,COAlESCE(tp.value_code ,'not_qualified')AS product_group_code ,COALESCE(tpt.name,'Non qualifié') AS product_group_name , year(i.date) AS year, month(i.date) AS month 
          FROM invoice i INNER JOIN third_party t ON (i.third_party_id = t.id)
          left JOIN invoice_detail id ON id.invoice_id=i.id
          LEFT JOIN product p ON p.id=id.product_id
          LEFT JOIN tenant_parameter_value tp ON tp.id=p.group_id
          LEFT JOIN tenant_parameter_value_translation tpt ON tpt.value_id=tp.id AND tpt.locale='fr'
          where i.outbound=1 and i.tenant_code = '" . $tenant . "'";
        $sql =  $sql . "GROUP BY month(i.date),year(i.date), tpt.name ORDER by  year(i.date),month(i.date),tpt.name";
        //dd($sql);
        $query = $this->getEntityManager()->getConnection()->query($sql);
        $result = $query->fetchAll();*/
        $product_group_names = $this->getListProductGroup($tenant);
        $finalResult=[];
        $product_group_name=[];
        $details=[];
     //   $details[0]["total"]=$result;
        for ($numero = 0; $numero < count($product_group_names); $numero++){
            $detailsyear =[];
            $totalyear= [];
            $totalyear=$this-> getReportingCAByYearByMonth($tenant, 2020,  0,$product_group_names[$numero]["product_group_id"]);
            $detailsyear[0]["all"]=$totalyear;          
            $listmonth=$this->getListMonth();
             for ($i = 0; $i < count($listmonth); $i++){
                $detailsyear[$i+1][$listmonth[$i]["number"]]= $this-> getReportingCAByYearByMonth($tenant, 2020,  $listmonth[$i]["number"],$product_group_names[$numero]["product_group_id"]  );          
             }
             $product_group_names[$numero]["2020"]=$detailsyear;
             $details[$numero][$product_group_names[$numero]["product_group_code"]]=$product_group_names[$numero];
        }
      //  $product_group_names[0]["details"]=$result;
       
        
        return  $details;
    }
    public function getReportingCAByYearByMonth($tenant, $year, $month,$product_group_id){
        $sql =  "SELECT SUM( (CASE WHEN i.credit_note=0 then id.unit_price else (-id.unit_price) END) * quantity) /100  as total
        , null as percentage
        FROM invoice i INNER JOIN third_party t ON (i.third_party_id = t.id)
        left JOIN invoice_detail id ON id.invoice_id=i.id
        LEFT JOIN product p ON p.id=id.product_id
        LEFT JOIN tenant_parameter_value tp ON tp.id=p.group_id
        where i.outbound=1 and i.tenant_code = '" . $tenant . "'  
        and  year(i.date)=".$year."   and ((month(i.date)=".$month." and  ".$month." != 0 ) or  ".$month." = 0 )
        and  ((p.group_id =".$product_group_id." and ".$product_group_id."!=0 ) OR ".$product_group_id."=0) ";
        $sql =  $sql . "GROUP BY year(i.date) ORDER by  year(i.date),month(i.date)";
        $query = $this->getEntityManager()->getConnection()->query($sql);
        $result = $query->fetchAll();
  
        return $result;
    }

    public function getReportingCharge($tenant){
        $sql =  "SELECT SUM( i.total_amount_incl_tax ) /100  as total ,COAlESCE(tct.code ,'not_qualified')AS charge_type_code ,COALESCE(tct.name,'Non qualifié') AS charge_type_name , year(i.date) AS year, month(i.date) AS month 
        FROM invoice i INNER JOIN third_party t ON (i.third_party_id = t.id)
        LEFT JOIN master_parameter_value tc ON tc.code=i.charge_type_code
        LEFT JOIN master_code_translation tct ON tct.code=tc.code AND tct.locale='fr'
      WHERE i.outbound =0 and i.tenant_code = '" . $tenant . "'";
      $sql =  $sql . "GROUP BY month(i.date),year(i.date), tct.name ORDER by  year(i.date),month(i.date),tct.name";
      //dd($sql);
      $query = $this->getEntityManager()->getConnection()->query($sql);
      $result = $query->fetchAll();

      return $result;
  }
    public function getListProductGroup($tenant){
        $sql =  "SELECT DISTINCT COALESCE(tp.id ,0) AS product_group_id,  COAlESCE(tp.value_code ,'not_qualified')AS product_group_code ,COALESCE(tpt.name,'Non qualifié') AS product_group_name  
        FROM product p 
        LEFT JOIN tenant_parameter_value tp ON tp.id=p.group_id
        LEFT JOIN tenant_parameter_value_translation tpt ON tpt.value_id=tp.id AND tpt.locale='fr'
        WHERE p.tenant_code = '" . $tenant . "'";
   
    $query = $this->getEntityManager()->getConnection()->query($sql);
    $result = $query->fetchAll();
    return $result;
    }
  function getListMonth()
  { $listMonth=[];
      for($m=1; $m<=12; ++$m){
        $listMonth[$m-1]["number"]=$m;
        $listMonth[$m-1]["name"]=date('F', mktime(0, 0, 0, $m, 1));
   }
   return  $listMonth;
  }
  function unique_multidim_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();
   
    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}
}
