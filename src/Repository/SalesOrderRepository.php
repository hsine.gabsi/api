<?php

namespace App\Repository;

use App\Entity\SalesOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SalesOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method SalesOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method SalesOrder[]    findAll()
 * @method SalesOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SalesOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SalesOrder::class);
    }

    public function findNextNumber($tenant_code){
        $lastNumber = $this ->createQueryBuilder("t")
                            ->select("t.number")
                            ->where("t.tenant = :tenant_code")
                            ->setParameter ("tenant_code", $tenant_code)
                            ->orderBy("t.id","DESC")
                        ->setMaxResults(1)
                        ->getQuery()
                        ->getResult();
        
        
        if (count($lastNumber) > 0){
            $number = $lastNumber[0]["number"];
            $number = (int)$number;
            $number++;
        }else{ 
            $number = 1000;
        }
        return  $number;
    }

    // /**
    //  * @return SalesOrder[] Returns an array of SalesOrder objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SalesOrder
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
