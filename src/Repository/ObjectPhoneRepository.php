<?php

namespace App\Repository;

use App\Entity\ObjectPhone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ObjectPhone|null find($id, $lockMode = null, $lockVersion = null)
 * @method ObjectPhone|null findOneBy(array $criteria, array $orderBy = null)
 * @method ObjectPhone[]    findAll()
 * @method ObjectPhone[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ObjectPhoneRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ObjectPhone::class);
    }

    // /**
    //  * @return ObjectPhone[] Returns an array of ObjectPhone objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ObjectPhone
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
