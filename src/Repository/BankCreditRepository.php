<?php

namespace App\Repository;

use App\Entity\BankCredit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BankCredit|null find($id, $lockMode = null, $lockVersion = null)
 * @method BankCredit|null findOneBy(array $criteria, array $orderBy = null)
 * @method BankCredit[]    findAll()
 * @method BankCredit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BankCreditRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BankCredit::class);
    }

    // /**
    //  * @return BankCredit[] Returns an array of BankCredit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BankCredit
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
