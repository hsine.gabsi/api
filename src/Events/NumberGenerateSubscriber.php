<?php


namespace App\Events;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;

use App\Repository\TaskRepository;
use App\Entity\Task;
//use App\Repository\InvoiceRepository;
//use App\Entity\Invoice;
use App\Repository\SalesInvoiceRepository;
use App\Entity\SalesInvoice;
use App\Repository\QuoteRepository;
use App\Entity\Quote;

class NumberGenerateSubscriber implements EventSubscriberInterface 
{
	private $tokenStorage;
	protected $jwtManager;
	private $taskRepository;
	private $salesInvoiceRepository;
	private $quoteRepository;

	public function __construct(TokenStorageInterface $tokenStorage, TaskRepository $taskRepository, JWTTokenManagerInterface $jwtManager, SalesInvoiceRepository $salesInvoiceRepository, QuoteRepository $quoteRepository) {//InvoiceRepository $invoiceRepository, 
		$this->taskRepository = $taskRepository;
		$this->tokenStorage = $tokenStorage;
		$this->jwtManager = $jwtManager;
		$this->salesInvoiceRepository = $salesInvoiceRepository;
		$this->quoteRepository = $quoteRepository;
	}


	public static function getSubscribedEvents()
	{
		return [
			KernelEvents::VIEW => ['generateNumber',EventPriorities::PRE_VALIDATE]
		];

	}

	public function generateNumber(ViewEvent $event)
	{  
		$object   = $event->getControllerResult();
		$method = $event->getRequest()->getMethod();
	
		if($object instanceof Task && $method === "POST"){
			$decodedJwtToken = $this->jwtManager->decode($this->tokenStorage->getToken()); 
			$tenantCode = $decodedJwtToken['tenant'];

			$taskTypeCode = $object->getType()->getCode();
			if($object->getParent() == null){
			$parentId  = 'null';
			}else{
			$parentId = $object->getParent()->getId();
			}
			
			if($object->getBusinessUnit() == null){
			$businessUnitCode  = 'null';
			}else{
			$businessUnitCode = $object->getBusinessUnit()->getCode();
			}
			$number = $this->taskRepository->findNextNumber($tenantCode,$taskTypeCode,$parentId,$businessUnitCode);
			$object->setNumber($number);
		}
		
		if($object instanceof SalesInvoice && $method === "POST"){
			$decodedJwtToken = $this->jwtManager->decode($this->tokenStorage->getToken()); 
			$tenantCode = $decodedJwtToken['tenant'];
			//$yearOfSend = $object->getDate()->format("Y");
			$yearOfSend = (strlen($object->getNumber()) > 0)?explode('-',$object->getNumber())[0]:null;
			if(!is_numeric($yearOfSend)){
				$date = new \DateTime();
				$yearOfSend = $date->format('Y');
			}
			$number = $this->salesInvoiceRepository->findNextNumberSalesInvoice($tenantCode, $yearOfSend);
			$object->setNumber($number);
		}
		if($object instanceof Quote && ($method === "POST" || $method === "PUT")){
			$decodedJwtToken = $this->jwtManager->decode($this->tokenStorage->getToken()); 
			$tenantCode = $decodedJwtToken['tenant'];
			$yearOfSend = $object->getDate()->format("Y");
			if(!is_numeric($yearOfSend)){
				$date = new \DateTime();
				$yearOfSend = $date->format('Y');
			}
			if($method === "POST"){
				$number = $this->quoteRepository->findNextNumberQuote($tenantCode, $yearOfSend, 0);
			}else{
				$quoteId = $object->getId();
				$number = $this->quoteRepository->findNextNumberQuote($tenantCode, $yearOfSend, $quoteId);
			}
			$object->setNumber($number);
		}
	}
}
