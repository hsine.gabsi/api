<?php

namespace App\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
Use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\SalesOrder;
use App\Services\TokenInformation;
use App\Repository\SalesOrderRepository;
use App\Entity\PurchaseInvoice;
use App\Entity\SalesInvoice;
use App\Repository\MasterParameterValueRepository;

//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;


class ProcessPreWrite implements EventSubscriberInterface {
    /**  @var UserPasswordEncoderInterface  */
    private $encoder;
    private $salesOrderRepository;
    private $tokenInfos;
	//private $masterParameterValueRepository;
    public function  __construct(UserPasswordEncoderInterface $encoder, TokenInformation $tokenInfos, SalesOrderRepository $salesOrderRepository)//, MasterParameterValueRepository $masterParameterValueRepository
    {
        $this->encoder =$encoder;
        $this->salesOrderRepository = $salesOrderRepository;
        $this->tokenInfos = $tokenInfos;
		//$this->masterParameterValueRepository = $masterParameterValueRepository;
    }
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['onKernelView',EventPriorities::PRE_WRITE]
        ];
    }

    public function onKernelView(ViewEvent $event)
    {
        $method = $event->getRequest()->getMethod();
        $object = $event->getControllerResult();
        //user
        if ( ($method==="POST" || $method==="PUT") && $object instanceof User ){
            if($object->getPassword() != null && $object->getPassword() != ""){
                $hash = $this->encoder->encodePassword($object, $object->getPassword());
                $object-> setPassword($hash);
            }else{
                $object-> setPassword("");
            }
            if($object->getUsername() == null || $object->getUsername() == ""){
                $object->setUsername("0000");
            }
        }
        if ( $method === "POST" && $object instanceof SalesOrder){ 
            $tenantCode = $this->tokenInfos->getTenantCode();
            $number = $this->salesOrderRepository->findNextNumber($tenantCode);
            $object->setNumber($number);
        }
        //order
        // if ( $method === "POST" && $object instanceof Order){ 
        //     $tenantCode = $this->tokenInfos->getTenantCode();
        //     $number = $this->orderRepository->findNextNumber($tenantCode);
        //     $object->setNumber($number);
        // }
        //purchase invoice
        /*if ( $method === "POST" && $object instanceof PurchaseInvoice){
            $unpaid = $this->masterParameterValueRepository->find('unpaid');
            $object->setPaymentStatusType($unpaid->getCode());
            $object->setRemainingAmountDue($object->getTotalAmountInclTax());
        }*/
        //sales invoice
        /*if ( $method === "POST" && $object instanceof SalesInvoice){
			if($object->getPaymentStatusType() == null || $object->getPaymentStatusType() == ""){
				$mRepository = $this->masterParameterValueRepository->find('code' => 'unpaid');
				dd($mRepository);
				$object->setPaymentStatusType($mRepository->getCode());
			}
			if($object->getRemainingAmountDue() == null || $object->getRemainingAmountDue() == ""){
				$object->setRemainingAmountDue($object->getTotalAmountInclTax());
			}
        }*/
    }
}
