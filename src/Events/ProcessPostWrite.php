<?php

namespace App\Events;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use App\Repository\UserRepository;

class ProcessPostWrite implements EventSubscriberInterface
{
    private $repository;
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['afterPayment', EventPriorities::POST_WRITE],
        ];
    }

    public function afterPayment(ViewEvent $event): void
    {
        $method = $event->getRequest()->getMethod();
        $result = $event->getControllerResult();
        if ( $method === "POST" && $result instanceof User){
            if($result->getUsername() == "0000"){
                $this->repository->updateUsernameById($result->getId());
                $result->setUsername($result->getId());
            }
        }
    }
}