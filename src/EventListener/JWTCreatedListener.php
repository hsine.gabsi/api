<?php

// /src/AppBundle/Event/Listener/JWTCreatedListener.php

namespace App\EventListener;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\UserTenant;

class JWTCreatedListener
{
    protected $requestStack;
    private $repository;

    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;
        $this->repository = $entityManager->getRepository(UserTenant::class);
    }
    /**
     * Adds additional data to the generated JWT
     *
     * @param JWTCreatedEvent $event
     *
     * @return void
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        /** @var $user \AppBundle\Entity\User */
        $user = $event->getUser();
        $userId = $user->getId();

        $request = $this->requestStack->getCurrentRequest()->getContent();
        $rq = json_decode($request, true);

        if(!isset($rq["tenant"]) || $rq["tenant"] == ""){
            $userTenant = $this->repository->findBy(['user' => $userId]);
            $nbTenantOfUser = Count($userTenant); 
            if($nbTenantOfUser == 1){
                $tenantCode = $userTenant[0]->getTenant()->getCode();
                $fileStorageBucket = $userTenant[0]->getTenant()->getFileStorageBucket();
                $tenantName = $userTenant[0]->getTenant()->getName();
                $isAdmin = $userTenant[0]->getAdmin();
                $priceVatIncluded = $userTenant[0]->getTenant()->getPriceVatIncluded();
            }else{
                $tenantCode = "";
                $fileStorageBucket = "";
                $tenantName = "";
                $isAdmin = 0;
                $priceVatIncluded = 0;
            }
        }else{
            $userTenant = $this->repository->findBy(['user' => $userId, 'tenant' => $rq["tenant"]]);
            $tenantCode = $userTenant[0]->getTenant()->getCode();
            $fileStorageBucket = $userTenant[0]->getTenant()->getFileStorageBucket();
            $tenantName = $userTenant[0]->getTenant()->getName();
            $isAdmin = $userTenant[0]->getAdmin();
            $priceVatIncluded = $userTenant[0]->getTenant()->getPriceVatIncluded();
        }
        
        // merge with existing event data
        $payload = array_merge(
            $event->getData(),
            [
                'id' => $user->getId(),
                'name' => $user->getFirstName() . ' '. $user->getLastName(),
                'tenant' => $tenantCode,
                'tenantName' => $tenantName,
                'fileStorageBucket' => $fileStorageBucket,
                'isAdmin' => $isAdmin,
                'priceVatIncluded' => $priceVatIncluded
            ]
        );

        $event->setData($payload);
    }
}
