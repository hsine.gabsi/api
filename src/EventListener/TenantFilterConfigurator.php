<?php

namespace App\EventListener;


use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\HttpFoundation\RequestStack;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

final class TenantFilterConfigurator
{
    private $em;
    private $tokenStorage;
    private $reader;
    protected $requestStack;
    protected $jwtManager;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage, Reader $reader, RequestStack $requestStack, JWTTokenManagerInterface $jwtManager)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->reader = $reader;
        $this->requestStack = $requestStack;
        $this->jwtManager = $jwtManager;
    }

    public function onKernelRequest():void
    {
        if($this->tokenStorage->getToken() != null){
            $pathinfo = explode("/", $this->requestStack->getCurrentRequest()->getPathInfo());
            $entity = $pathinfo[count($pathinfo)-1];

            if($entity != "min_user_tenants" && $entity != "login_check"){
                $decodedJwtToken = $this->jwtManager->decode($this->tokenStorage->getToken()); 
                $tenant_code = $decodedJwtToken['tenant'];
                $filter = $this->em->getFilters()->enable('tenant_filter');
                $filter->setParameter('code', $tenant_code);
            
                if($tenant_code != 'advences'){                    
                    if($entity == 'fare_types'){ //$entity == 'tenant_parameter_values' || $entity == 'localities' || 
                        $filter->setParameter('masterTenant', "advences");
                    }
                }
                $filter->setAnnotationReader($this->reader);
            }
        }
    }
}
