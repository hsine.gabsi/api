<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(
 *              formats={"json"},
 *              normalizationContext={"groups"={"objectdetail_read"}},
 *              denormalizationContext={"groups"={"objectdetail_write"}},
 *              attributes={"order"={"rank": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"entity":"exact","objectId":"exact", "tenant":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\ObjectDetailRepository")
 */
class ObjectDetail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"objectdetail_read","objectdetail_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code",nullable=false)
     * @Groups({"objectdetail_read","objectdetail_write"})
     * @Assert\NotBlank(message="L'entity est obligatoire")
     */
    private $entity;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"objectdetail_read","objectdetail_write"})
     * @Assert\NotBlank(message="Le objectId est obligatoire")
     */
    private $objectId;


    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"objectdetail_read","objectdetail_write"})
     */
    private $title;

    // /**
    //  * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
    //  * @ORM\JoinColumn(name="group_id", nullable=true)
    //  * @Groups({"objectdetail_read","objectdetail_write"})
    //  */
    // private $group;

    /**
     * @ORM\Column(type="text")
     * @Groups({"objectdetail_read","objectdetail_write"})
     * @Assert\NotBlank(message="Le text est obligatoire")
     */
    private $text;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="locale_code", referencedColumnName="code",nullable=false)
     * @Groups({"objectdetail_read","objectdetail_write"})
     * @Assert\NotBlank(message="Le locale est obligatoire")
     */
    private $locale;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"objectdetail_read","objectdetail_write"})
     */
    private $rank;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectdetail_read","objectdetail_write"})
     * @Assert\NotBlank(message="Le tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"objectdetail_read","objectdetail_write"})
     */
    private $list;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code", nullable=true)
     * @Groups({"objectdetail_read","objectdetail_write"})
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }




    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    // public function getGroup(): ?TenantParameterValue
    // {
    //     return $this->group;
    // }

    // public function setGroup(?TenantParameterValue $group): self
    // {
    //     $this->group = $group;

    //     return $this;
    // }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getLocale(): ?MasterParameterValue
    {
        return $this->locale;
    }

    public function setLocale(?MasterParameterValue $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getList(): ?bool
    {
        return $this->list;
    }

    public function setList(bool $list): self
    {
        $this->list = $list;

        return $this;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }


}
