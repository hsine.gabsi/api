<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *  formats={"json"}
 * )
 * @ApiFilter(SearchFilter::class, properties={"entity":"exact","objectId":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\ObjectPhoneRepository")
 */
class ObjectPhone
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Le Number est obligatoire")
     */
    private $number;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code",nullable=false)
     * @Assert\NotBlank(message="L'entity est obligatoire")
     */
    private $entity;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le objectId est obligatoire")
     */
    private $objectId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code",nullable=false)
     * @Assert\NotBlank(message="Le type est obligatoire")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Locality")
     * @ORM\JoinColumn(name="country_id", nullable=false)
     * @Assert\NotBlank(message="Le country est obligatoire")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le country est obligatoire")
     */
    private $tenant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCountry(): ?Locality
    {
        return $this->country;
    }

    public function setCountry(?Locality $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }
}
