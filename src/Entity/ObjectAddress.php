<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json"})
 * @ApiFilter(SearchFilter::class, properties={"entity":"exact","objectId":"exact", "tenant":"exact"})
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_entity_objectid_billing", columns={"entity_code", "tenant_code", "object_id", "billing"})})
 * @ORM\Entity(repositoryClass="App\Repository\ObjectAddressRepository")
 */  
class ObjectAddress
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code",nullable=false)
     * @Assert\NotBlank(message="L'entity est obligatoire")
     */
    private $entity;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le objectID est obligatoire")
     */
    private $objectId;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(message="Le name est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Le tenant est obligatoire")
     */
    private $cityName;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Le zipcode est obligatoire")
     */
    private $zipCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Locality")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Le country est obligatoire")
     */
    private $country;

    /**
     * @ORM\Column(type="boolean",options={"default" : 1})
     * @Assert\NotBlank(message="Le billing est obligatoire")
     */
    private $billing;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\Column(type="string", length=400)
     * @Assert\NotBlank(message="Le street est obligatoire")
     */
    private $street;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCityName(): ?string
    {
        return $this->cityName;
    }

    public function setCityName(?string $cityName): self
    {
        $this->cityName = $cityName;

        return $this;
    }

    public function getCountry(): ?Locality
    {
        return $this->country;
    }

    public function setCountry(?Locality $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getBilling(): ?bool
    {
        return $this->billing;
    }

    public function setBilling(bool $billing): self
    {
        $this->billing = $billing;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }
}
