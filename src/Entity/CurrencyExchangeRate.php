<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json","jsonld"})
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyExchangeRateRepository")
 * @ApiFilter(SearchFilter::class, properties={"date":"exact", "convertedCurrency":"exact", "referenceCurrency":"exact"})
 * @ORM\Table(name="currency_exchange_rate",uniqueConstraints={@ORM\UniqueConstraint(name="uniq_currency_exchange_rate", columns={"tenant_code", "date", "converted_currency_code",  "reference_currency_code"})})
 */
class CurrencyExchangeRate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\Column(type="date", nullable=false)
	 * @Assert\NotBlank(message="Le champ date est obligatoire")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
	 * @ORM\JoinColumn(name="converted_currency_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ converted currency est obligatoire")
     */
    private $convertedCurrency;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="reference_currency_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ reference currency est obligatoire")
     */
    private $referenceCurrency;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
	 * @Assert\NotBlank(message="Le champ rate est obligatoire")
     */
    private $rate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getConvertedCurrency(): ?Currency
    {
        return $this->convertedCurrency;
    }

    public function setConvertedCurrency(?Currency $convertedCurrency): self
    {
        $this->convertedCurrency = $convertedCurrency;

        return $this;
    }

    public function getReferenceCurrency(): ?Currency
    {
        return $this->referenceCurrency;
    }

    public function setReferenceCurrency(?Currency $referenceCurrency): self
    {
        $this->referenceCurrency = $referenceCurrency;

        return $this;
    }

    public function getRate(): ?string
    {
        return $this->rate;
    }

    public function setRate(string $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}
