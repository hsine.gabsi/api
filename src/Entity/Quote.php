<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Annotation\TenantAware;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json", "jsonld"},
 *                       collectionOperations={
 *                          "get"={"normalization_context"={"groups"={"quote_read"}}},
 *                          "post"={"denormalization_context"={"groups"="quote_post"}}
 *                       },
 *                       itemOperations={
 *                          "get"={"normalization_context"={"groups"={"quote_read"}}},
 *                          "put"={"denormalization_context"={"groups"="quote_put"}}, 
 *                          "delete"
 *                       })
 * @ApiFilter(SearchFilter::class, properties={"tenant":"exact","id":"exact", "thirdParty.id":"exact", "number":"partial"})
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(RangeFilter::class, properties={"totalAmountInclTax"})
 * @ORM\Entity(repositoryClass="App\Repository\QuoteRepository")
 */
class Quote
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"quote_read", "quote_post"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="third_party_id", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     * @Assert\NotBlank(message="Le champ thirdparty est obligatoire")
     */
    private $thirdParty;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $number;

    /**
     * @ORM\Column(name="total_amount_incl_tax", type="decimal")
     * @Groups({"quote_read", "quote_post", "quote_put"})
     * @Assert\NotBlank(message="Le champ totalAmountExclTax est obligatoire")
     */
    private $totalAmountInclTax;

    /**
     * @Groups({"salesorder_read"})
     */
    private $totalAmountExclTax;

    /**
     * @ORM\Column(name="vat_amount", type="decimal", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $vatAmount;

    /**
     * @ORM\Column(type="date")
     * @Groups({"quote_read", "quote_post", "quote_put"})
     * @Assert\NotBlank(message="Le champ date est obligatoire")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="discount_type_code", referencedColumnName="code", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $discountType;

    /**
     * @ORM\Column(name="discount_value", type="decimal", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $discountValue;

    // /**
    //  * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
    //  * @ORM\JoinColumn(name="payment_status_type_code", referencedColumnName="code", nullable=false)
    //  * @Groups({"quote_read", "quote_post", "quote_put"})
    //  * @Assert\NotBlank(message="Le champ paymentStatusType est obligatoire")
    //  */
    // private $paymentStatusType;

    // /**
    //  * @ORM\Column(name="remaining_amount_due", type="decimal", nullable=false)
    //  * @Groups({"quote_read", "quote_post", "quote_put"})
    //  * @Assert\NotBlank(message="Le champ remainingAmountDue est obligatoire")
    //  */
    // private $remainingAmountDue;

    // /**
    //  * @ORM\Column(type="date", nullable=true)
    //  * @Groups({"quote_read", "quote_post", "quote_put"})
    //  */
    // private $fromDate;

    // /**
    //  * @ORM\Column(type="date", nullable=true)
    //  * @Groups({"quote_read", "quote_post", "quote_put"})
    //  */
    // private $toDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $updateUserId;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $accepted;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="requester_civility_code", referencedColumnName="code", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $requesterCivility;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $requesterFirstName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $requesterLastName;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $requesterIsOrganisation;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $requesterOrganisationName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $requesterEmail;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $requesterMobile1PhoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $requesterMobile2PhoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $requesterOfficePhoneNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuotePrice", mappedBy="quote", orphanRemoval=true, cascade={"persist"})
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $quotePrice;

    public function __construct()
    {
        $this->quotePrice = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getTotalAmountInclTax(): ?string
    {
        //return $this->totalAmountInclTax;
        return $this->priceFormatGet($this->totalAmountInclTax);
    }

    public function setTotalAmountInclTax(string $totalAmountInclTax): self
    {
        //$this->totalAmountInclTax = $totalAmountInclTax;
        $this->totalAmountInclTax = $this->priceFormatSet($totalAmountInclTax);

        return $this;
    }

    public function getTotalAmountExclTax(): ?string
    {
        $this->totalAmountExclTax = $this->totalAmountInclTax - $this->vatAmount;
        return $this->priceFormatGet($this->totalAmountExclTax);
    }

    public function getVatAmount(): ?string
    {
        //return $this->vatAmount;
        return $this->priceFormatGet($this->vatAmount);
    }

    public function setVatAmount(string $vatAmount): self
    {
        //$this->vatAmount = $vatAmount;
        $this->vatAmount = $this->priceFormatSet($vatAmount);
        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDiscountType(): ?MasterParameterValue
    {
        return $this->discountType;
    }

    public function setDiscountType(?MasterParameterValue $discountType): self
    {
        $this->discountType = $discountType;

        return $this;
    }

    public function getDiscountValue(): ?string
    {
        //return $this->discountValue;
        return $this->priceFormatGet($this->discountValue);
    }

    public function setDiscountValue(?string $discountValue): self
    {
        //$this->discountValue = $discountValue;
        $this->vatAmount = $this->priceFormatSet($discountValue);
        return $this;
    }

    // public function getPaymentStatusType(): ?MasterParameterValue
    // {
    //     return $this->paymentStatusType;
    // }

    // public function setPaymentStatusType(?MasterParameterValue $paymentStatusType): self
    // {
    //     $this->paymentStatusType = $paymentStatusType;

    //     return $this;
    // }

    // public function getRemainingAmountDue(): ?string
    // {
    //     //return $this->remainingAmountDue;
    //     return $this->priceFormatGet($this->remainingAmountDue);
    // }

    // public function setRemainingAmountDue(string $remainingAmountDue): self
    // {
    //     //$this->remainingAmountDue = $remainingAmountDue;
    //     $this->remainingAmountDue = $this->priceFormatSet($remainingAmountDue);
    //     return $this;
    // }

    public function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    public function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }

    // public function getFromDate(): ?\DateTimeInterface
    // {
    //     return $this->fromDate;
    // }

    // public function setFromDate(?\DateTimeInterface $fromDate): self
    // {
    //     $this->fromDate = $fromDate;

    //     return $this;
    // }

    // public function getToDate(): ?\DateTimeInterface
    // {
    //     return $this->toDate;
    // }

    // public function setToDate(?\DateTimeInterface $toDate): self
    // {
    //     $this->toDate = $toDate;

    //     return $this;
    // }

    
    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }

    public function getAccepted(): ?bool
    {
        return $this->accepted;
    }

    public function setAccepted(bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    public function getRequesterCivility(): ?MasterParameterValue
    {
        return $this->requesterCivility;
    }

    public function setRequesterCivility(?MasterParameterValue $requesterCivility): self
    {
        $this->requesterCivility = $requesterCivility;

        return $this;
    }

    public function getRequesterFirstName(): ?string
    {
        return $this->requesterFirstName;
    }

    public function setRequesterFirstName(?string $requesterFirstName): self
    {
        $this->requesterFirstName = $requesterFirstName;

        return $this;
    }

    public function getRequesterLastName(): ?string
    {
        return $this->requesterLastName;
    }

    public function setRequesterLastName(?string $requesterLastName): self
    {
        $this->requesterLastName = $requesterLastName;

        return $this;
    }

    public function getRequesterIsOrganisation(): ?bool
    {
        return $this->requesterIsOrganisation;
    }

    public function setRequesterIsOrganisation(bool $requesterIsOrganisation): self
    {
        $this->requesterIsOrganisation = $requesterIsOrganisation;

        return $this;
    }

    public function getRequesterOrganisationName(): ?string
    {
        return $this->requesterOrganisationName;
    }

    public function setRequesterOrganisationName(?string $requesterOrganisationName): self
    {
        $this->requesterOrganisationName = $requesterOrganisationName;

        return $this;
    }

    public function getRequesterEmail(): ?string
    {
        return $this->requesterEmail;
    }

    public function setRequesterEmail(?string $requesterEmail): self
    {
        $this->requesterEmail = $requesterEmail;

        return $this;
    }

    public function getRequesterMobile1PhoneNumber(): ?string
    {
        return $this->requesterMobile1PhoneNumber;
    }

    public function setRequesterMobile1PhoneNumber(?string $requesterMobile1PhoneNumber): self
    {
        $this->requesterMobile1PhoneNumber = $requesterMobile1PhoneNumber;

        return $this;
    }

    public function getRequesterMobile2PhoneNumber(): ?string
    {
        return $this->requesterMobile2PhoneNumber;
    }

    public function setRequesterMobile2PhoneNumber(?string $requesterMobile2PhoneNumber): self
    {
        $this->requesterMobile2PhoneNumber = $requesterMobile2PhoneNumber;

        return $this;
    }

    public function getRequesterOfficePhoneNumber(): ?string
    {
        return $this->requesterOfficePhoneNumber;
    }

    public function setRequesterOfficePhoneNumber(?string $requesterOfficePhoneNumber): self
    {
        $this->requesterOfficePhoneNumber = $requesterOfficePhoneNumber;

        return $this;
    }

    /**
     * @return Collection|QuotePrice[]
     */
    public function getQuotePrice(): Collection
    {
        return $this->quotePrice;
    }

    public function addQuotePrice(QuotePrice $quotePrice): self
    {
        if (!$this->quotePrice->contains($quotePrice)) {
            $this->quotePrice[] = $quotePrice;
            $quotePrice->setQuote($this);
        }

        return $this;
    }

    public function removeQuotePrice(QuotePrice $quotePrice): self
    {
        if ($this->quotePrice->contains($quotePrice)) {
            $this->quotePrice->removeElement($quotePrice);
            // set the owning side to null (unless already changed)
            if ($quotePrice->getQuote() === $this) {
                $quotePrice->setQuote(null);
            }
        }

        return $this;
    }
}
