<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(formats={"json", "jsonld"},
 *               normalizationContext={"groups"={"salesinvoiceprice_read"}},
 *               denormalizationContext={"groups"={"salesinvoiceprice_write"}})
 * @ORM\Entity(repositoryClass="App\Repository\SalesInvoicePriceRepository")
 * @ApiFilter(SearchFilter::class, properties={"salesInvoice":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\SalesInvoicePriceRepository")
 */
class SalesInvoicePrice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"salesinvoice_read", "salesinvoice_post", "salesinvoice_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(name="product_id", nullable=true)
     * @Groups({"salesinvoice_read", "salesinvoice_post", "salesinvoice_put"})
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"salesinvoice_read", "salesinvoice_post", "salesinvoice_put"})
     * @Assert\NotBlank(message="Le champ name est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", nullable=false)
     * @Groups({"salesinvoice_read", "salesinvoice_post", "salesinvoice_put"})
     * @Assert\NotBlank(message="Le champ unitPrice est obligatoire")
     */
    private $unitPrice;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2, nullable=false)
     * @Groups({"salesinvoice_read", "salesinvoice_post", "salesinvoice_put"})
     * @Assert\NotBlank(message="Le champ quantity est obligatoire")
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SalesInvoice", inversedBy="salesInvoicePrices")
     * @ORM\JoinColumn(name="sales_invoice_id", nullable=false)
     * @Assert\NotBlank(message="Le champ salesInvoice est obligatoire")
     */
    private $salesInvoice;

    /**
     * @Groups({"salesinvoice_read"})
     */
    private $totalPrice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUnitPrice(): ?string
    {
        $unitPrice = $this->unitPrice / pow(10, 2);
        return number_format($unitPrice, 2, '.', ' ');
    }

    public function setUnitPrice(string $unitPrice): self
    {
        $unitPrice = str_replace(',', '.', $unitPrice);
        $unitPrice = $this->toFixed($unitPrice, 2);
        $this->unitPrice = $unitPrice * pow(10, 2);
        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): self
    {
        $quantity = str_replace(',', '.', $quantity);
        $this->quantity = $quantity;

        return $this;
    }

    public function getSalesInvoice(): ?SalesInvoice
    {
        return $this->salesInvoice;
    }

    public function setSalesInvoice(?SalesInvoice $salesInvoice): self
    {
        $this->salesInvoice = $salesInvoice;

        return $this;
    }

    public function toFixed($price, $decimals)
    {
        $pos = strpos($price . '', ".");
        if ($pos > 0) {
            $int_str = substr($price, 0, $pos);
            $dec_str = substr($price, $pos + 1);
            if (strlen($dec_str) > $decimals) {
                return $int_str . ($decimals > 0 ? '.' : '') . substr($dec_str, 0, $decimals);
            } else {
                return $price;
            }
        } else {
            return $price;
        }
    }

    public function getTotalPrice(): ?string
    { 
        $this->totalPrice = $this->unitPrice * $this->quantity;
        $totalPrice = $this->totalPrice / pow(10, $this->salesInvoice->getCurrency()->getDecimals());
        return number_format($totalPrice, $this->salesInvoice->getCurrency()->getDecimals(), '.', ' ');
    }
}
