<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(formats={"json", "jsonld"})
 * @ORM\Entity(repositoryClass="App\Repository\QuotePriceRepository")
 */
class QuotePrice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(name="product_id", nullable=true)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     * @Assert\NotBlank(message="Le champ name est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(name="unit_price", type="decimal", nullable=false)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     * @Assert\NotBlank(message="Le champ unitPrice est obligatoire")
     */
    private $unitPrice;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=false)
     * @Groups({"quote_read", "quote_post", "quote_put"})
     * @Assert\NotBlank(message="Le champ quantity est obligatoire")
     */
    private $quantity;

    /**
     * @Groups({"salesorder_read"})
     */
    private $totalPrice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Quote", inversedBy="quotePrice")
     * @ORM\JoinColumn(nullable=false)
     */
    private $quote;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUnitPrice(): ?string
    {
        return $this->unitPrice/100;
    }

    public function setUnitPrice(?int $unitPrice): self
    { 
        $this->unitPrice = $unitPrice*100;

        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getQuote(): ?Quote
    {
        return $this->quote;
    }

    public function setQuote(?Quote $quote): self
    {
        $this->quote = $quote;

        return $this;
    }

    public function getTotalPrice(): ?string
    { 
        $this->totalPrice = $this->unitPrice * $this->quantity;
        return $this->quote->priceFormatGet($this->totalPrice);
    }
}
