<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json","jsonld"},
 *              attributes={
 *                          "normalization_context"={"groups"={"recurringTransaction_read", "thirdparties", "products", "masterParameterValues"}},
 *                          "denormalization_context"={"groups"={"recurringTransaction_write"}}
 *              }
 * )
 * @ApiFilter(SearchFilter::class, properties={"thirdParty":"exact", "price":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"sales"})
 * @ORM\Entity(repositoryClass="App\Repository\RecurringTransactionRepository")
 */
class RecurringTransaction
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="third_party_id", nullable=false)
     * @Assert\NotBlank(message="Le champ thirdparty est obligatoire")
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $thirdParty;

    /**
     * @ORM\Column(name="start_date", type="date", nullable=true)
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $startDate;

    /**
     * @ORM\Column(name="end_date", type="date", nullable=true)
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $endDate;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default" : 1})
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $sales = true;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="period_type_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ periodeType est obligatoire")
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $periodType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(name="product_id", nullable=true)
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $product;

    /**
     * @ORM\Column(name="product_name", type="string", length=100, nullable=true)
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     * 
     */
    private $productName;

    /**
     * @ORM\Column(type="decimal", nullable=false)
     * @Assert\NotBlank(message="Le champ price est obligatoire")
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="price_type_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ priceType est obligatoire")
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $priceType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $currency;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @Groups({"recurringTransaction_read", "recurringTransaction_write"})
     */
    private $active;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getSales(): ?bool
    {
        return $this->sales;
    }

    public function setSales(bool $sales): self
    {
        $this->sales = $sales;

        return $this;
    }

    public function getPeriodType(): ?MasterParameterValue
    {
        return $this->periodType;
    }

    public function setPeriodType(?MasterParameterValue $periodType): self
    {
        $this->periodType = $periodType;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->productName;
    }

    public function setProductName(?string $productName): self
    {
        $this->productName = $productName;

        return $this;
    }

    public function getPrice(): ?string
    {
        //return $this->price;
        return $this->price / pow(10, $this->currency->getDecimals());
    }

    public function setPrice(?string $price): self
    {
        //$this->price = $price;
        $price = str_replace(',', '.', $price);
        $price = $this->toFixed($price, $this->currency->getDecimals());
        $this->price = $price * pow(10, $this->currency->getDecimals());

        return $this;
    }

    public function getPriceType(): ?MasterParameterValue
    {
        return $this->priceType;
    }

    public function setPriceType(?MasterParameterValue $priceType): self
    {
        $this->priceType = $priceType;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function toFixed($price, $decimals)
    {
        $pos = strpos($price . '', ".");
        if ($pos > 0) {
            $int_str = substr($price, 0, $pos);
            $dec_str = substr($price, $pos + 1);
            if (strlen($dec_str) > $decimals) {
                return $int_str . ($decimals > 0 ? '.' : '') . substr($dec_str, 0, $decimals);
            } else {
                return $price;
            }
        } else {
            return $price;
        }
    }
}
