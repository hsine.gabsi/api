<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ApiResource(formats={"json", "jsonld"},
 *                      normalizationContext={"groups"={"salesinvoicepayment_read"}},
 *                      denormalizationContext={"groups"={"salesinvoicepayment_write"}})
 * @ApiFilter(SearchFilter::class, properties={"payment":"exact", "salesInvoice":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\SalesInvoicePaymentRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_sales_invoice_payment", columns={"sales_invoice_id", "payment_id"})})
 */
class SalesInvoicePayment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"salesinvoicepayment_read", "salesinvoicepayment_write", "salesinvoice_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SalesInvoice", inversedBy="invoicePayments")
     * @ORM\JoinColumn(name="sales_invoice_id", nullable=false)
     * @Groups({"salesinvoicepayment_read", "salesinvoicepayment_write"})
     * @Assert\NotBlank(message="Le champ sales invoice est obligatoire")
     */
    private $salesInvoice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Payment")
     * @ORM\JoinColumn(name="payment_id", nullable=false)
     * @Groups({"salesinvoicepayment_read", "salesinvoicepayment_write"})
     * @Assert\NotBlank(message="Le champ payment est obligatoire")
     */
    private $payment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code",  nullable=false)
     * @Groups({"salesinvoicepayment_read", "salesinvoicepayment_write"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\Column(type="decimal")
     * @Groups({"salesinvoicepayment_read", "salesinvoicepayment_write", "salesinvoice_read"})
     * @Assert\NotBlank(message="Le champ amount est obligatoire")
     */
    private $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSalesInvoice(): ?SalesInvoice
    {
        return $this->salesInvoice;
    }

    public function setSalesInvoice(?SalesInvoice $salesInvoice): self
    {
        $this->salesInvoice = $salesInvoice;

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount / pow(10, $this->currency->getDecimals());
    }

    public function setAmount(?string $amount): self
    {
        $amount = str_replace(',', '.', $amount);
        $amount = $this->toFixed($amount, $this->currency->getDecimals());
        $this->amount = $amount * pow(10, $this->currency->getDecimals());

        return $this;
    }

    public function toFixed($price, $decimals)
    {
        $pos = strpos($price . '', ".");
        if ($pos > 0) {
            $int_str = substr($price, 0, $pos);
            $dec_str = substr($price, $pos + 1);
            if (strlen($dec_str) > $decimals) {
                return $int_str . ($decimals > 0 ? '.' : '') . substr($dec_str, 0, $decimals);
            } else {
                return $price;
            }
        } else {
            return $price;
        }
    }
}
