<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ForecastRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotation\TenantAware;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json", "jsonld"},
 *                       collectionOperations={
 *                          "get"={"normalization_context"={"groups"={"forecast_read", "thirdparties", "masterParameterValues"}}},
 *                          "post"={"denormalization_context"={"groups"="forecast_post"}}
 *                       },
 *                       itemOperations={
 *                          "get"={"normalization_context"={"groups"={"forecast_read", "thirdparties", "masterParameterValues"}}},
 *                          "put"={"denormalization_context"={"groups"="forecast_put"}}, 
 *                          "delete"
 *                       }
 * )
 * @ApiFilter(SearchFilter::class, properties={"type":"exact"})
 * @ORM\Entity(repositoryClass=ForecastRepository::class)
 */
class Forecast
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"forecast_read", "forecast_post", "forecast_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=ThirdParty::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $thirdParty;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"forecast_read", "forecast_post"})
     */
    private $tenant;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
    * @ORM\JoinColumn(name="type_code", referencedColumnName="code", nullable=false)
    * @Groups({"forecast_read", "forecast_post", "forecast_put"})
    */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"forecast_read", "forecast_post", "forecast_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

   /**
     * @ORM\Column(name="amount", type="decimal", nullable=false)
     * @Groups({"forecast_read", "forecast_post", "forecast_put"})
     * @Assert\NotBlank(message="Le champ amount est obligatoire")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"forecast_read", "forecast_post", "forecast_put"})
     */
    private $period;
/**
    * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
    * @ORM\JoinColumn(name="product_group_id", referencedColumnName="id", nullable=true)
    * @Groups({"forecast_read", "forecast_post", "forecast_put"})
    */
    private $productGroup;

    /**
    * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
    * @ORM\JoinColumn(name="charge_type_code", referencedColumnName="code", nullable=true)
    * @Groups({"forecast_read", "forecast_post", "forecast_put"})
    */
    private $chargeType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="charge_group_id", referencedColumnName="id", nullable=true)
     * @Groups({"forecast_read", "forecast_post", "forecast_put"})
     */
    private $chargeGroup;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"forecast_read", "forecast_post", "forecast_put"})
     */
    private $additionalInformation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

  
    public function getAmount(): ?string
    {
        return $this->priceFormatGet($this->amount);
    }

    public function setAmount(?string $amount): self
    {
        
        $this->amount = $this->priceFormatSet($amount);
        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(string $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getProductGroup(): ?TenantParameterValue
    {
        return $this->productGroup;
    }

    public function setProductGroup(?TenantParameterValue $productGroup): self
    {
        $this->productGroup = $productGroup;

        return $this;
    }

    public function getChargeType(): ?MasterParameterValue
    {
        return $this->chargeType;
    }

    public function setChargeType(?MasterParameterValue $chargeType): self
    {
        $this->chargeType = $chargeType;

        return $this;
    }

    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }

    public function getChargeGroup(): ?TenantParameterValue
    {
        return $this->chargeGroup;
    }

    public function setChargeGroup(?TenantParameterValue $chargeGroup): self
    {
        $this->chargeGroup = $chargeGroup;

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }
}
