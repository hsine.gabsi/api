<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(
 *              formats={"json"},
 *              normalizationContext={"groups"={"translations"}},
 *              denormalizationContext={"groups"={"seo_write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ObjectSeoContentRepository")
 * @ApiFilter(SearchFilter::class, properties={"entity":"exact","objectId":"exact","locale":"exact"})
 */
class ObjectSeoContent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"translations", "seo_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"translations", "seo_write"})
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="locale", referencedColumnName="code", nullable=false)
     * @Groups({"translations", "seo_write"})
     */
    private $locale;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code", nullable=false)
     * @Groups({"translations", "seo_write"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code", nullable=false)
     * @Groups({"translations", "seo_write"})
     */
    private $entity;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"translations", "seo_write"})
     */
    private $objectId;
	

    /**
     * @ORM\Column(type="text")
     * @Groups({"translations", "seo_write"})
     */
    private $text;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getLocale(): ?MasterParameterValue
    {
        return $this->locale;
    }

    public function setLocale(?MasterParameterValue $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }
}
