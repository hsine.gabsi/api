<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(formats={"json", "jsonld"},
 *              normalizationContext={"groups"={"paymentbankoperation_read"}},
 *              denormalizationContext={"groups"={"paymentbankoperation_write"}})
 * @ORM\Entity(repositoryClass="App\Repository\PaymentBankOperationRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_payment_bankoperation", columns={"payment_id", "bank_operation_id"})})
 */
class PaymentBankOperation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"paymentbankoperation_read", "paymentbankoperation_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Payment")
     * @ORM\JoinColumn(name="payment_id", nullable=false)
     * @Groups({"paymentbankoperation_read", "paymentbankoperation_write"})
     * @Assert\NotBlank(message="Le champ payment est obligatoire")
     */
    private $payment;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankOperation")
     * @ORM\JoinColumn(name="bank_operation_id", nullable=false)
     * @Groups({"paymentbankoperation_read", "paymentbankoperation_write"})
     * @Assert\NotBlank(message="Le champ bankOperation est obligatoire")
     */
    private $bankOperation;

    /**
     * @ORM\Column(type="decimal")
     * @Groups({"paymentbankoperation_read", "paymentbankoperation_write"})
     * @Assert\NotBlank(message="Le champ amount est obligatoire")
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"paymentbankoperation_read", "paymentbankoperation_write"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getBankOperation(): ?BankOperation
    {
        return $this->bankOperation;
    }

    public function setBankOperation(?BankOperation $bankOperation): self
    {
        $this->bankOperation = $bankOperation;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount / pow(10, $this->currency->getDecimals());
    }

    public function setAmount(?string $amount): self
    {
        $amount = str_replace(',', '.', $amount);
        $amount = $this->toFixed($amount, $this->currency->getDecimals());
        $this->amount = $amount * pow(10, $this->currency->getDecimals());

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function toFixed($price, $decimals)
    {
        $pos = strpos($price . '', ".");
        if ($pos > 0) {
            $int_str = substr($price, 0, $pos);
            $dec_str = substr($price, $pos + 1);
            if (strlen($dec_str) > $decimals) {
                return $int_str . ($decimals > 0 ? '.' : '') . substr($dec_str, 0, $decimals);
            } else {
                return $price;
            }
        } else {
            return $price;
        }
    }
}
