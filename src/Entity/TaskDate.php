<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ApiResource(formats={"json"},
 *              normalizationContext={"groups"={"taskdate_read"}},
 *              denormalizationContext={"groups"={"taskdate_write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\TaskDateRepository")
 * @ApiFilter(SearchFilter::class, properties={"task":"exact"})
 */
class TaskDate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"taskdate_read", "taskdate_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="role_code", referencedColumnName="code", nullable=false)
     * @Groups({"taskdate_read", "taskdate_write"})
     * @Assert\NotBlank(message="Le champ role est obligatoire")
     */
    private $role;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task", inversedBy="date")
     * @ORM\JoinColumn(name="task_id", nullable=false)
     * @Groups({"taskdate_read", "taskdate_write"})
     * @Assert\NotBlank(message="Le champ task est obligatoire")
     */
    private $task;

    /**
     * @ORM\Column(type="date")
     * @Groups({"taskdate_read", "taskdate_write"})
     * @Assert\NotBlank(message="Le champ date est obligatoire")
     */
    private $date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRole(): ?MasterParameterValue
    {
        return $this->role;
    }

    public function setRole(?MasterParameterValue $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getTask(): ?Task
    {
        return $this->task;
    }

    public function setTask(?Task $task): self
    {
        $this->task = $task;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

}
