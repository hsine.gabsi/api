<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(
 *     formats={"json","jsonld"},
 *              attributes={
 *                          "normalization_context"={"groups"={"bankOperation_read","bankAccounts"}},
 *                          "denormalization_context"={"groups"={"bankOperation_write"}}
 *              }
 * )
 * @ApiFilter(SearchFilter::class, properties={"id":"exact", "bankAccount":"exact", "operationDate":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"credit"})
 * @ApiFilter(DateFilter::class, properties={"valueDate", "operationDate"})
 * @ApiFilter(RangeFilter::class, properties={"amount"})
 * @ORM\Entity(repositoryClass="App\Repository\BankOperationRepository")
 */
class BankOperation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankAccount")
     * @ORM\JoinColumn(name="bank_account_id", nullable=false)
     * @Assert\NotBlank(message="Le champ account est obligatoire")
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $bankAccount;

    /**
     * @ORM\Column(type="date", nullable=false)
     * @Assert\NotBlank(message="Le champ operationDate est obligatoire")
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $operationDate;

    /**
     * @ORM\Column(type="date", nullable=false)
     * @Assert\NotBlank(message="Le champ valueDate est obligatoire")
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $valueDate;

    /**
     * @ORM\Column(type="boolean", nullable=false)
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $credit;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="method_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ methode est obligatoire")
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $method;

    /**
     * @ORM\Column(type="decimal", nullable=false)
     * @Assert\NotBlank(message="Le champ amount est obligatoire")
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Le champ name est obligatoire")
	 * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le champ balance est obligatoire")
     * @Groups({"bankOperation_read", "bankOperation_write"})
     */
    private $balance;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getBankAccount(): ?BankAccount
    {
        return $this->bankAccount;
    }

    public function setBankAccount(?BankAccount $bankAccount): self
    {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    public function getOperationDate(): ?\DateTimeInterface
    {
        return $this->operationDate;
    }

    public function setOperationDate(\DateTimeInterface $operationDate): self
    {
        $this->operationDate = $operationDate;

        return $this;
    }

    public function getValueDate(): ?\DateTimeInterface
    {
        return $this->valueDate;
    }

    public function setValueDate(\DateTimeInterface $valueDate): self
    {
        $this->valueDate = $valueDate;

        return $this;
    }

    public function getCredit(): ?bool
    {
        return $this->credit;
    }

    public function setCredit(bool $credit): self
    {
        $this->credit = $credit;

        return $this;
    }

    public function getMethod(): ?masterParameterValue
    {
        return $this->method;
    }

    public function setMethod(?masterParameterValue $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->priceFormatGet($this->amount);
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $this->priceFormatSet($amount);
        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBalance(): ?int
    {
        return $this->balance;
    }

    public function setBalance(int $balance): self
    {
        $this->balance = $balance;

        return $this;
    }
    
    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }
}
