<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(
 *              formats={"json"},
 *              normalizationContext={"groups"={"objectmedia_read"}},
 *              denormalizationContext={"groups"={"objectmedia_write"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"entity":"exact","objectId":"exact", "tenant":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"list","logo"})
 * @ORM\Entity(repositoryClass="App\Repository\ObjectMediaRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_media_object", columns={"tenant_code","url", "object_id","entity_code"})})
 */
class ObjectMedia
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code",nullable=false)
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    private $entity;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    private $objectId;

   

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    private $rank;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    private $tenant;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default" : 0})
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    private $panoramic;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default" : 1})
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    private $gallery;

    /**
     * @ORM\Column(type="string", length=400)
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    
    private $url;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default" : 0})
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    private $list;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"objectmedia_read","objectmedia_write"})
     */
    private $logo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    
    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getPanoramic(): ?bool
    {
        return $this->panoramic;
    }

    public function setPanoramic(bool $panoramic): self
    {
        $this->panoramic = $panoramic;

        return $this;
    }

    public function getGallery(): ?bool
    {
        return $this->gallery;
    }

    public function setGallery(bool $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getList(): ?bool
    {
        return $this->list;
    }

    public function setList(bool $list): self
    {
        $this->list = $list;

        return $this;
    }

    public function getLogo(): ?bool
    {
        return $this->logo;
    }

    public function setLogo(bool $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

  
}
