<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(formats={"json","jsonld"},
 *              normalizationContext={"groups"={"thirdpartyperson_read","thirdparties", "masterParameterValues"}},
 *              denormalizationContext={"groups"={"thirdpartyperson_write"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"id":"exact","firstName":"partial","lastName":"partial","email":"exact", "thirdParty":"exact", "billingContact":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"active"})
 * @ORM\Entity(repositoryClass="App\Repository\ThirdPartyPersonRepository")
 */
class ThirdPartyPerson
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="third_party_id", referencedColumnName="id",nullable=false)
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $thirdParty;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="civility_code", referencedColumnName="code")
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $civility;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Assert\Email(message="L'adresse email doit avoir un format valide !")
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Le prénom est obligatoire")
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="boolean",options={"default" : 1})
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $active;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $billingContact;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $function;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $mobile1PhoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $mobile2PhoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"thirdpartyperson_read", "thirdpartyperson_write"})
     */
    private $officePhoneNumber;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getCivility(): ?MasterParameterValue
    {
        return $this->civility;
    }

    public function setCivility(?MasterParameterValue $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getBillingContact(): ?bool
    {
        return $this->billingContact;
    }

    public function setBillingContact(?bool $billingContact): self
    {
        $this->billingContact = $billingContact;

        return $this;
    }

    public function getFunction(): ?string
    {
        return $this->function;
    }

    public function setFunction(?string $function): self
    {
        $this->function = $function;

        return $this;
    }

    public function getMobile1PhoneNumber(): ?string
    {
        return $this->mobile1PhoneNumber;
    }

    public function setMobile1PhoneNumber(?string $mobile1PhoneNumber): self
    {
        $this->mobile1PhoneNumber = $mobile1PhoneNumber;

        return $this;
    }

    public function getMobile2PhoneNumber(): ?string
    {
        return $this->mobile2PhoneNumber;
    }

    public function setMobile2PhoneNumber(?string $mobile2PhoneNumber): self
    {
        $this->mobile2PhoneNumber = $mobile2PhoneNumber;

        return $this;
    }

    public function getOfficePhoneNumber(): ?string
    {
        return $this->officePhoneNumber;
    }

    public function setOfficePhoneNumber(?string $officePhoneNumber): self
    {
        $this->officePhoneNumber = $officePhoneNumber;

        return $this;
    }
}
