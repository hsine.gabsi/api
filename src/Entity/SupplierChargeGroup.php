<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ApiResource(formats={"json", "jsonld"},
 *              normalizationContext={"groups"={"supplierchargegroup_read"}},
 *              denormalizationContext={"groups"={"supplierchargegroup_write"}})
 * @ApiFilter(SearchFilter::class, properties={"supplier":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\SupplierChargeGroupRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_supplier_chargeGroup", columns={"supplier_id", "charge_group_id"})})
 */
class SupplierChargeGroup
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"supplierchargegroup_read", "supplierchargegroup_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="supplier_id", nullable=false)
     * @Groups({"supplierchargegroup_read", "supplierchargegroup_write"})
     * @Assert\NotBlank(message="Le champ supplier est obligatoire")
     */
    private $supplier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="charge_group_id", referencedColumnName="id", nullable=false)
     * @Groups({"supplierchargegroup_read", "supplierchargegroup_write"})
     * @Assert\NotBlank(message="Le champ chargeGroup est obligatoire")
     */
    private $chargeGroup;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSupplier(): ?ThirdParty
    {
        return $this->supplier;
    }

    public function setSupplier(ThirdParty $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getChargeGroup(): ?TenantParameterValue
    {
        return $this->chargeGroup;
    }

    public function setChargeGroup(TenantParameterValue $chargeGroup): self
    {
        $this->chargeGroup = $chargeGroup;

        return $this;
    }
}
