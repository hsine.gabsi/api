<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TenantAware;

/**
 * @ORM\Entity()
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(
 *     collectionOperations={
 *          "get_publication"={
 *             "method"="GET",
 *              "path"="/tasks",
 *              "controller"=TaskController::class,
 *              "normalization_context"={"groups"={"task_read", "translations"}},
 *          },
 *          "POST"={ "method"="POST" }
 *      },
 *     attributes={
 *        "normalization_context"={"groups"={"task_read", "translations"}},
 *        "denormalization_context"={"groups"={"task_write"}}
 *     })
 * @ApiFilter(SearchFilter::class, properties={"id":"exact","type":"exact","businessUnit":"exact","status":"exact","name":"partial", "number":"partial", "parent":"exact", "queue":"exact","thirdParty":"exact"})
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_task_number", columns={"number", "tenant_code"})})
 * @ORM\Entity(repositoryClass="App\Repository\TaskRepository")
 */
class Task
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"task_read", "task_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="status_code", referencedColumnName="code", nullable=false)
     * @Groups({"task_read", "task_write"})
     * @Assert\NotBlank(message="Le champ status est obligatoire")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"task_read", "task_write"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code", nullable=false)
     * @Groups({"task_read", "task_write"})
     * @Assert\NotBlank(message="Le champ type est obligatoire")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="business_unit_code", referencedColumnName="code", nullable=false)
     * @Groups({"task_read", "task_write"})
     * @Assert\NotBlank(message="Le champ business unit est obligatoire")
     */
    private $businessUnit;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="channel_code", referencedColumnName="code", nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $channel;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"task_read", "task_write"})
     * @Assert\NotBlank(message="Le champ nom est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=4000, nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"task_read", "task_write"})
     * @Assert\NotBlank(message="Le champ number est obligatoire")
     */
    private $number;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"task_read", "task_write"})
     */
    private $issue;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="priority_type_code", referencedColumnName="code", nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $priorityType;

    /**
     * @ORM\Column(name="short_code", type="string", length=10, nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $shortCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task")
	 * @ORM\JoinColumn(name="queue_id", referencedColumnName="id", nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $queue;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="process_code", referencedColumnName="code", nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $process;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="third_party_id", nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $thirdParty;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RecurringOperation")
     * @ORM\JoinColumn(name="recurring_operation_id", nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $recurringOperation;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"task_read", "task_write"})
     */
    private $period;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getBusinessUnit(): ?MasterParameterValue
    {
        return $this->businessUnit;
    }

    public function setBusinessUnit(?MasterParameterValue $businessUnit): self
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChannel(): ?MasterParameterValue
    {
        return $this->channel;
    }

    public function setChannel(?MasterParameterValue $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    public function getIssue(): ?bool
    {
        return $this->issue;
    }

    public function setIssue(bool $issue): self
    {
        $this->issue = $issue;

        return $this;
    }

   public function getStatus(): ?MasterParameterValue
    {
        return $this->status;
    }

    public function setStatus(?MasterParameterValue $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }
    
    public function getPriorityType(): ?MasterParameterValue
    {
        return $this->priorityType;
    }

    public function setPriorityType(?MasterParameterValue $priorityType): self
    {
        $this->priorityType = $priorityType;

        return $this;
    }

    public function getShortCode(): ?string
    {
        return $this->shortCode;
    }

    public function setShortCode(?string $shortCode): self
    {
        $this->shortCode = $shortCode;

        return $this;
    }

    public function getQueue(): ?self
    {
        return $this->queue;
    }

    public function setQueue(?self $queue): self
    {
        $this->queue = $queue;

        return $this;
    }

    public function getProcess(): ?MasterParameterValue
    {
        return $this->process;
    }

    public function setProcess(?MasterParameterValue $process): self
    {
        $this->process = $process;

        return $this;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getRecurringOperation(): ?RecurringOperation
    {
        return $this->recurringOperation;
    }

    public function setRecurringOperation(?RecurringOperation $recurringOperation): self
    {
        $this->recurringOperation = $recurringOperation;

        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(?string $period): self
    {
        $this->period = $period;

        return $this;
    }
}
