<?php
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json"}, 
 *              normalizationContext={"groups"={"objectcomment_read", "users"}},
 *              denormalizationContext={"groups"={"objectcomment_write"}},
 *              attributes={"order"={"id"="DESC"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ObjectCommentRepository")
 * @ApiFilter(SearchFilter::class, properties={"task.id":"exact", "objectId":"exact"})
 */
class ObjectComment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"objectcomment_read", "objectcomment_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     * @Groups({"objectcomment_read", "objectcomment_write"})
     * @Assert\NotBlank(message="Le champ internal est obligatoire")
     */
    private $internal;

    /**
     * @ORM\Column(type="text")
     * @Groups({"objectcomment_read", "objectcomment_write"})
     * @Assert\NotBlank(message="Le champ internal est obligatoire")
     */
    private $text;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     * @Groups({"objectcomment_read", "objectcomment_write"})
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="created_by", nullable=false)
     * @Groups({"objectcomment_read", "objectcomment_write", "users"})
     */
    private $createdBy;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"objectcomment_read", "objectcomment_write"})
     * @Assert\NotBlank(message="Le champ objectId est obligatoire")
     */
    private $objectId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectcomment_read", "objectcomment_write"})
     * @Assert\NotBlank(message="Le chemp entity est obligatoire")
     */
    private $entity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectcomment_read", "objectcomment_write"})
     * @Assert\NotBlank(message="Le tenant est obligatoire")
     */
    private $tenant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInternal(): ?bool
    {
        return $this->internal;
    }

    public function setInternal(bool $internal): self
    {
        $this->internal = $internal;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }
}
