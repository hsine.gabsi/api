<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Annotation\TenantAware;

/** 
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json"},
 *              normalizationContext={"groups"={"tenantuser_read"}},
 *              denormalizationContext={"groups"={"tenantuser_write"}}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserTenantRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_user", columns={"tenant_code", "user_id"})})
 * @ApiFilter(SearchFilter::class, properties={"user":"exact"})
 */
class UserTenant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"tenantuser_read", "tenantuser_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"tenantuser_read", "tenantuser_write"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userTenants")
     * @ORM\JoinColumn(name="user_id", nullable=false)
     * @Groups({"tenantuser_read", "tenantuser_write"})
     * @Assert\NotBlank(message="Le champ user est obligatoire")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"tenantuser_read", "tenantuser_write"}) 
     */
    private $affiliated;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"tenantuser_read", "tenantuser_write"})
     */
    private $admin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getAffiliated(): ?bool
    {
        return $this->affiliated;
    }

    public function setAffiliated(bool $affiliated): self
    {
        $this->affiliated = $affiliated;

        return $this;
    }

    public function getAdmin(): ?bool
    {
        return $this->admin;
    }

    public function setAdmin(bool $admin): self
    {
        $this->admin = $admin;

        return $this;
    }
}
