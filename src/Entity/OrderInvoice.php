<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(formats={"json", "jsonld"},
 *              normalizationContext={"groups"={"orderinvoice_read"}},
 *              denormalizationContext={"groups"={"orderinvoice_write"}})
 * @ORM\Entity(repositoryClass="App\Repository\OrderInvoiceRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_order_invoice", columns={"order_id", "invoice_id"})})
 */
class OrderInvoice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"orderinvoice_read", "orderinvoice_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrderInvoice")
     * @ORM\JoinColumn(name="order_id", nullable=false)
     * @Groups({"orderinvoice_read", "orderinvoice_write"})
     * @Assert\NotBlank(message="Le champ order est obligatoire")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SalesInvoice")
     * @ORM\JoinColumn(name="invoice_id", nullable=false)
     * @Groups({"orderinvoice_read", "orderinvoice_write"})
     * @Assert\NotBlank(message="Le champ invoice est obligatoire")
     */
    private $invoice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"orderinvoice_read", "orderinvoice_write"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;
    
    /**
     * @ORM\Column(type="decimal")
     * @Groups({"orderinvoice_read", "orderinvoice_write"})
     * @Assert\NotBlank(message="Le champ amount est obligatoire")
     */
    private $amount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): ?self
    {
        return $this->order;
    }

    public function setOrder(?self $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getInvoice(): ?SalesInvoice
    {
        return $this->invoice;
    }

    public function setInvoice(?SalesInvoice $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount / pow(10, $this->currency->getDecimals());
    }

    public function setAmount(?string $amount): self
    {
        $amount = str_replace(',', '.', $amount);
        $amount = $this->toFixed($amount, $this->currency->getDecimals());
        $this->amount = $amount * pow(10, $this->currency->getDecimals());

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function toFixed($price, $decimals)
    {
        $pos = strpos($price . '', ".");
        if ($pos > 0) {
            $int_str = substr($price, 0, $pos);
            $dec_str = substr($price, $pos + 1);
            if (strlen($dec_str) > $decimals) {
                return $int_str . ($decimals > 0 ? '.' : '') . substr($dec_str, 0, $decimals);
            } else {
                return $price;
            }
        } else {
            return $price;
        }
    }
}
