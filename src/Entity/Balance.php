<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotation\TenantAware;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;

/**
 * @ApiResource(attributes={"order"={"date": "DESC"}} ,formats={"json", "jsonld"},
 *                       collectionOperations={
 *                          "get"={"normalization_context"={"groups"={"balance_read"}}},
 *                          "post"={"denormalization_context"={"groups"="balance_post"}}
 *                       },
 *                       itemOperations={
 *                          "get"={"normalization_context"={"groups"={"balance_read"}}},
 *                          "put"={"denormalization_context"={"groups"="balance_put"}}, 
 *                          "delete"
 *                       }
 * )
 * @ApiFilter(SearchFilter::class, properties={"objectId":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\BalanceRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_date_object", columns={"date", "object_id","object_code", "tenant_code"})})
 */
class Balance
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"balance_read", "balance_post", "balance_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"balance_read", "balance_post"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="object_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ object_code est obligatoire")
     * @Groups({"balance_read", "balance_post", "balance_put"})
     */
    private $objectCode;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"balance_read", "balance_post", "balance_put"})
     */
    private $objectId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"balance_read", "balance_post", "balance_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"balance_read", "balance_post", "balance_put"})
     */
    private $date;

    /**
     * @ORM\Column(type="decimal")
     * @Groups({"balance_read", "balance_post", "balance_put"})
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getObjectCode(): ?MasterParameterValue
    {
        return $this->objectCode;
    }

    public function setObjectCode(?MasterParameterValue $objectCode): self
    {
        $this->objectCode = $objectCode;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(?int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
        
    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }


    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getValue(): ?string
    {
        
        return $this->priceFormatGet($this->value);
    }

    public function setValue(?string $value): self
    {
       
        $this->value = $this->priceFormatSet($value);
        return $this;
    }
}
