<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json","jsonld"},
 *                       collectionOperations={
 *                          "get"={"normalization_context"={"groups"={"thirdparty_read", "translations"}}},
 *                          "post"={"denormalization_context"={"groups"="thirdparty_post"}}
 *                       },
 *                       itemOperations={
 *                          "get"={"normalization_context"={"groups"={"thirdparty_read", "translations"}}},
 *                          "put"={"denormalization_context"={"groups"="thirdparty_put"}}, 
 *                          "delete"
 *                       }
 * )
 * @ApiFilter(SearchFilter::class, properties={"tenant":"exact","id":"exact","name": "partial","type":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"active"})
 * @ORM\Table(name="third_party",uniqueConstraints={@ORM\UniqueConstraint(name="uniq_name_tenant", columns={"name", "tenant_code"})})
 * @ORM\Entity(repositoryClass="App\Repository\ThirdPartyRepository")
 */
class ThirdParty 
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"read", "product_read", "thirdparty_read", "thirdparty_post", "thirdparty_put", "user_read", "product_write", "thirdparties", "quote_read", "order_read","expectedpayment_read","thirdpartyperson_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code",nullable=false)
     * @Groups({"thirdparty_read", "thirdparty_post"})
     * @Assert\NotBlank(message="Le tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\Column(type="boolean",options={"default" : 1})
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $organization;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code",nullable=false)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     * @Assert\NotBlank(message="Le type est obligatoire")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="civility_code", referencedColumnName="code",nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $civility;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"product_read", "translations", "thirdparty_read", "thirdparty_post", "thirdparty_put", "user_read", "thirdparties", "quote_read", "order_read","expectedpayment_read","thirdpartyperson_read"})
     * @Assert\NotBlank(message="Le nom est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"thirdparty_read", "thirdparty_post", "thirdparty_put","payment_read","purchaseinvoice_read","sales_read"})
     */
    private $firstName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id",nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $group;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="sub_group_id", referencedColumnName="id",nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $subGroup;

    /**
     * @ORM\Column(name="accounting_code", type="string", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $accounting;

    /**
     * @ORM\Column(type="decimal", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $openingBalance;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $openingBalanceDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="preferred_payment_method_code", referencedColumnName="code",nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $preferredPaymentMethod;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="payment_delay_code", referencedColumnName="code",nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put", "invoice_read"})
     */
    private $paymentDelay;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="language_code", referencedColumnName="code", nullable=false)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $webSite;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $active;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $vatExempt;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Email(message="L'adresse email doit avoir un format valide !")
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $mobile1PhoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $mobile2PhoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_post", "thirdparty_put"})
     */
    private $officePhoneNumber;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $birth_day;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $birth_year;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Locality")
     * @ORM\JoinColumn(name="country_id",nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $country;
    
    /**
     * @ORM\Column(name="city_name",type="string", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $cityName;
    
     /**
     * @ORM\Column(name="zip_code",type="string", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $zipCode;
    
    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $street;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *  @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     *  @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *  @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     *  @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $updateUserId;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $additionalInformation;

    public function __construct()
    {

        $this->products = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getOrganization(): ?bool
    {
        return $this->organization;
    }

    public function setOrganization(bool $organization): self
    {
        $this->organization = $organization;

        return $this;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCivility(): ?MasterParameterValue
    {
        return $this->civility;
    }

    public function setCivility(?MasterParameterValue $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setfirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getGroup(): ?TenantParameterValue
    {
        return $this->group;
    }

    public function setGroup(?TenantParameterValue $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getSubGroup(): ?TenantParameterValue
    {
        return $this->subGroup;
    }

    public function setSubGroup(?TenantParameterValue $subGroup): self
    {
        $this->subGroup= $subGroup;

        return $this;
    }

    public function getAccounting(): ?string
    {
        return $this->accounting;
    }

    public function setAccounting(?string $accounting): self
    {
        $this->accounting = $accounting;

        return $this;
    }

    public function getOpeningBalance(): ?string
    {
        //return $this->openingBalance/100;
        if (is_numeric($this->openingBalance)) {
            return $this->openingBalance / pow(10, 2);
        } else {
            return $this->openingBalance;
        }
    }

    public function setOpeningBalance(?string $openingBalance): self
    {
        //$this->openingBalance = $openingBalance*100;
        if (is_numeric($openingBalance)) {
            $openingBalance = str_replace(',', '.', $openingBalance);
            $openingBalance = $this->toFixed($openingBalance, 2);
            $this->openingBalance = $openingBalance * pow(10, 2);
        } else {
            $this->openingBalance = $openingBalance;
        }
        return $this;
    }

    public function getOpeningBalanceDate(): ?\DateTimeInterface
    {
        return $this->openingBalanceDate;
    }

    public function setOpeningBalanceDate(?\DateTimeInterface $openingBalanceDate): self
    {
        $this->openingBalanceDate = $openingBalanceDate;

        return $this;
    }

    public function getPreferredPaymentMethod(): ?MasterParameterValue
    {
        return $this->preferredPaymentMethod;
    }

    public function setPreferredPaymentMethod(?MasterParameterValue $preferredPaymentMethod): self
    {
        $this->preferredPaymentMethod = $preferredPaymentMethod;

        return $this;
    }

    public function getPaymentDelay(): ?MasterParameterValue
    {
        return $this->paymentDelay;
    }

    public function setPaymentDelay(?MasterParameterValue $paymentDelay): self
    {
        $this->paymentDelay = $paymentDelay;

        return $this;
    }

    public function getLanguage(): ?MasterParameterValue
    {
        return $this->language;
    }

    public function setLanguage(?MasterParameterValue $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    public function setWebSite(?string $webSite): self
    {
        $this->webSite = $webSite;

        return $this;
    }


    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getVatExempt(): ?bool
    {
        return $this->vatExempt;
    }

    public function setVatExempt(bool $vatExempt): self
    {
        $this->vatExempt = $vatExempt;

        return $this;
    }

    public function toFixed($price, $decimals)
    {
        $pos = strpos($price . '', ".");
        if ($pos > 0) {
            $int_str = substr($price, 0, $pos);
            $dec_str = substr($price, $pos + 1);
            if (strlen($dec_str) > $decimals) {
                return $int_str . ($decimals > 0 ? '.' : '') . substr($dec_str, 0, $decimals);
            } else {
                return $price;
            }
        } else {
            return $price;
        }
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMobile1PhoneNumber(): ?string
    {
        return $this->mobile1PhoneNumber;
    }

    public function setMobile1PhoneNumber(?string $mobile1PhoneNumber): self
    {
        $this->mobile1PhoneNumber = $mobile1PhoneNumber;

        return $this;
    }

    public function getMobile2PhoneNumber(): ?string
    {
        return $this->mobile2PhoneNumber;
    }

    public function setMobile2PhoneNumber(?string $mobile2PhoneNumber): self
    {
        $this->mobile2PhoneNumber = $mobile2PhoneNumber;

        return $this;
    }

    public function getOfficePhoneNumber(): ?string
    {
        return $this->officePhoneNumber;
    }

    public function setOfficePhoneNumber(?string $officePhoneNumber): self
    {
        $this->officePhoneNumber = $officePhoneNumber;

        return $this;
    }

    public function getBirthDay(): ?string
    {
        return $this->birth_day;
    }

    public function setBirthDay(?string $birth_day): self
    {
        $this->birth_day = $birth_day;

        return $this;
    }

    public function getBirthYear(): ?int
    {
        return $this->birth_year;
    }

    public function setBirthYear(?int $birth_year): self
    {
        $this->birth_year = $birth_year;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }
    
    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;
    
        return $this;
    }
    
    public function getCityName(): ?string
    {
        return $this->cityName;
    }
    
    public function setCityName(?string $cityName): self
    {
        $this->cityName = $cityName;
    
        return $this;
    }
    
    public function getCountry(): ?Locality
    {
        return $this->country;
    }
    
    public function setCountry(?Locality $country): self
    {
        $this->country = $country;
    
        return $this;
    }
    
    public function getStreet(): ?string
    {
        return $this->street;
    }
    
    public function setStreet(?string $street): self
    {
        $this->street = $street;
    
        return $this;
    }

    
    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }
    
}
