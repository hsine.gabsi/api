<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotation\TenantAware;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource( formats={"json"},
 *              normalizationContext={"groups"={"translations"}},
 *              denormalizationContext={"groups"={"tenantPreference_write"}})
 * @ApiFilter(SearchFilter::class, properties={"tenant":"exact","type":"exact","entity":"exact","object":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"default"})
 * @ORM\Entity(repositoryClass="App\Repository\TenantPreferenceRepository")
 */
class TenantPreference
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"tenantPreference_write", "translations"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"tenantPreference_write", "translations"})
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code", nullable=false)
     * @Groups({"tenantPreference_write", "translations"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code", nullable=false)
     * @Groups({"tenantPreference_write", "translations"})
     */
    private $entity;

    /**
     * @ORM\Column(name="object_code", type="string", length=100)
     * @Groups({"tenantPreference_write", "translations"})
     */
    private $object;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"tenantPreference_write", "translations"})
     */
    private $rank;

    /**
     * @ORM\Column(name="proposed_by_default", type="boolean", nullable=false, options={"default" : 0})
     * @Groups({"tenantPreference_write", "translations"})
     */
    private $default;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="payment_delay_code", referencedColumnName="code", nullable=false)
     * @Groups({"tenantPreference_write", "translations"})
     */
    private $paymentDelay;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObject(): ?string
    {
        return $this->object;
    }

    public function setObject(string $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getDefault(): ?bool
    {
        return $this->default;
    }

    public function setDefault(bool $default): self
    {
        $this->default = $default;

        return $this;
    }

    public function getPaymentDelay(): ?MasterParameterValue
    {
        return $this->paymentDelay;
    }

    public function setPaymentDelay(?MasterParameterValue $paymentDelay): self
    {
        $this->paymentDelay = $paymentDelay;

        return $this;
    }
}
