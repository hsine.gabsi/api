<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json", "jsonld"},
 *              collectionOperations={
 *                    "get"={"normalization_context"={"groups"={"objectdocument_read"}}},
 *                    "post"={"denormalization_context"={"groups"="objectdocument_post"}}
 *              },
 *              itemOperations={
 *                    "get"={"normalization_context"={"groups"={"objectdocument_read"}}},
 *                    "put"={"denormalization_context"={"groups"="objectdocument_put"}}, 
 *                    "delete"
 *              }
 * )
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_entity_object_url", columns={"tenant_code", "entity_code","object_id","url"})})
 * @ApiFilter(SearchFilter::class, properties={"entity":"exact","objectId":"exact", "tenant":"exact", "type":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\ObjectDocumentRepository")
 */
class ObjectDocument
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"objectdocument_read","objectdocument_post","objectdocument_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectdocument_read","objectdocument_post"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectdocument_read","objectdocument_post","objectdocument_put"})
     * @Assert\NotBlank(message="Le champ entity est obligatoire")
     */
    private $entity;

    /**
     * @ORM\Column(name="object_id", type="integer", nullable=false)
     * @Groups({"objectdocument_read","objectdocument_post","objectdocument_put"})
     * @Assert\NotBlank(message="Le champ objectId est obligatoire")
     */
    private $objectId;

    /**
     * @ORM\Column(type="string", length=400, nullable=false)
     * @Groups({"objectdocument_read","objectdocument_post","objectdocument_put"})
     * @Assert\NotBlank(message="Le champ url est obligatoire")
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectdocument_read","objectdocument_post","objectdocument_put"})
     * @Assert\NotBlank(message="Le champ type est obligatoire")
     */
    private $type;

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }
}
