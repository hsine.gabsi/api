<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Locastic\ApiPlatformTranslationBundle\Model\AbstractTranslatable;
use Locastic\ApiPlatformTranslationBundle\Model\TranslationInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json","jsonld"},
 *              normalizationContext={"groups"={"product_read", "translations", "read"}},
 *              denormalizationContext={"groups"={"product_write"}})
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ApiFilter(SearchFilter::class, properties={"id":"exact", "translations.name":"partial", "code": "partial", "type":"exact", "supplier.id":"exact", "group.id":"exact"})
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_code", columns={"tenant_code","code"})})
 */
class Product extends AbstractTranslatable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"product_read","product_write", "products", "quoteitem_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"product_read","product_write"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code", nullable=false)
     * @Groups({"product_read","product_write"})
     * @Assert\NotBlank(message="Le champ type est obligatoire")
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"product_read","product_write"})
     * @Assert\NotBlank(message="Le champ code est obligatoire")
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="group_id")
     * @Groups({"product_read","product_write"})
     */
    private $group;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="sub_group_id")
     * @Groups({"product_read","product_write"})
     */
    private $subGroup;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $initialInventory;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $initialInventoryDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $renewingInventoryLevel;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="supplier_id", nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $supplier;

    /**
     * @ORM\Column(name="purchase_account_reference", type="string", length=100, nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $purchaseAccountReference;

    /**
     * @ORM\Column(name="sales_account_reference", type="string", length=100, nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $salesAccountReference;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"product_read","product_write"})
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductTranslation", mappedBy="product", orphanRemoval=true, cascade={"persist"})
     * @Groups({"product_read","product_write"})
     */
    protected $translations;
	
	 /**
     * @Groups({"product_read", "product_write", "products", "quoteitem_read"})
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=4, scale=2)
     * @Groups({"product_read","product_write"})
     */
    private $vatRate;

    /**
     * @ORM\Column(name="sales_unit_price", type="decimal", nullable=true)
	 * @Groups({"product_read","product_write"})
     */
    private $salesUnitPrice;

    /**
     * @ORM\Column(name="purchase_unit_price", type="decimal", nullable=true)
	 * @Groups({"product_read","product_write"})
     */
    private $purchaseUnitPrice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="sales_currency_code", referencedColumnName="code", nullable=true)
	 * @Groups({"product_read","product_write"})
     */
    private $salesCurrency;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="purchase_currency_code", referencedColumnName="code", nullable=true)
	 * @Groups({"product_read","product_write"})
     */
    private $purchaseCurrency;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $updateUserId;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"product_read","product_write"})
     */
    private $additionalInformation;

    public function __construct()
    {
        $this->prices = new ArrayCollection();
        $this->translations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
	public function setName($name)
    {
        $this->getTranslation()->setName($name);
    }

    public function getName(): string
    {
        return $this->getTranslation()->getName();
    }
	
    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getGroup(): ?TenantParameterValue
    {
        return $this->group;
    }

    public function setGroup(?TenantParameterValue $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getSubGroup(): ?TenantParameterValue
    {
        return $this->subGroup;
    }

    public function setSubGroup(?TenantParameterValue $subGroup): self
    {
        $this->subGroup = $subGroup;

        return $this;
    }

    public function getInitialInventory(): ?int
    {
        return $this->initialInventory;
    }

    public function setInitialInventory(?int $initialInventory): self
    {
        $this->initialInventory = $initialInventory;

        return $this;
    }

    public function getInitialInventoryDate(): ?\DateTimeInterface
    {
        return $this->initialInventoryDate;
    }

    public function setInitialInventoryDate(?\DateTimeInterface $initialInventoryDate): self
    {
        $this->initialInventoryDate = $initialInventoryDate;

        return $this;
    }

    public function getRenewingInventoryLevel(): ?int
    {
        return $this->renewingInventoryLevel;
    }

    public function setRenewingInventoryLevel(?int $renewingInventoryLevel): self
    {
        $this->renewingInventoryLevel = $renewingInventoryLevel;

        return $this;
    }

    public function getSupplier(): ?ThirdParty
    {
        return $this->supplier;
    }

    public function setSupplier(?ThirdParty $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getPurchaseAccountReference(): ?string
    {
        return $this->purchaseAccountReference;
    }

    public function setPurchaseAccountReference(?string $purchaseAccountReference): self
    {
        $this->purchaseAccountReference = $purchaseAccountReference;

        return $this;
    }

    public function getSalesAccountReference(): ?string
    {
        return $this->salesAccountReference;
    }

    public function setSalesAccountReference(?string $salesAccountReference): self
    {
        $this->salesAccountReference = $salesAccountReference;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

	protected function createTranslation():TranslationInterface
    {
        return new ProductTranslation();
    }

    /**
     * @return Collection|ProductTranslation[]
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }
    public function addTranslation(TranslationInterface $translation): void
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setProduct($this);
        }
    }

    public function removeTranslation(TranslationInterface $translation): void
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getProduct() === $this) {
                $translation->setProduct(null);
            }
        }
    }

    public function getVatRate(): ?string
    {
        return $this->vatRate;
    }

    public function setVatRate(?string $vatRate): self
    {
        $this->vatRate = $vatRate;

        return $this;
    }

    public function getSalesUnitPrice(): ?string
    {
        return $this->salesUnitPriceFormatGet($this->salesUnitPrice);
    }

    public function setSalesUnitPrice(?string $salesUnitPrice): ?int
    {
        $this->salesUnitPrice = $this->salesUnitPriceFormatSet($salesUnitPrice);

        return $this->salesUnitPrice;
    }

    public function getPurchaseUnitPrice(): ?string
    {
        return $this->purchaseUnitPriceFormatGet($this->purchaseUnitPrice);
    }

    public function setPurchaseUnitPrice(?string $purchaseUnitPrice): ?int
    {
        $this->purchaseUnitPrice = $this->purchaseUnitPriceFormatSet($purchaseUnitPrice);

        return $this->purchaseUnitPrice;
    }

    public function getSalesCurrency(): ?Currency
    {
        return $this->salesCurrency;
    }

    public function setSalesCurrency(?Currency $salesCurrency): self
    {
        $this->salesCurrency = $salesCurrency;

        return $this;
    }

    
    public function getPurchaseCurrency(): ?Currency
    {
        return $this->purchaseCurrency;
    }

    public function setPurchaseCurrency(?Currency $purchaseCurrency): self
    {
        $this->purchaseCurrency = $purchaseCurrency;

        return $this;
    }

    private function salesUnitPriceFormatGet(?string $salesUnitPrice):?string
    {
        if (is_numeric($salesUnitPrice)) {
            $salesUnitPrice = $salesUnitPrice / pow(10, 2);//$this->salesCurrency->getDecimals()
            return number_format($salesUnitPrice, 2, '.', ' ');//$this->salesCurrency->getDecimals()
        } else {
            return $salesUnitPrice;
        }
    }

    private function salesUnitPriceFormatSet($salesUnitPrice)
    {
        $salesUnitPrice = str_replace(' ', '', $salesUnitPrice);
        $salesUnitPrice = str_replace(',', '.', $salesUnitPrice);

        if (is_numeric($salesUnitPrice)) {
            $salesUnitPrice = $salesUnitPrice * pow(10, 2);//$this->salesCurrency->getDecimals()
            $pos = strpos((string)$salesUnitPrice, ".");
            if ($pos > 0) {
                $salesUnitPrice = substr($salesUnitPrice, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $salesUnitPrice;
    }
 
    private function purchaseUnitPriceFormatGet(?string $purchaseUnitPrice):?string
    {
        if (is_numeric($purchaseUnitPrice)) {
            $purchaseUnitPrice = $purchaseUnitPrice / pow(10, 2);// $this->purchaseCurrency->getDecimals()
            return number_format($purchaseUnitPrice, 2, '.', ' ');// $this->purchaseCurrency->getDecimals()
        } else {
            return $purchaseUnitPrice;
        }
    }

    private function purchaseUnitPriceFormatSet($purchaseUnitPrice)
    {
        $purchaseUnitPrice = str_replace(' ', '', $purchaseUnitPrice);
        $purchaseUnitPrice = str_replace(',', '.', $purchaseUnitPrice);

        if (is_numeric($purchaseUnitPrice)) {
            $purchaseUnitPrice = $purchaseUnitPrice * pow(10, 2);//$this->purchaseCurrency->getDecimals()
            $pos = strpos((string)$purchaseUnitPrice, ".");
            if ($pos > 0) {
                $purchaseUnitPrice = substr($purchaseUnitPrice, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $purchaseUnitPrice;
    }

    
    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }
}