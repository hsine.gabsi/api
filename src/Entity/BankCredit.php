<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotation\TenantAware;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json","jsonld"},
 *              collectionOperations={
 *                          "get"={"normalization_context"={"groups"={"bankcredit_read"}}},
 *                          "post"={"denormalization_context"={"groups"="bankcredit_post"}}
 *                       },
 *              itemOperations={
 *                          "get"={"normalization_context"={"groups"={"bankcredit_read"}}},
 *                          "put"={"denormalization_context"={"groups"="bankcredit_put"}}, 
 *                          "delete"
 *                       })
 * @ORM\Entity(repositoryClass="App\Repository\BankCreditRepository")
 */
class BankCredit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"bankcredit_read", "bankcredit_post", "bankcredit_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"bankcredit_read", "bankcredit_post"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"bankcredit_read", "bankcredit_post", "bankcredit_put"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"bankcredit_read", "bankcredit_post", "bankcredit_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\Column(type="decimal")
     * @Groups({"bankcredit_read", "bankcredit_post", "bankcredit_put"})
     * @Assert\NotBlank(message="Le champ amount est obligatoire")
     */
    private $amount;

    /**
     * @ORM\Column(name="interest_rate", type="decimal", precision=4, scale=2)
     * @Groups({"bankcredit_read", "bankcredit_post", "bankcredit_put"})
     * @Assert\NotBlank(message="Le champ interestRate est obligatoire")
     */
    private $interestRate;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"bankcredit_read", "bankcredit_post", "bankcredit_put"})
     * @Assert\NotBlank(message="Le champ durationMonths est obligatoire")
     */
    private $durationMonths;

    /**
     * @ORM\Column(type="date")
     * @Groups({"bankcredit_read", "bankcredit_post", "bankcredit_put"})
     * @Assert\NotBlank(message="Le champ firstReimbursementDate est obligatoire")
     */
    private $firstReimbursementDate;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"bankcredit_read", "bankcredit_post", "bankcredit_put"})
     */
    private $additionnelInformation;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     * @Groups({"bankcredit_read", "bankcredit_post", "bankcredit_put"})
     */
    private $active;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAmount(): ?string
    {
        //return $this->amount;
        return $this->priceFormatGet($this->amount);
    }

    public function setAmount(?string $amount): self
    {
        //$this->amount = $amount;
        $this->amount = $this->priceFormatSet($amount);
        return $this;
    }

    public function getInterestRate(): ?string
    {
        return $this->interestRate;
    }

    public function setInterestRate(string $interestRate): self
    {
        $this->interestRate = $interestRate;

        return $this;
    }

    public function getDurationMonths(): ?int
    {
        return $this->durationMonths;
    }

    public function setDurationMonths(int $durationMonths): self
    {
        $this->durationMonths = $durationMonths;

        return $this;
    }

    public function getFirstReimbursementDate(): ?\DateTimeInterface
    {
        return $this->firstReimbursementDate;
    }

    public function setFirstReimbursementDate(\DateTimeInterface $firstReimbursementDate): self
    {
        $this->firstReimbursementDate = $firstReimbursementDate;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getAdditionnelInformation(): ?string
    {
        return $this->additionnelInformation;
    }

    public function setAdditionnelInformation(?string $additionnelInformation): self
    {
        $this->additionnelInformation = $additionnelInformation;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }
}
