<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use App\Annotation\TenantAware;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json", "jsonld"},
 *              normalizationContext={"groups"={"order_read"}},
 *              denormalizationContext={"groups"={"order_write"}})
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ApiFilter(SearchFilter::class, properties={"id":"exact","customer.id":"exact","productGroup.id":"exact"})
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(RangeFilter::class, properties={"totalAmountInclTax"})
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"order_read", "order_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"order_read", "order_write"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="customer_id", nullable=true)
     * @Groups({"order_read", "order_write"})
     */
    private $customer;

    /**
     * @ORM\Column(type="date", nullable=false)
     * @Groups({"order_read", "order_write"})
     * @Assert\NotBlank(message="Le champ date est obligatoire")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"order_read", "order_write"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\Column(type="decimal", nullable=false)
     * @Groups({"order_read", "order_write"})
     * @Assert\NotBlank(message="Le champ totalAmountInclTax est obligatoire")
     */
    private $totalAmountInclTax;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     * @Groups({"order_read", "order_write"})
     */
    private $vatAmount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="product_group_id", nullable=true)
     * @Groups({"order_read", "order_write"})
     */
    private $productGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="product_sub_group_id", nullable=true)
     * @Groups({"order_read", "order_write"})
     */
    private $productSubGroup;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"order_read", "order_write"})
     */
    private $number;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getCustomer(): ?ThirdParty
    {
        return $this->customer;
    }

    public function setCustomer(?ThirdParty $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTotalAmountInclTax(): ?string
    {
        return $this->priceFormatGet($this->totalAmountInclTax);
    }

    public function setTotalAmountInclTax(string $totalAmountInclTax): self
    {
        $this->totalAmountInclTax = $this->priceFormatSet($totalAmountInclTax);
        return $this;
    }

    public function getVatAmount(): ?string
    {
        return $this->priceFormatGet($this->vatAmount);
    }

    public function setVatAmount(?string $vatAmount): self
    {
        $this->vatAmount = $this->priceFormatSet($vatAmount);
        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getProductGroup(): ?TenantParameterValue
    {
        return $this->productGroup;
    }

    public function setProductGroup(?TenantParameterValue $productGroup): self
    {
        $this->productGroup = $productGroup;

        return $this;
    }

    public function getProductSubGroup(): ?TenantParameterValue
    {
        return $this->productSubGroup;
    }

    public function setProductSubGroup(?TenantParameterValue $productSubGroup): self
    {
        $this->productSubGroup = $productSubGroup;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }
}
