<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json"},
 *      attributes={
 *        "normalization_context"={"groups"={"objectmember_read", "translations"}},
 *        "denormalization_context"={"groups"={"objectmember_write"}}
 *     }))
 * @ORM\Entity(repositoryClass="App\Repository\ObjectMemberRepository")
 * @ApiFilter(SearchFilter::class, properties={"task.id":"exact", "objectId":"exact","role":"exact"})
 */
class ObjectMember
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"objectmember_read", "objectmember_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false)
     * @Groups({"objectmember_read", "objectmember_write"})
     * @Assert\NotBlank(message="Le champ user est obligatoire")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="role_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectmember_read", "objectmember_write"})
     * @Assert\NotBlank(message="Le champ role est obligatoire")
     */
    private $role;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectmember_read", "objectmember_write"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectmember_read", "objectmember_write"})
     * @Assert\NotBlank(message="Le champ entity est obligatoire")
     */
    private $entity;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"objectmember_read", "objectmember_write"})
     * @Assert\NotBlank(message="Le champ objectId est obligatoire")
     */
    private $objectId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getRole(): ?MasterParameterValue
    {
        return $this->role;
    }

    public function setRole(?MasterParameterValue $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }
}
