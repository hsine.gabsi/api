<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(formats={"json", "jsonld"},
 *              normalizationContext={"groups"={"bankcreditreimbursement_read"}},
 *              denormalizationContext={"groups"={"bankcreditreimbursement_write"}})
 * @ORM\Entity(repositoryClass="App\Repository\BankCreditReimbursementRepository")
 */
class BankCreditReimbursement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"bankcreditreimbursement_read", "bankcreditreimbursement_write"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"bankcreditreimbursement_read", "bankcreditreimbursement_write"})
     */
    private $rank;

    /**
     * @ORM\Column(type="date")
     * @Groups({"bankcreditreimbursement_read", "bankcreditreimbursement_write"})
     * @Assert\NotBlank(message="Le champ date est obligatoire")
     */
    private $date;

    /**
     * @ORM\Column(type="decimal")
     * @Groups({"bankcreditreimbursement_read", "bankcreditreimbursement_write"})
     * @Assert\NotBlank(message="Le champ interestAmount est obligatoire")
     */
    private $interestAmount;

    /**
     * @ORM\Column(type="decimal")
     * @Groups({"bankcreditreimbursement_read", "bankcreditreimbursement_write"})
     * @Assert\NotBlank(message="Le champ amortizedCapitalAmount est obligatoire")
     */
    private $amortizedCapitalAmount;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     * @Groups({"bankcreditreimbursement_read", "bankcreditreimbursement_write"})
     */
    private $otherChargesAmount;

    /**
     * @ORM\Column(type="decimal")
     * @Groups({"bankcreditreimbursement_read", "bankcreditreimbursement_write"})
     * @Assert\NotBlank(message="Le champ totalAmount est obligatoire")
     */
    private $totalAmount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankCredit")
     * @ORM\JoinColumn(name="bank_credit_id", nullable=false)
     * @Groups({"bankcreditreimbursement_read", "bankcreditreimbursement_write"})
     * @Assert\NotBlank(message="Le champ bankCredit est obligatoire")
     */
    private $bankCredit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getInterestAmount(): ?string
    {
        //return $this->interestAmount;
        return $this->priceFormatGet($this->interestAmount);
    }

    public function setInterestAmount(string $interestAmount): self
    {
        //$this->interestAmount = $interestAmount;
        $this->interestAmount = $this->priceFormatSet($interestAmount);

        return $this;
    }

    public function getAmortizedCapitalAmount(): ?string
    {
        //return $this->amortizedCapitalAmount;
        return $this->priceFormatGet($this->amortizedCapitalAmount);
    }

    public function setAmortizedCapitalAmount(string $amortizedCapitalAmount): self
    {
        //$this->amortizedCapitalAmount = $amortizedCapitalAmount;
        $this->amortizedCapitalAmount = $this->priceFormatSet($amortizedCapitalAmount);

        return $this;
    }

    public function getOtherChargesAmount(): ?string
    {
        //return $this->otherChargesAmount;
        return $this->priceFormatGet($this->otherChargesAmount);
    }

    public function setOtherChargesAmount(?string $otherChargesAmount): self
    {
        //$this->otherChargesAmount = $otherChargesAmount;
        $this->otherChargesAmount = $this->priceFormatSet($otherChargesAmount);

        return $this;
    }

    public function getTotalAmount(): ?string
    {
        //return $this->totalAmount;
        return $this->priceFormatGet($this->totalAmount);
    }

    public function setTotalAmount(string $totalAmount): self
    {
        //$this->totalAmount = $totalAmount;
        $this->totalAmount = $this->priceFormatSet($totalAmount);

        return $this;
    }
    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }

    public function getBankCredit(): ?BankCredit
    {
        return $this->bankCredit;
    }

    public function setBankCredit(?BankCredit $bankCredit): self
    {
        $this->bankCredit = $bankCredit;

        return $this;
    }
}
