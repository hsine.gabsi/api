<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotation\TenantAware;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json", "jsonld"},
 *              normalizationContext={"groups"={"permission_read", "translations"}},
 *              denormalizationContext={"groups"={"permission_write"}})
 * @ORM\Entity(repositoryClass="App\Repository\PermissionRepository")
 * @ApiFilter(SearchFilter::class, properties={"user":"exact","businessEntity":"exact"})
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_user_businessentity", columns={"tenant_code", "business_entity_code","user_id"})})
 */
class Permission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"permission_read", "permission_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"permission_read", "permission_write"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="business_entity_code", referencedColumnName="code", nullable=false)
     * @Groups({"permission_read", "permission_write"})
     * @Assert\NotBlank(message="Le champ businessEntity est obligatoire")
     */
    private $businessEntity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", nullable=false)
     * @Groups({"permission_read", "permission_write"})
     * @Assert\NotBlank(message="Le champ user est obligatoire")
     */
    private $user;

    /**
     * @ORM\Column(name="update_create", type="boolean", options={"default" : 1})
     * @Groups({"permission_read", "permission_write"})
     */
    private $updateCreate;

    /**
     * @ORM\Column(name="consult", type="boolean", options={"default" : 1})
     * @Groups({"permission_read", "permission_write"})
     */
    private $consult;

    /**
     * @ORM\Column(name="export", type="boolean", options={"default" : 1})
     * @Groups({"permission_read", "permission_write"})
     */
    private $export;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getBusinessEntity(): ?BusinessEntity
    {
        return $this->businessEntity;
    }

    public function setBusinessEntity(?BusinessEntity $businessEntity): self
    {
        $this->businessEntity = $businessEntity;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUpdateCreate(): ?bool
    {
        return $this->updateCreate;
    }

    public function setUpdateCreate(bool $updateCreate): self
    {
        $this->updateCreate = $updateCreate;

        return $this;
    }

    public function getConsult(): ?bool
    {
        return $this->consult;
    }

    public function setConsult(bool $consult): self
    {
        $this->consult = $consult;

        return $this;
    }

    public function getExport(): ?bool
    {
        return $this->export;
    }

    public function setExport(bool $export): self
    {
        $this->export = $export;

        return $this;
    }
}
