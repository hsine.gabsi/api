<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(attributes={"order"={"date": "DESC"}} ,formats={"json","jsonld"},
 *              collectionOperations={
 *                    "get"={"normalization_context"={"groups"={"payment_read", "translations", "thirdparties"}}},
 *                    "post"={"denormalization_context"={"groups"="payment_post"}}
 *              },
 *              itemOperations={
 *                    "get"={"normalization_context"={"groups"={"payment_read", "translations", "thirdparties"}}},
 *                    "put"={"denormalization_context"={"groups"="payment_put"}}, 
 *                    "delete"
 *              }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 * @ApiFilter(SearchFilter::class, properties={"id":"exact", "thirdParty.id":"exact", "method": "exact", "chequeReference":"partial", "bank":"partial"})
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(BooleanFilter::class, properties={"inbound"})
 * @ApiFilter(RangeFilter::class, properties={"amount"})
 */
class Payment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"payment_read","payment_post"})
     * @Assert\NotBlank(message="Le tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="third_party_id", referencedColumnName="id", nullable=false)
     * @Groups({"payment_read","payment_post", "payment_put"})
     * @Assert\NotBlank(message="Le champ third Party est obligatoire")
     */
    private $thirdParty;

    /**
     * @ORM\Column(name="date", type="date")
     * @Groups({"payment_read","payment_post", "payment_put"})
     * @Assert\NotBlank(message="Le champ date est obligatoire")
     */
    private $date;

    /**
     * @ORM\Column(name="inbound", type="boolean", options={"default" : 1})
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $inbound;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"payment_read","payment_post", "payment_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="method_code", referencedColumnName="code", nullable=false)
     * @Groups({"payment_read","payment_post", "payment_put"})
     * @Assert\NotBlank(message="Le champ methode est obligatoire")
     */
    private $method;

    /**
     * @ORM\Column(name="cheque_reference", type="string", length=100, nullable=true)
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $chequeReference;

    /**
     * @ORM\Column(name="bank_name", type="string", length=100, nullable=true)
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $bank;

    /**
     * @ORM\Column(name="receipt_reference", type="string", length=100, nullable=true)
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $receiptReference;

    /**
     * @ORM\Column(type="decimal")
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $chequeRemittanceNumber;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $updateUserId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BankAccount")
     * @ORM\JoinColumn(name="bank_account_id", referencedColumnName="id", nullable=true)
     * @Groups({"payment_read","payment_post", "payment_put"})
     */
    private $bankAccountId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getInbound(): ?bool
    {
        return $this->inbound;
    }

    public function setInbound(bool $inbound): self
    {
        $this->inbound = $inbound;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getMethod(): ?MasterParameterValue
    {
        return $this->method;
    }

    public function setMethod(?MasterParameterValue $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getChequeReference(): ?string
    {
        return $this->chequeReference;
    }

    public function setChequeReference(?string $chequeReference): self
    {
        $this->chequeReference = $chequeReference;

        return $this;
    }

    public function getBank(): ?string
    {
        return $this->bank;
    }

    public function setBank(?string $bank): self
    {
        $this->bank = $bank;

        return $this;
    }

    public function getReceiptReference(): ?string
    {
        return $this->receiptReference;
    }

    public function setReceiptReference(?string $receiptReference): self
    {
        $this->receiptReference = $receiptReference;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->priceFormatGet($this->amount);
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $this->priceFormatSet($amount); 

        return $this;
    }

    public function getChequeRemittanceNumber(): ?string
    {
        return $this->chequeRemittanceNumber;
    }

    public function setChequeRemittanceNumber(?string $chequeRemittanceNumber): self
    {
        $this->chequeRemittanceNumber = $chequeRemittanceNumber;

        return $this;
    }

    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }

    public function getBankAccountId(): ?BankAccount
    {
        return $this->bankAccountId;
    }

    public function setBankAccountId(?BankAccount $bankAccountId): self
    {
        $this->bankAccountId = $bankAccountId;

        return $this;
    }
}
