<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json","jsonld"},
 *              attributes={
 *                  "filters"={"translation.groups"},
 *                  "normalization_context"={"groups"={"article_read", "tenantparameters"}},
 *                  "denormalization_context"={"groups"={"article_write"}}
 *              }
 * )
 * @ApiFilter(SearchFilter::class, properties={"tenant":"exact","id":"exact","translation.locale":"exact","translation.title":"partial", "group":"exact","subGroup":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"active"})
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"article_read","article_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"article_read","article_write"})
     */
    private $tenant;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ArticleTranslation", mappedBy="article", cascade={"persist", "remove"})
     * @Groups({"article_read","article_write"})
     */
    protected $translation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="group_id")
     * @Groups({"article_read","article_write"})
     */
    private $group;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="sub_group_id")
     * @Groups({"article_read","article_write"})
     */
    private $subGroup;

    /**
     * @ORM\Column(name="active", type="boolean", nullable=false, options={"default" : 1})
     * @Groups({"article_read","article_write"})
     */
    private $active;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"article_read","article_write"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"article_read","article_write"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"article_read","article_write"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"article_read","article_write"})
     */
    private $updateUserId;

    public function __construct()
    {
        $this->translation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?tenant
    {
        return $this->tenant;
    }

    public function setTenant(?tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    /**
     * @return Collection|ArticleTranslation[]
     */
    public function getTranslation(): Collection
    {
        return $this->translation;
    }

    public function addTranslation(ArticleTranslation $translation): self
    {
        if (!$this->translation->contains($translation)) {
            $this->translation[] = $translation;
            $translation->setArticle($this);
        }

        return $this;
    }

    public function removeTranslation(ArticleTranslation $translation): self
    {
        if ($this->translation->contains($translation)) {
            $this->translation->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getArticle() === $this) {
                $translation->setArticle(null);
            }
        }

        return $this;
    }

    public function getGroup(): ?TenantParameterValue
    {
        return $this->group;
    }

    public function setGroup(?TenantParameterValue $group): self
    {
        $this->group = $group;

        return $this;
    }

    public function getSubGroup(): ?TenantParameterValue
    {
        return $this->subGroup;
    }

    public function setSubGroup(?TenantParameterValue $subGroup): self
    {
        $this->subGroup = $subGroup;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    
    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }
}
