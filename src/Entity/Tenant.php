<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(formats={"json"})
 * @ApiFilter(SearchFilter::class, properties={"name": "partial","relationType":"exact","organization":"exact","active":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\TenantRepository")
 */
class Tenant
{
    /**
     * @ORM\Column(type="integer", unique=true)
     * @Groups({"product_read", "translations"})
     */
    private $id;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"read"})
     * @ApiProperty(identifier=true)
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write", "translations", "thirdparty_read"})
     * @Groups({"product_read", "product_write", "translations"})
     */
    private $code;


    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write", "translations", "thirdparty_read"})
     * @Groups({"product_read", "product_write", "translations"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean",options={"default" : 1})
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $fileStorageBucket;

    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     */
    private $webSite;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     */
    private $priceVatIncluded;

    /**
     * @ORM\Column(type="integer")
     */
    private $fiscalYearFirstMonth;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $officePhoneNumber;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Locality")
     * @ORM\JoinColumn(name="country_id",nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $country;
    
    /**
     * @ORM\Column(name="city_name",type="string", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $cityName;
    
     /**
     * @ORM\Column(name="zip_code",type="string", length=100, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $zipCode;
    
    /**
     * @ORM\Column(type="string", length=400, nullable=true)
     * @Groups({"read", "thirdparty_read", "thirdparty_write"})
     */
    private $street;


    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }


    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getFileStorageBucket(): ?string
    {
        return $this->fileStorageBucket;
    }

    public function setFileStorageBucket(string $fileStorageBucket): self
    {
        $this->fileStorageBucket = $fileStorageBucket;

        return $this;
    }

    public function getWebSite(): ?string
    {
        return $this->webSite;
    }

    public function setWebSite(?string $webSite): self
    {
        $this->webSite = $webSite;

        return $this;
    }

    public function getPriceVatIncluded(): ?bool
    {
        return $this->priceVatIncluded;
    }

    public function setPriceVatIncluded(bool $priceVatIncluded): self
    {
        $this->priceVatIncluded = $priceVatIncluded;

        return $this;
    }

    public function getFiscalYearFirstMonth(): ?int
    {
        return $this->fiscalYearFirstMonth;
    }

    public function setFiscalYearFirstMonth(int $fiscalYearFirstMonth): self
    {
        $this->fiscalYearFirstMonth = $fiscalYearFirstMonth;

        return $this;
    }

    public function getOfficePhoneNumber(): ?string
    {
        return $this->officePhoneNumber;
    }

    public function setOfficePhoneNumber(?string $officePhoneNumber): self
    {
        $this->officePhoneNumber = $officePhoneNumber;

        return $this;
    }


}
