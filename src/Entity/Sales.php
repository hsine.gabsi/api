<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotation\TenantAware;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json", "jsonld"},
 *               collectionOperations={
 *                    "get"={"normalization_context"={"groups"={"sales_read", "translations", "thirdparties"}}},
 *                    "post"={"denormalization_context"={"groups"="sales_post"}}
 *              },
 *              itemOperations={
 *                    "get"={"normalization_context"={"groups"={"sales_read", "translations", "thirdparties"}}},
 *                    "put"={"denormalization_context"={"groups"="sales_put"}}, 
 *                    "delete"
 *              }
 * )
 * @ApiFilter(SearchFilter::class, properties={"customer":"exact", "productGroup":"exact","productSubGroup":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\SalesRepository")
 * @ORM\Table(name="sales",uniqueConstraints={@ORM\UniqueConstraint(name="uniq_sales_name", columns={"tenant_code", "product_group_id", "product_sub_group_id", "customer_id", "period"})})
 */
class Sales
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"sales_read", "sales_post"})
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"sales_read", "sales_post"})
     */
    private $tenant;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     * @Assert\NotBlank(message="Le period date est obligatoire")
     */
    private $period;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="product_group_id", referencedColumnName="id", nullable=true)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     */
    private $productGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="product_sub_group_id", referencedColumnName="id", nullable=true)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     */
    private $productSubGroup;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     */
    private $customer;

   /**
     * @ORM\Column(name="total_amount_incl_tax", type="decimal", nullable=false)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     * @Assert\NotBlank(message="Le champ totalAmountInclTax est obligatoire")
     */
    private $totalAmountInclTax;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     */
    private $updateUserId;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"sales_read", "sales_post", "sales_put"})
     */
    private $additionalInformation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(?string $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getProductGroup(): ?TenantParameterValue
    {
        return $this->productGroup;
    }

    public function setProductGroup(?TenantParameterValue $productGroup): self
    {
        $this->productGroup = $productGroup;

        return $this;
    }

    public function getProductSubGroup(): ?TenantParameterValue
    {
        return $this->productSubGroup;
    }

    public function setProductSubGroup(?TenantParameterValue $productSubGroup): self
    {
        $this->productSubGroup = $productSubGroup;

        return $this;
    }

    public function getCustomer(): ?ThirdParty
    {
        return $this->customer;
    }

    public function setCustomer(?ThirdParty $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getTotalAmountInclTax(): ?string
    {
        return $this->priceFormatGet($this->totalAmountInclTax);
        // return $this->total_amount_incl_tax;
    }

    // Le set doit changer en string au lieu de int $total_amount_incl_tax
    public function setTotalAmountInclTax(?string $totalAmountInclTax): ?int
    {
        // $this->total_amount_incl_tax = $total_amount_incl_tax;
        $this->totalAmountInclTax = $this->priceFormatSet($totalAmountInclTax);
        return $this->totalAmountInclTax;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }
}
