<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *  formats={"json"},
 *     collectionOperations ={
 *              "get" : {
 *                          "method": "GET",
 *                          "normalization_context"={"groups"={"business_read"}}
 *              },
 *              "POST"
 *      },
 *     itemOperations ={"GET"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\BusinessEntityRepository")
 * @ApiFilter(BooleanFilter::class, properties={"hasGroup","hasTag", "hasDetail", "hasTask", "hasArticle", "hasDocument", "hasSubGroup", "hasScreen"})
 * @ApiFilter(SearchFilter::class, properties={"code":"exact"})
 */
class BusinessEntity
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", length=100)
     * @Groups({"read", "business_read"})
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write","objectmedia_read","objectdetail_read", "translations"})
     */
    private $code;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\MasterCode", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="code", referencedColumnName="code",nullable=false)
     */
    private $entity;

    /**
     * @ORM\Column(type="boolean",options={"default" : 1})
     * @Groups({"read", "business_read"})
     */
    private $active;
     /**
     * @Groups({"masterCode_read", "business_read"})
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write","objectmedia_read","objectdetail_read", "translations"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasGroup;


    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasFile;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasMedia;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasComment;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasMember;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasTag;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasAddress;

    /**
     * @ORM\Column(type="boolean",options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasPhone;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    private $hasDetail;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"business_read"})
     */
    private $tableName;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasTask;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasArticle;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasDocument;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"business_read"})
     */
    private $hasSubGroup;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     * @Groups({"business_read"})
     */
    private $hasScreen;

    public function setName($name)
    {
        $this->getEntity()->setName($name);
    }
    public function getName(): ?string
    {
        return $this->getEntity()->getName();
    }


    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getEntity(): ?MasterCode
    {
        return $this->entity;
    }

    public function setEntity(MasterCode $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getHasGroup(): ?bool
    {
        return $this->hasGroup;
    }

    public function setHasGroup(bool $hasGroup): self
    {
        $this->hasGroup = $hasGroup;

        return $this;
    }

    public function getHasFile(): ?bool
    {
        return $this->hasFile;
    }

    public function setHasFile(?bool $hasFile): self
    {
        $this->hasFile = $hasFile;

        return $this;
    }

    public function getHasMedia(): ?bool
    {
        return $this->hasMedia;
    }

    public function setHasMedia(?bool $hasMedia): self
    {
        $this->hasMedia = $hasMedia;

        return $this;
    }

    public function getHasComment(): ?bool
    {
        return $this->hasComment;
    }

    public function setHasComment(?bool $hasComment): self
    {
        $this->hasComment = $hasComment;

        return $this;
    }

    public function getHasMember(): ?bool
    {
        return $this->hasMember;
    }

    public function setHasMember(?bool $hasMember): self
    {
        $this->hasMember = $hasMember;

        return $this;
    }

    public function getHasTag(): ?bool
    {
        return $this->hasTag;
    }

    public function setHasTag(?bool $hasTag): self
    {
        $this->hasTag = $hasTag;

        return $this;
    }

    public function getHasAddress(): ?bool
    {
        return $this->hasAddress;
    }

    public function setHasAddress(?bool $hasAddress): self
    {
        $this->hasAddress = $hasAddress;

        return $this;
    }

    public function getHasPhone(): ?bool
    {
        return $this->hasPhone;
    }

    public function setHasPhone(?bool $hasPhone): self
    {
        $this->hasPhone = $hasPhone;

        return $this;
    }

    public function getTableName(): ?string
    {
        return $this->tableName;
    }

    public function setTableName(?string $tableName): self
    {
        $this->tableName = $tableName;

        return $this;
    }

    public function getHasDetail(): ?bool
    {
        return $this->hasDetail;
    }

    public function setHasDetail(bool $hasDetail): self
    {
        $this->hasDetail = $hasDetail;

        return $this;
    }

    public function getHasTask(): ?bool
    {
        return $this->hasTask;
    }

    public function setHasTask(bool $hasTask): self
    {
        $this->hasTask = $hasTask;

        return $this;
    }

    public function getHasArticle(): ?bool
    {
        return $this->hasArticle;
    }

    public function setHasArticle(bool $hasArticle): self
    {
        $this->hasArticle = $hasArticle;

        return $this;
    }

    public function getHasDocument(): ?bool
    {
        return $this->hasDocument;
    }

    public function setHasDocument(bool $hasDocument): self
    {
        $this->hasDocument = $hasDocument;

        return $this;
    }

    public function getHasSubGroup(): ?bool
    {
        return $this->hasSubGroup;
    }

    public function setHasSubGroup(bool $hasSubGroup): self
    {
        $this->hasSubGroup = $hasSubGroup;

        return $this;
    }

    public function getHasScreen(): ?bool
    {
        return $this->hasScreen;
    }

    public function setHasScreen(bool $hasScreen): self
    {
        $this->hasScreen = $hasScreen;

        return $this;
    }


}
