<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Locastic\ApiPlatformTranslationBundle\Model\AbstractTranslatable;
use Locastic\ApiPlatformTranslationBundle\Model\TranslationInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(formats={"json"},
 *     collectionOperations={
 *        "get" : {"method": "GET"},
 *        "post" : {
 *           "method": "POST",
 *           "normalization_context"={"groups"={"translations"}},
 *        }
 *     },
 *     itemOperations={
 *        "get" : {"method": "GET"},
 *        "put" : {
 *           "method": "PUT",
 *           "normalization_context"={"groups"={"translations"}},
 *        }
 *     },
 *     attributes={
 *        "filters"={"translation.groups"},
 *        "normalization_context"={"groups"={"locality_read"}},
 *        "denormalization_context"={"groups"={"locality_write"}}
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={"id":"exact","translations.name":"partial"})
 * @ORM\Entity(repositoryClass="App\Repository\LocalityRepository")
 */
class Locality extends AbstractTranslatable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"locality_read", "translations"})
     * @Groups({"product_read","productlocality_read","translations"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code",nullable=false)
     * @Groups({"locality_read", "locality_write", "translations"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"locality_read", "locality_write", "translations"})
     * @Groups({"product_read", "translations"})
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"locality_read","locality_write","autosuggest_locality_read"})
     */
    private $tenant;

    /**
     * @ORM\Column(type="decimal", precision=19, scale=8, nullable=true)
     * @Groups({"locality_read", "locality_write", "translations"})
     */
    private $longitude;

    /**
     * @ORM\Column(type="decimal", precision=18, scale=8, nullable=true)
     * @Groups({"locality_read", "locality_write", "translations"})
     */
    private $latitude;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Locality")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id",nullable=true)
     * @Groups({"locality_read", "locality_write", "translations"})
     */
    private $parent;

    /**
     * @Groups({"locality_read", "locality_write", "translations"})
     * @Groups({"product_read","translations"})
     */
    private $name;

    /**
     * @Groups({"locality_read", "locality_write", "translations"})
     * @ORM\OneToMany(targetEntity="App\Entity\LocalityTranslation", mappedBy="locality", cascade={"persist", "remove"})
     */
    protected $translations;

    /**
     * @ORM\Column(name="calling_code", type="string", length=100, nullable=true)
     * @Groups({"locality_read", "locality_write", "translations"})
     */
    private $calling;

    public function __construct()
    {
        parent::__construct();
    }

    public function setName($name)
    {
        $this->getTranslation()->setName($name);
    }

    public function getName(): string
    {
        return $this->getTranslation()->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getParent(): ?Locality
    {
        return $this->parent;
    }

    public function setParent(?Locality $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    protected function createTranslation():TranslationInterface
    {
        return new LocalityTranslation();
    }

    /**
     * @return Collection|LocalityTranslation[]
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }
    public function addTranslation(TranslationInterface $translation): void
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setLocality($this);
        }
    }

    public function removeTranslation(TranslationInterface $translation): void
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getLocality() === $this) {
                $translation->setLocality(null);
            }
        }
    }

    public function getCalling(): ?string
    {
        return $this->calling;
    }

    public function setCalling(?string $calling): self
    {
        $this->calling = $calling;

        return $this;
    }

}
