<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Locastic\ApiPlatformTranslationBundle\Model\AbstractTranslatable;
use Locastic\ApiPlatformTranslationBundle\Model\TranslationInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(
 * formats={"json","jsonld"},
 *     collectionOperations={
 *        "get" : {"method": "GET"},
 *        "post" : {
 *           "method": "POST",
 *           "normalization_context"={"groups"={"translations"}},
 *
 *        }
 *     },
 *     itemOperations={
 *        "get" : {"method": "GET"},
 *        "put" : {
 *           "method": "PUT",
 *           "normalization_context"={"groups"={"translations"}},
 *        }
 *     } ,
 *     attributes={
 *        "filters"={"translation.groups"},
 *        "normalization_context"={"groups"={"TenantParameterValue_read"}},
 *        "denormalization_context"={"groups"={"TenantParameterValue_write"}}
 *     })
 * @ApiFilter(SearchFilter::class, properties={"tenant":"exact","parameter":"exact","entity":"exact","parentValue":"exact","id":"exact","translations.name":"partial","translations.locale":"exact","valueCode":"partial"})
 * @ORM\Entity(repositoryClass="App\Repository\TenantParameterValueRepository")
 * @ORM\Table(name="tenant_parameter_value",uniqueConstraints={@ORM\UniqueConstraint(name="uniq_parameterValueTenant", columns={"value_code", "tenant_code"})})
 */
class TenantParameterValue extends AbstractTranslatable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"TenantParameterValue_read", "translations", "tenantparameters", "order_read","supplierchargegroup_read","forecast_read"})
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write", "translations", "media_read", "objectdetail_read"})
     */
    private $valueCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code",nullable=false)
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write"})
     */
    private $tenant;
     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterCode")
     * @ORM\JoinColumn(name="parameter_code", referencedColumnName="code",nullable=false)
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write"})
     */
    private $parameter;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code",nullable=false)
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write"})
     */
    private $entity;


    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write"})
     */
    private $rank;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write"})
     */
    private $active;


     /**
     * @Groups({"TenantParameterValue_read","media_read","objectdetail_read", "translations", "tenantparameters", "order_read","supplierchargegroup_read","forecast_read"})
     */
    private $name;

     /**
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write"})
     * @ORM\JoinColumn(name="value_code", referencedColumnName="value_code")
     * @ORM\OneToMany(targetEntity="App\Entity\TenantParameterValueTranslation", mappedBy="tenantParameterValue", cascade={"persist", "remove"})
     */
    protected $translations;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue", inversedBy="tenantParameterValuesByParent")
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write"})
     */
    private $parentValue;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"TenantParameterValue_read", "TenantParameterValue_write"})
     */
    private $public;



    public function setName($name)
    {
        $this->getTranslation()->setName($name);
    }

    public function getName(): string
    {
        return $this->getTranslation()->getName();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValueCode(): ?string
    {
        return $this->valueCode;
    }

    public function setValueCode(?string $valueCode): self
    {
        $this->valueCode = $valueCode;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getParameter(): ?MasterCode
    {
        return $this->parameter;
    }

    public function setParameter(?MasterCode $parameter): self
    {
        $this->parameter = $parameter;

        return $this;
    }
    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }
    protected function createTranslation():TranslationInterface
    {
        return new TenantParameterValueTranslation();
    }

    /**
     * @return Collection|TenantParameterValueTranslation[]
     */
    public function getTranslations(): Collection
    {
        return $this->translations;
    }
    public function addTranslation(TranslationInterface $translation): void
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setTenantParameterValue($this);
        }
    }

    public function removeTranslation(TranslationInterface $translation): void
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getTenantParameterValue() === $this) {
                $translation->setTenantParameterValue(null);
            }
        }
    }


    public function getParentValue(): ?self
    {
        return $this->parentValue;
    }

    public function setParentValue(?self $parentValue): self
    {
        $this->parentValue = $parentValue;

        return $this;
    }

    public function getPublic(): ?bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;

        return $this;
    }









}
