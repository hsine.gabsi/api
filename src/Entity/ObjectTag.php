<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json"},
 *              normalizationContext={"groups"={"objecttag_read"}},
 *              denormalizationContext={"groups"={"objecttag_write"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"entity":"exact","objectId":"exact", "tenant":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\ObjectTagRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_tag_object", columns={"tag_id", "object_id","entity_code"})})
 */

class ObjectTag
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"objecttag_read","objecttag_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code",nullable=false)
     * @Assert\NotBlank(message="L'entity est obligatoire")
     * @Groups({"objecttag_read","objecttag_write"})
     */
    private $entity;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le champ objectId est obligatoire")
     * @Groups({"objecttag_read","objecttag_write"})
     */
    private $objectId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Le tag est obligatoire")
     * @Groups({"objecttag_read","objecttag_write"})
     */
    private $tag;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"objecttag_read","objecttag_write"})
     */
    private $rank;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le tenant est obligatoire")
     * @Groups({"objecttag_read","objecttag_write"})
     */
    private $tenant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getTag(): ?TenantParameterValue
    {
        return $this->tag;
    }

    public function setTag(?TenantParameterValue $tag): self
    {
        $this->tag = $tag;

        return $this;
    }

    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }
}
