<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(formats={"json"},
 *              collectionOperations={
 *                      "get_publication"={
 *                      "method"="GET",
 *                      "path"="/currencies",
 *                      "controller"=CurrencyController::class
 *          }
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\CurrencyRepository")
 */
class Currency
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", length=3)
     * @Groups({"read"})
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=30,nullable=true)
     */
    private $symbol;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $decimals;

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getDecimals(): ?int
    {
        return $this->decimals;
    }

    public function setDecimals(?int $decimals): self
    {
        $this->decimals = $decimals;

        return $this;
    }
}
