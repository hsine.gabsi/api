<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Annotation\TenantAware;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json", "jsonld"},
 *                       collectionOperations={
 *                          "get"={"normalization_context"={"groups"={"salesorder_read", "thirdparties", "masterParameterValues"}}},
 *                          "post"={"denormalization_context"={"groups"="salesorder_post"}}
 *                       },
 *                       itemOperations={
 *                          "get"={"normalization_context"={"groups"={"salesorder_read", "thirdparties", "masterParameterValues"}}},
 *                          "put"={"denormalization_context"={"groups"="salesorder_put"}}, 
 *                          "delete"
 *                       })
 * @ORM\Entity(repositoryClass="App\Repository\SalesOrderRepository")
 */
class SalesOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"salesorder_read", "salesorder_post"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="third_party_id", nullable=false)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     * @Assert\NotBlank(message="Le champ thirdparty est obligatoire")
     */
    private $thirdParty;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $number;

    /**
     * @ORM\Column(name="total_amount_incl_tax", type="decimal")
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     * @Assert\NotBlank(message="Le champ totalAmountExclTax est obligatoire")
     */
    private $totalAmountInclTax;

    /**
     * @Groups({"salesorder_read"})
     */
    private $totalAmountExclTax;

    /**
     * @ORM\Column(name="vat_amount", type="decimal", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $vatAmount;

    /**
     * @ORM\Column(type="date")
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     * @Assert\NotBlank(message="Le champ date est obligatoire")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="discount_type_code", referencedColumnName="code", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $discountType;

    /**
     * @ORM\Column(name="discount_value", type="decimal", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $discountValue;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="payment_status_type_code", referencedColumnName="code", nullable=false)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     * @Assert\NotBlank(message="Le champ paymentStatusType est obligatoire")
     */
    private $paymentStatusType;

    /**
     * @ORM\Column(name="remaining_amount_due", type="decimal", nullable=false)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     * @Assert\NotBlank(message="Le champ remainingAmountDue est obligatoire")
     */
    private $remainingAmountDue;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SalesOrderPrice", mappedBy="salesOrder", orphanRemoval=true, cascade={"persist"})
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $salesOrderPrices;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $fromDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $toDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $updateUserId;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $additionalInformation;

    public function __construct()
    {
        $this->salesOrderPrices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getTotalAmountInclTax(): ?string
    {
        //return $this->totalAmountInclTax;
        return $this->priceFormatGet($this->totalAmountInclTax);
    }

    public function setTotalAmountInclTax(string $totalAmountInclTax): self
    {
        //$this->totalAmountInclTax = $totalAmountInclTax;
        $this->totalAmountInclTax = $this->priceFormatSet($totalAmountInclTax);

        return $this;
    }

    public function getTotalAmountExclTax(): ?string
    {
        $this->totalAmountExclTax = $this->totalAmountInclTax - $this->vatAmount;
        return $this->priceFormatGet($this->totalAmountExclTax);
    }

    public function getVatAmount(): ?string
    {
        //return $this->vatAmount;
        return $this->priceFormatGet($this->vatAmount);
    }

    public function setVatAmount(string $vatAmount): self
    {
        //$this->vatAmount = $vatAmount;
        $this->vatAmount = $this->priceFormatSet($vatAmount);
        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDiscountType(): ?MasterParameterValue
    {
        return $this->discountType;
    }

    public function setDiscountType(?MasterParameterValue $discountType): self
    {
        $this->discountType = $discountType;

        return $this;
    }

    public function getDiscountValue(): ?string
    {
        //return $this->discountValue;
        return $this->priceFormatGet($this->discountValue);
    }

    public function setDiscountValue(?string $discountValue): self
    {
        //$this->discountValue = $discountValue;
        $this->vatAmount = $this->priceFormatSet($discountValue);
        return $this;
    }

    public function getPaymentStatusType(): ?MasterParameterValue
    {
        return $this->paymentStatusType;
    }

    public function setPaymentStatusType(?MasterParameterValue $paymentStatusType): self
    {
        $this->paymentStatusType = $paymentStatusType;

        return $this;
    }

    public function getRemainingAmountDue(): ?string
    {
        //return $this->remainingAmountDue;
        return $this->priceFormatGet($this->remainingAmountDue);
    }

    public function setRemainingAmountDue(string $remainingAmountDue): self
    {
        //$this->remainingAmountDue = $remainingAmountDue;
        $this->remainingAmountDue = $this->priceFormatSet($remainingAmountDue);
        return $this;
    }

    public function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    public function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }

    /**
     * @return Collection|SalesOrderPrice[]
     */
    public function getSalesOrderPrices(): Collection
    {
        return $this->salesOrderPrices;
    }

    public function addSalesOrderPrice(SalesOrderPrice $salesOrderPrice): self
    {
        if (!$this->salesOrderPrices->contains($salesOrderPrice)) {
            $this->salesOrderPrices[] = $salesOrderPrice;
            $salesOrderPrice->setSalesOrder($this);
        }

        return $this;
    }

    public function removeSalesOrderPrice(SalesOrderPrice $salesOrderPrice): self
    {
        if ($this->salesOrderPrices->contains($salesOrderPrice)) {
            $this->salesOrderPrices->removeElement($salesOrderPrice);
            // set the owning side to null (unless already changed)
            if ($salesOrderPrice->getSalesOrder() === $this) {
                $salesOrderPrice->setSalesOrder(null);
            }
        }

        return $this;
    }

    public function getFromDate(): ?\DateTimeInterface
    {
        return $this->fromDate;
    }

    public function setFromDate(?\DateTimeInterface $fromDate): self
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    public function getToDate(): ?\DateTimeInterface
    {
        return $this->toDate;
    }

    public function setToDate(?\DateTimeInterface $toDate): self
    {
        $this->toDate = $toDate;

        return $this;
    }

    
    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }
	
}
