<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Locastic\ApiPlatformTranslationBundle\Model\AbstractTranslation;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(formats={"json"})
 * @ORM\Entity(repositoryClass="App\Repository\ProductTranslationRepository")
 */
class ProductTranslation extends AbstractTranslation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"product_read", "product_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\product", inversedBy="translations")
     * @ORM\JoinColumn(name="product_id", nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"product_read","product_write"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=2)
     * @Groups({"product_read", "product_write", "translations"})
     */
    protected $locale;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?product
    {
        return $this->product;
    }

    public function setProduct(?product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

       public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): void
    {
        $this->locale = $locale;
    }
}
