<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ApiResource(formats={"json"},
 *              normalizationContext={"groups"={"objectemail_read", "translations"}},
 *              denormalizationContext={"groups"={"objectemail_write"}})
 * @ORM\Entity(repositoryClass="App\Repository\ObjectEmailRepository")
 * @ApiFilter(SearchFilter::class, properties={"entity":"exact","objectId":"exact"})
 */
class ObjectEmail
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"objectemail_read", "objectemail_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectemail_read", "objectemail_write", "translations"})
     * @Assert\NotBlank(message="L'entity est obligatoire")
     */
    private $entity;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"objectemail_read", "objectemail_write"})
     * @Assert\NotBlank(message="L'objectId est obligatoire")
     */
    private $objectId;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"objectemail_read", "objectemail_write"})
     * @Assert\NotBlank(message="L'email est obligatoire")
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="business_unit_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectemail_read", "objectemail_write"})
     * @Assert\NotBlank(message="Le businessUnit est obligatoire")
     */
    private $businessUnit;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"objectemail_read", "objectemail_write"})
     * @Assert\NotBlank(message="Le tenant est obligatoire")
     */
    private $tenant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBusinessUnit(): ?MasterParameterValue
    {
        return $this->businessUnit;
    }

    public function setBusinessUnit(?MasterParameterValue $businessUnit): self
    {
        $this->businessUnit = $businessUnit;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }
}
