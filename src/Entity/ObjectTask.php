<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json"},
 *              attributes={
 *                  "normalization_context"={"groups"={"objecttask_read"}},
 *                  "denormalization_context"={"groups"={"objecttask_write"}}
 *              })
 * @ORM\Entity(repositoryClass="App\Repository\ObjectTaskRepository")
 * @ApiFilter(SearchFilter::class, properties={"task.id":"exact"})
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_tenant_media_object", columns={"tenant_code","entity_code", "object_id","task_id"})})
 */
class ObjectTask
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"objecttask_read", "objecttask_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"objecttask_read", "objecttask_write"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code", nullable=false)
     * @Groups({"objecttask_read", "objecttask_write"})
     * @Assert\NotBlank(message="Le champ entity est obligatoire")
     */
    private $entity;

    /**
     * @ORM\Column(name="object_id", type="integer", nullable=true)
     * @Groups({"objecttask_read", "objecttask_write"})
     */
    private $objectId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Task")
     * @ORM\JoinColumn(name="task_id", nullable=false)
     * @Groups({"objecttask_read", "objecttask_write"})
     * @Assert\NotBlank(message="Le champ external object est obligatoire")
     */
    private $task;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getObjectId(): ?int
    {
        return $this->objectId;
    }

    public function setObjectId(?int $objectId): self
    {
        $this->objectId = $objectId;

        return $this;
    }

    public function getTask(): ?Task
    {
        return $this->task;
    }

    public function setTask(?Task $task): self
    {
        $this->task = $task;

        return $this;
    }

}
