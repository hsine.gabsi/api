<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(formats={"json", "jsonld"})
 * @ORM\Entity(repositoryClass="App\Repository\SalesOrderPriceRepository")
 */
class SalesOrderPrice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product")
     * @ORM\JoinColumn(name="product_id", nullable=true)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     * @Assert\NotBlank(message="Le champ name est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(name="unit_price", type="decimal", nullable=false)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     * @Assert\NotBlank(message="Le champ unitPrice est obligatoire")
     */
    private $unitPrice;

    /**
     * @ORM\Column(type="decimal", precision=6, scale=2, nullable=false)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     * @Assert\NotBlank(message="Le champ quantity est obligatoire")
     */
    private $quantity;

    // /**
    //  * @ORM\ManyToOne(targetEntity="App\Entity\SalesOrder", inversedBy="salesOrderPrice")
    //  * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
    //  * @ORM\JoinColumn(name="sales_order_id", nullable=false)
    //  */
    // private $salesOrder;

    /**
     * @Groups({"salesorder_read"})
     */
    private $totalPrice;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SalesOrder", inversedBy="salesOrderPrices")
     * @ORM\JoinColumn(name="sales_order_id", nullable=false)
     * @Groups({"salesorder_read", "salesorder_post", "salesorder_put"})
     */
    private $salesOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUnitPrice(): ?string
    {
        //return $this->unitPrice;
        //return $this->salesOrder->priceFormatGet($this->unitPrice);
        return $this->unitPrice/100;
    }

    public function setUnitPrice(?int $unitPrice): self
    { 
        //$this->unitPrice = $unitPrice;
        //$this->unitPrice = $this->salesOrder->priceFormatSet($unitPrice);
        $this->unitPrice = $unitPrice*100;

        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    // public function getSalesOrder(): ?SalesOrder
    // {
    //     return $this->salesOrder;
    // }

    // public function setSalesOrder(?SalesOrder $salesOrder): self
    // {
    //     $this->salesOrder = $salesOrder;

    //     return $this;
    // }

    public function getTotalPrice(): ?string
    { 
        $this->totalPrice = $this->unitPrice * $this->quantity;
        return $this->salesOrder->priceFormatGet($this->totalPrice);
    }

    // private function priceFormatGet(?string $price):?string
    // {
    //     if (is_numeric($price)) {
    //         $price = $price / pow(10, $this->salesOrder->getCurrency()->getDecimals());
    //         return number_format($price, $this->salesOrder->getCurrency()->getDecimals(), '.', ' ');
    //     } else {
    //         return $price;
    //     }
    // }

    // private function priceFormatSet($price)
    // {
    //     $price = str_replace(' ', '', $price);
    //     $price = str_replace(',', '.', $price);

    //     if (is_numeric($price)) {
    //         $price = $price * pow(10, $this->salesOrder->getCurrency()->getDecimals());
    //         $pos = strpos((string)$price, ".");
    //         if ($pos > 0) {
    //             $price = substr($price, 0, $pos);
    //         }
    //     } else {
    //         return NULL;
    //     }
    //     return $price;
    // }

    public function getSalesOrder(): ?SalesOrder
    {
        return $this->salesOrder;
    }

    public function setSalesOrder(?SalesOrder $salesOrder): self
    {
        $this->salesOrder = $salesOrder;

        return $this;
    }
}
