<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Annotation\TenantAware;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\ExistsFilter;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(attributes={"order"={"date": "DESC"}} ,formats={"json", "jsonld"},
 *                       collectionOperations={
 *                          "get"={"normalization_context"={"groups"={"purchaseinvoice_read", "thirdparties", "masterParameterValues"}}},
 *                          "post"={"denormalization_context"={"groups"="purchaseinvoice_post"}}
 *                       },
 *                       itemOperations={
 *                          "get"={"normalization_context"={"groups"={"purchaseinvoice_read", "thirdparties", "masterParameterValues"}}},
 *                          "put"={"denormalization_context"={"groups"="purchaseinvoice_put"}}, 
 *                          "delete"
 *                       }
 * )
 * @ApiFilter(SearchFilter::class, properties={"id":"exact", "thirdParty.id":"exact", "paymentStatusType":"exact", "number":"partial", "checkType":"exact", "chargeGroup":"exact","withoutProof":"exact", "invoicePayments.payment":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"creditNote"})
 * @ApiFilter(DateFilter::class, properties={"date","dueDate"})
 * @ApiFilter(RangeFilter::class, properties={"totalAmountInclTax"})
 * @ApiFilter(ExistsFilter::class, properties={"chargeGroup"})
 * @ORM\Entity(repositoryClass="App\Repository\PurchaseInvoiceRepository")
 */
class PurchaseInvoice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="third_party_id", nullable=false)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     * @Assert\NotBlank(message="Le champ thirdParty est obligatoire")
     */
    private $thirdParty;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     * @Assert\NotBlank(message="Le champ number est obligatoire")
     */
    private $number;

    /**
     * @ORM\Column(name="total_amount_incl_tax", type="decimal", nullable=false)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     * @Assert\NotBlank(message="Le champ totalAmountInclTax est obligatoire")
     */
    private $totalAmountInclTax;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default" : 0})
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $creditNote ;

    /**
     * @ORM\Column(name="vat_amount", type="decimal", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $vatAmount;

    /**
     * @ORM\Column(type="date", nullable=false)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     * @Assert\NotBlank(message="Le champ date est obligatoire")
     */
    private $date;

    /**
     * @Groups({"purchaseinvoice_read"})
     */
    private $totalAmountExclTax;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="charge_type_code", referencedColumnName="code", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $chargeType;

    /**
    * @ORM\Column(type="date", nullable=true)
    * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
    */
    private $dueDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="check_type_code", referencedColumnName="code", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $checkType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="payment_status_type_code", referencedColumnName="code", nullable=false)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     * @Assert\NotBlank(message="Le champ paiement statut type code est obligatoire")
     */
    private $paymentStatusType;

    /**
     * @ORM\Column(name="remaining_amount_due", type="decimal", nullable=false)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     * @Assert\NotBlank(message="Le champ remaining amount due est obligatoire")
     */
    private $remainingAmountDue;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="charge_group_id", referencedColumnName="id", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $chargeGroup;

    /**
     * @ORM\Column(type="boolean", nullable=false , options={"default" : 0})
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $withoutProof;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PurchaseInvoice")
     * @ORM\JoinColumn(name="related_purchase_invoice_id", referencedColumnName="id", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $relatedPurchaseInvoice;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $fromDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $toDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $updateUserId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PurchaseInvoicePayment", mappedBy="purchaseInvoice")
     * @Groups({"purchaseinvoice_read"})
     */
    private $invoicePayments;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"purchaseinvoice_read", "purchaseinvoice_post", "purchaseinvoice_put"})
     */
    private $additionalInformation;

    public function __construct()
    {
        $this->invoiceDetails = new ArrayCollection();
        $this->invoicePayments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    } 

    public function getTotalAmountExclTax(): ?string
    {
        $this->totalAmountExclTax = $this->totalAmountInclTax - $this->vatAmount;
        return $this->priceFormatGet($this->totalAmountExclTax);
    }

    public function getTotalAmountInclTax(): ?string
    {
        return $this->priceFormatGet($this->totalAmountInclTax);
    }

    public function setTotalAmountInclTax(?string $totalAmountInclTax): ?int
    {
        $this->totalAmountInclTax = $this->priceFormatSet($totalAmountInclTax);
        return $this->totalAmountInclTax;
    }

    public function getVatAmount(): ?string
    {
        return $this->priceFormatGet($this->vatAmount);
    }

    public function setVatAmount(?string $vatAmount): self
    {
        $this->vatAmount = $this->priceFormatSet($vatAmount);
        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCreditNote(): ?bool
    {
        return $this->creditNote;
    }

    public function setCreditNote(bool $creditNote): self
    {
        $this->creditNote = $creditNote;

        return $this;
    }    

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getChargeType(): ?MasterParameterValue
    {
        return $this->chargeType;
    }

    public function setChargeType(?MasterParameterValue $chargeType): self
    {
        $this->chargeType = $chargeType;

        return $this;
    }

    public function getCheckType(): ?MasterParameterValue
    {
        return $this->checkType;
    }

    public function setCheckType(?MasterParameterValue $checkType): self
    {
        $this->checkType = $checkType;

        return $this;
    }  
    public function getDueDate(): ?\DateTimeInterface
    {
        return $this->dueDate;
    }

    public function setDueDate(\DateTimeInterface $dueDate): self
    {
        $this->dueDate = $dueDate;

        return $this;
    }

    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }

    public function getPaymentStatusType(): ?MasterParameterValue
    {
        return $this->paymentStatusType;
    }

    public function setPaymentStatusType(?MasterParameterValue $paymentStatusType): self
    {
        $this->paymentStatusType = $paymentStatusType;

        return $this;
    }

    public function getRemainingAmountDue(): ?string
    {
        return $this->priceFormatGet($this->remainingAmountDue);
    }

    public function setRemainingAmountDue(string $remainingAmountDue): ?int
    {
        $this->remainingAmountDue = $this->priceFormatSet($remainingAmountDue);
        return $this->remainingAmountDue;
    }   
    
    public function getChargeGroup(): ?TenantParameterValue
    {
        return $this->chargeGroup;
    }

    public function setChargeGroup(?TenantParameterValue $chargeGroup): self
    {
        $this->chargeGroup = $chargeGroup;

        return $this;
    }

    public function getWithoutProof(): ?bool
    {
        return $this->withoutProof;
    }

    public function setWithoutProof(bool $withoutProof): self
    {
        $this->withoutProof = $withoutProof;

        return $this;
    }

    public function getRelatedPurchaseInvoice(): ?self
    {
        return $this->relatedPurchaseInvoice;
    }

    public function setRelatedPurchaseInvoice(?self $relatedPurchaseInvoice): self
    {
        $this->relatedPurchaseInvoice = $relatedPurchaseInvoice;

        return $this;
    }

    public function getFromDate(): ?\DateTimeInterface
    {
        return $this->fromDate;
    }

    public function setFromDate(?\DateTimeInterface $fromDate): self
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    public function getToDate(): ?\DateTimeInterface
    {
        return $this->toDate;
    }

    public function setToDate(?\DateTimeInterface $toDate): self
    {
        $this->toDate = $toDate;

        return $this;
    }

    
    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }

    /**
     * @return Collection|PurchaseInvoicePayment[]
     */
    public function getInvoicePayments(): Collection
    {
        return $this->invoicePayments;
    }

    public function addInvoicePayment(PurchaseInvoicePayment $invoicePayment): self
    {
        if (!$this->invoicePayments->contains($invoicePayment)) {
            $this->invoicePayments[] = $invoicePayment;
            $invoicePayment->setPurchaseInvoice($this);
        }

        return $this;
    }

    public function removeInvoicePayment(PurchaseInvoicePayment $invoicePayment): self
    {
        if ($this->invoicePayments->contains($invoicePayment)) {
            $this->invoicePayments->removeElement($invoicePayment);
            // set the owning side to null (unless already changed)
            if ($invoicePayment->getPurchaseInvoice() === $this) {
                $invoicePayment->setPurchaseInvoice(null);
            }
        }

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }

}