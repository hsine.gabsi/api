<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource(formats={"json"})
 * @ORM\Entity(repositoryClass="App\Repository\ArticleTranslationRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_article_locale", columns={"article_id", "locale"})})
 */
class ArticleTranslation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"article_read","article_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Article", inversedBy="translation")
     * @ORM\JoinColumn(name="article_id", nullable=false)
     */
    private $article;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="locale", referencedColumnName="code", nullable=false)
     * @Groups({"article_read","article_write"})
     */
    private $locale;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"article_read","article_write"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"article_read","article_write"})
     */
    private $body;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticle(): ?Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article): self
    {
        $this->article = $article;

        return $this;
    }

    public function getLocale(): ?MasterParameterValue
    {
        return $this->locale;
    }

    public function setLocale(?MasterParameterValue $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }
}
