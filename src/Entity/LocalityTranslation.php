<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiProperty;
use Locastic\ApiPlatformTranslationBundle\Model\AbstractTranslation;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ApiResource(formats={"json"})
 * @ORM\Entity(repositoryClass="App\Repository\LocalityTranslationRepository")
 * @ORM\Table(name="locality_translation",uniqueConstraints={@ORM\UniqueConstraint(name="uniq_locality_name", columns={"name", "locale"})})
 */
class LocalityTranslation extends AbstractTranslation
{   
     /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"locality_read", "locality_write", "translations"})
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Locality", inversedBy="translations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"locality_read", "locality_write", "translations"})
     */
    private $locality;
    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"locality_read", "locality_write", "translations"})
     * @Assert\NotBlank(message="Le nom de localité est obligatoire")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=2)
     * @Groups({"locality_read", "locality_write", "translations"})
     */
    protected $locale;

    public function getId(): ?int
    {
        return $this->id;
    }
    public function getLocality(): ?Locality
    {
        return $this->locality;
    }

    public function setLocality(?Locality $locality): self
    {
        $this->locality = $locality;

        return $this;
    }

    
    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function setLocale(?string $locale): void
    {
        $this->locale = $locale;
    }

}
