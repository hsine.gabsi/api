<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *      formats={"json"},
 *      collectionOperations={"get"},
 *      itemOperations={"get"},
 *      attributes={"order"={"rank": "ASC"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"parameter":"exact","entity":"exact","parentValue":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\MasterParameterValueRepository")
 */
class MasterParameterValue
{
     /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ApiProperty(identifier=true)
     * @ORM\Column(type="string", length=100)
     * @Groups({"product_read", "translations","media_read", "thirdparty_read", "user_read","locality_read","produitcategorie_read","productlocality_read", "fare_read", "masterParameterValues", "invoice_read","suppliercharge_read", "salesinvoice_read","expectedpayment_read","thirdpartyperson_read"})
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BusinessEntity")
     * @ORM\JoinColumn(name="entity_code", referencedColumnName="code",nullable=false)
     */
    private $entity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rank;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $active;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="parent_value_code", referencedColumnName="code",nullable=true)
     */
    private $parentValue;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterCode")
     * @ORM\JoinColumn(name="parameter_code", referencedColumnName="code",nullable=false)
     * @ORM\JoinColumn(nullable=false)
     */
    private $parameter;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\MasterCode", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="code", referencedColumnName="code",nullable=false)
     */
    private $masterCode;

     /**
     * @Groups({"product_read", "translations","media_read", "thirdparty_read", "user_read","locality_read","produitcategorie_read","productlocality_read","fare_read", "masterParameterValues", "invoice_read","suppliercharge_read","salesinvoice_read","expectedpayment_read","thirdpartyperson_read"})
     */
    private $name;

    public function setName($name)
    {
        $this->getMasterCode()->setName($name);
    }

    public function getName(): string
    {
        return $this->getMasterCode()->getName();
    }

    public function getCode(): ?string
    {
        return $this->code;
    }
    public function getRank(): ?int
    {
        return $this->rank;
    }

    public function setRank(?int $rank): self
    {
        $this->rank = $rank;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }


    public function getEntity(): ?BusinessEntity
    {
        return $this->entity;
    }

    public function setEntity(?BusinessEntity $entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    public function getparentValue(): ?self
    {
        return $this->parentValue;
    }

    public function setparentValue(?self $parentValue): self
    {
        $this->parentValue = $parentValue;

        return $this;
    }

    public function getParameter(): ?MasterCode
    {
        return $this->parameter;
    }

    public function setParameter(?MasterCode $parameter): self
    {
        $this->parameter = $parameter;

        return $this;
    }

    public function getMasterCode(): ?MasterCode
    {
        return $this->masterCode;
    }

    public function setMasterCode(MasterCode $masterCode): self
    {
        $this->masterCode = $masterCode;

        return $this;
    }









}
