<?php

namespace App\Entity;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(formats={"json","jsonld"},
 *              normalizationContext={"groups"={"user_read"}},
 *              denormalizationContext={"groups"={"user_write"}}
 * )
 * @ApiFilter(SearchFilter::class, properties={"userTenants.tenant":"exact", "id":"exact","firstName":"partial","lastName":"partial","email":"exact", "username":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"active"})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"user_read", "user_write", "users","objectmember_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true, nullable=false)
     * @Groups({"user_read", "user_write"})
     */
    private $username;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"user_read", "user_write"})
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Email(message="L'adresse email doit avoir un format valide !")
     * @Groups({"user_read", "user_write"})
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="civility_code", referencedColumnName="code",nullable=false)
     * @Groups({"user_read", "user_write"})
     */
    private $civility;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Le prénom est obligatoire")
     * @Groups({"user_read", "user_write", "users","objectmember_read"})
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"user_read", "user_write", "users","objectmember_read"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="boolean",options={"default" : 1})
     * @Groups({"user_read", "user_write"})
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserTenant", mappedBy="user", orphanRemoval=true)
     */
    private $userTenants;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"user_read", "user_write"})
     */
    private $function;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"user_read", "user_write"})
     */
    private $mobile1PhoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"user_read", "user_write"})
     */
    private $mobile2PhoneNumber;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"user_read", "user_write"})
     */
    private $officePhoneNumber;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_read", "user_write"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"user_read", "user_write"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"user_read", "user_write"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"user_read", "user_write"})
     */
    private $updateUserId;

    public function __construct()
    {
        $this->userTenants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        // $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCivility(): ?MasterParameterValue
    {
        return $this->civility;
    }

    public function setCivility(?MasterParameterValue $civility): self
    {
        $this->civility = $civility;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
    /**
     * @return Collection|UserTenant[]
     */
    public function getUserTenants(): Collection
    {
        return $this->userTenants;
    }

    public function addUserTenant(UserTenant $userTenant): self
    {
        if (!$this->userTenants->contains($userTenant)) {
            $this->userTenants[] = $userTenant;
            $userTenant->setUser($this);
        }

        return $this;
    }

    public function removeUserTenant(UserTenant $userTenant): self
    {
        if ($this->userTenants->contains($userTenant)) {
            $this->userTenants->removeElement($userTenant);
            // set the owning side to null (unless already changed)
            if ($userTenant->getUser() === $this) {
                $userTenant->setUser(null);
            }
        }

        return $this;
    }

    public function getFunction(): ?string
    {
        return $this->function;
    }

    public function setFunction(?string $function): self
    {
        $this->function = $function;

        return $this;
    }

    public function getMobile1PhoneNumber(): ?string
    {
        return $this->mobile1PhoneNumber;
    }

    public function setMobile1PhoneNumber(?string $mobile1PhoneNumber): self
    {
        $this->mobile1PhoneNumber = $mobile1PhoneNumber;

        return $this;
    }

    public function getMobile2PhoneNumber(): ?string
    {
        return $this->mobile2PhoneNumber;
    }

    public function setMobile2PhoneNumber(?string $mobile2PhoneNumber): self
    {
        $this->mobile2PhoneNumber = $mobile2PhoneNumber;

        return $this;
    }

    public function getOfficePhoneNumber(): ?string
    {
        return $this->officePhoneNumber;
    }

    public function setOfficePhoneNumber(?string $officePhoneNumber): self
    {
        $this->officePhoneNumber = $officePhoneNumber;

        return $this;
    }

    
    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }
}
