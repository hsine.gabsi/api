<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RecurringOperationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(formats={"json","jsonld"},
 *              normalizationContext={"groups"={"recurringoperation_read"}}
 * )
 * @ORM\Entity(repositoryClass=RecurringOperationRepository::class)
 */
class RecurringOperation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"recurringoperation_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"recurringoperation_read"})
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"recurringoperation_read"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=MasterParameterValue::class)
     * @ORM\JoinColumn(name="period_code", referencedColumnName="code", nullable=false)
     * @Groups({"recurringoperation_read"})
     */
    private $period;

    /**
     * @ORM\ManyToOne(targetEntity=Tenant::class)
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"recurringoperation_read"})
     */
    private $tenant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPeriod(): ?MasterParameterValue
    {
        return $this->period;
    }

    public function setPeriod(?MasterParameterValue $period): self
    {
        $this->period = $period;

        return $this;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }
}
