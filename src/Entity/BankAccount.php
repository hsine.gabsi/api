<?php

namespace App\Entity;



use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource( formats={"json","jsonld"},
 *                collectionOperations={
 *                    "get"={"normalization_context"={"groups"={"bankaccount_read"}}},
 *                    "post"={"denormalization_context"={"groups"="bankaccount_post"}}
 *              },
 *              itemOperations={
 *                    "get"={"normalization_context"={"groups"={"bankaccount_read"}}},
 *                    "put"={"denormalization_context"={"groups"="bankaccount_put"}}, 
 *                    "delete"
 *              }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\BankAccountRepository")
 * @ApiFilter(SearchFilter::class, properties={"tenant":"exact"})
 */
class BankAccount
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
	 * @Groups({"bankaccount_read", "bankaccount_post", "bankaccount_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     * @Groups({"bankaccount_read", "bankaccount_post"})
     */
    private $tenant;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"bankaccount_read", "bankaccount_post", "bankaccount_put"})
     */
    private $ownerName;

    /**
     * @ORM\Column(name="bank_name",type="string", length=100, nullable=true)
     * @Groups({"bankaccount_read", "bankaccount_post", "bankaccount_put"})
     */
    private $bankName;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"bankaccount_read", "bankaccount_post", "bankaccount_put"})
     */
    private $bankAddress;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"bankaccount_read","bankaccount_post", "bankaccount_put"})
     */
    private $iban;

    /**
     * @ORM\Column(name="bic_code", type="string", length=100, nullable=true)
     * @Groups({"bankaccount_read", "bankaccount_post", "bankaccount_put"})
     */
    private $bic;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     * @Groups({"bankaccount_read", "bankaccount_post", "bankaccount_put"})
     */
    private $currency;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Le champ name est obligatoire")
	 * @Groups({"bankAccounts", "bankaccount_read","bankaccount_post", "bankaccount_put"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     * @Groups({"bankaccount_read", "bankaccount_post", "bankaccount_put"})
     */
    private $cashier;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     * @Groups({"bankaccount_read", "bankaccount_post", "bankaccount_put"})
     */
    private $active;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getOwnerName(): ?string
    {
        return $this->ownerName;
    }

    public function setOwnerName(?string $ownerName): self
    {
        $this->ownerName = $ownerName;

        return $this;
    }

    public function getBankName(): ?string
    {
        return $this->bankName;
    }

    public function setBankName(?string $bankName): self
    {
        $this->bankName = $bankName;

        return $this;
    }


    public function getBankAddress(): ?string
    {
        return $this->bankAddress;
    }

    public function setBankAddress(?string $bankAddress): self
    {
        $this->bankAddress = $bankAddress;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getBic(): ?string
    {
        return $this->bic;
    }

    public function setBic(?string $bic): self
    {
        $this->bic = $bic;

        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCashier(): ?bool
    {
        return $this->cashier;
    }

    public function setCashier(bool $cashier): self
    {
        $this->cashier = $cashier;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
}
