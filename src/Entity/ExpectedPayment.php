<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use App\Annotation\TenantAware;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;



/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json","jsonld"},
 *              collectionOperations={
 *                    "get"={"normalization_context"={"groups"={"expectedpayment_read"}}},
 *                    "post"={"denormalization_context"={"groups"="expectedpayment_post"}}
 *              },
 *              itemOperations={
 *                    "get"={"normalization_context"={"groups"={"expectedpayment_read"}}},
 *                    "put"={"denormalization_context"={"groups"="expectedpayment_put"}}, 
 *                    "delete"
 *              }
 * )
 * @ApiFilter(SearchFilter::class, properties={"id":"exact", "thirdPartyId.id":"exact","thirdPartyName": "exact"})
 * @ApiFilter(DateFilter::class, properties={"date1"})
 * @ApiFilter(BooleanFilter::class, properties={"received"})
 * @ApiFilter(RangeFilter::class, properties={"amount"})
 * @ORM\Entity(repositoryClass="App\Repository\ExpectedPaymentRepository")
 */
class ExpectedPayment
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant")
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"expectedpayment_read", "expectedpayment_post"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="third_party_id", referencedColumnName="id", nullable=false)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $thirdPartyId;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $thirdPartyName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="operator_code", referencedColumnName="code",nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $operatorCode;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $date1;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $date2;

    /**
     * @ORM\Column(type="decimal")
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $amount;

    
     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Currency")
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="guarantee_type", referencedColumnName="code",nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $guaranteeType;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $additionalInformation;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : 0})
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $received;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"expectedpayment_read", "expectedpayment_post", "expectedpayment_put"})
     */
    private $updateUserId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getThirdPartyId(): ?ThirdParty
    {
        return $this->thirdPartyId;
    }

    public function setThirdPartyId(?ThirdParty $thirdPartyId): self
    {
        $this->thirdPartyId = $thirdPartyId;

        return $this;
    }

    public function getThirdPartyName(): ?string
    {
        return $this->thirdPartyName;
    }

    public function setThirdPartyName(?string $thirdPartyName): self
    {
        $this->thirdPartyName = $thirdPartyName;

        return $this;
    }

    public function getOperatorCode(): ?MasterParameterValue
    {
        return $this->operatorCode;
    }

    public function setOperatorCode(?MasterParameterValue $operatorCode): self
    {
        $this->operatorCode = $operatorCode;

        return $this;
    }

    public function getDate1(): ?\DateTimeInterface
    {
        return $this->date1;
    }

    public function setDate1(?\DateTimeInterface $date1): self
    {
        $this->date1 = $date1;

        return $this;
    }

    public function getDate2(): ?\DateTimeInterface
    {
        return $this->date2;
    }

    public function setDate2(?\DateTimeInterface $date2): self
    {
        $this->date2 = $date2;

        return $this;
    }

    public function getAmount(): ?string
    {
        //return $this->amount;
        return $this->priceFormatGet($this->amount);
    }

    public function setAmount(?string $amount): self
    {
        //$this->amount = $amount;
        $this->amount = $this->priceFormatSet($amount);
        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }


    public function getGuaranteeType(): ?MasterParameterValue
    {
        return $this->guaranteeType;
    }

    public function setGuaranteeType(?MasterParameterValue $guaranteeType): self
    {
        $this->guaranteeType = $guaranteeType;

        return $this;
    }

    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    public function setAdditionalInformation(?string $additionalInformation): self
    {
        $this->additionalInformation = $additionalInformation;

        return $this;
    }

    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }

    public function getReceived(): ?bool
    {
        return $this->received;
    }

    public function setReceived(?bool $received): self
    {
        $this->received = $received;

        return $this;
    }

    
    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }
}
