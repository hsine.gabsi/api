<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ApiResource(formats={"json", "jsonld"},
 *              normalizationContext={"groups"={"suppliercharge_read"}},
 *              denormalizationContext={"groups"={"suppliercharge_write"}})
 * @ApiFilter(SearchFilter::class, properties={"supplier":"exact"})
 * @ORM\Entity(repositoryClass="App\Repository\SupplierChargeTypeRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="uniq_supplier_chargeType", columns={"supplier_id", "charge_type_code"})})
 */
class SupplierChargeType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"suppliercharge_read", "suppliercharge_write"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThirdParty")
     * @ORM\JoinColumn(name="supplier_id", nullable=false)
     * @Groups({"suppliercharge_read", "suppliercharge_write"})
     * @Assert\NotBlank(message="Le champ supplier est obligatoire")
     */
    private $supplier;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\MasterParameterValue")
     * @ORM\JoinColumn(name="charge_type_code", referencedColumnName="code", nullable=false)
     * @Groups({"suppliercharge_read", "suppliercharge_write"})
     * @Assert\NotBlank(message="Le champ chargeType est obligatoire")
     */
    private $chargeType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSupplier(): ?ThirdParty
    {
        return $this->supplier;
    }

    public function setSupplier(?ThirdParty $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getChargeType(): ?MasterParameterValue
    {
        return $this->chargeType;
    }

    public function setChargeType(?MasterParameterValue $chargeType): self
    {
        $this->chargeType = $chargeType;

        return $this;
    }
}
