<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\ContractRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Annotation\TenantAware;

/**
 * @TenantAware(tenantFieldName="tenant_code")
 * @ApiResource(formats={"json", "jsonld"},
 *                       collectionOperations={
 *                          "get"={"normalization_context"={"groups"={"contract_read","thirdparties","masterParameterValues"}}},
 *                          "post"={"denormalization_context"={"groups"="contract_post"}}
 *                       },
 *                       itemOperations={
 *                          "get"={"normalization_context"={"groups"={"contract_read","thirdparties","masterParameterValues"}}},
 *                          "put"={"denormalization_context"={"groups"="contract_put"}}, 
 *                          "delete"
 *                       }
 * )
 * @ORM\Entity(repositoryClass=ContractRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"type":"exact","thirdParty":"exact", "name": "partial","periodType":"exact"})
 * @ApiFilter(BooleanFilter::class, properties={"active","sales"})
 */
class Contract
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Tenant::class)
     * @ORM\JoinColumn(name="tenant_code", referencedColumnName="code", nullable=false)
     * @Groups({"contract_read", "contract_post"})
     * @Assert\NotBlank(message="Le champ tenant est obligatoire")
     */
    private $tenant;

    /**
     * @ORM\ManyToOne(targetEntity=ThirdParty::class)
     * @ORM\JoinColumn(name="third_party_id", nullable=false)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     * @Assert\NotBlank(message="Le champ thirdParty est obligatoire")
     */
    private $thirdParty;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $sales;

    /**
     * @ORM\ManyToOne(targetEntity=MasterParameterValue::class)
     * @ORM\JoinColumn(name="type_code", referencedColumnName="code", nullable=false)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     * @Assert\NotBlank(message="Le champ type est obligatoire")
     */
    private $type;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $endDate;

    /**
     * @ORM\ManyToOne(targetEntity=MasterParameterValue::class)
     * @ORM\JoinColumn(name="price_type_code", referencedColumnName="code", nullable=false)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     * @Assert\NotBlank(message="Le champ priceType est obligatoire")
     */
    private $priceType;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     * @Assert\NotBlank(message="Le champ name est obligatoire")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @ORM\JoinColumn(name="product_id", nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=MasterParameterValue::class)
     * @ORM\JoinColumn(name="period_type_code", referencedColumnName="code", nullable=false)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     * @Assert\NotBlank(message="Le champ periodType est obligatoire")
     */
    private $periodType;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class)
     * @ORM\JoinColumn(name="currency_code", referencedColumnName="code", nullable=false)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     * @Assert\NotBlank(message="Le champ currency est obligatoire")
     */
    private $currency;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default" : 1})
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $productName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TenantParameterValue")
     * @ORM\JoinColumn(name="charge_group_id", referencedColumnName="id", nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $chargeGroup;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $creationDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="creation_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $creationUserId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $updateDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="update_user_id", referencedColumnName="id", nullable=true)
     * @Groups({"contract_read", "contract_post", "contract_put"})
     */
    private $updateUserId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTenant(): ?Tenant
    {
        return $this->tenant;
    }

    public function setTenant(?Tenant $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getThirdParty(): ?ThirdParty
    {
        return $this->thirdParty;
    }

    public function setThirdParty(?ThirdParty $thirdParty): self
    {
        $this->thirdParty = $thirdParty;

        return $this;
    }

    public function getSales(): ?bool
    {
        return $this->sales;
    }

    public function setSales(bool $sales): self
    {
        $this->sales = $sales;

        return $this;
    }

    public function getType(): ?MasterParameterValue
    {
        return $this->type;
    }

    public function setType(?MasterParameterValue $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getPriceType(): ?MasterParameterValue
    {
        return $this->priceType;
    }

    public function setPriceType(?MasterParameterValue $priceType): self
    {
        $this->priceType = $priceType;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getPeriodType(): ?MasterParameterValue
    {
        return $this->periodType;
    }

    public function setPeriodType(?MasterParameterValue $periodType): self
    {
        $this->periodType = $periodType;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->priceFormatGet($this->price);
    }

    public function setPrice(?string $price): self
    {
        $this->price = $this->priceFormatSet($price);
        return $this;
    }

    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->productName;
    }

    public function setProductName(?string $productName): self
    {
        $this->productName = $productName;

        return $this;
    }

    private function priceFormatGet(?string $price):?string
    {
        if (is_numeric($price)) {
            $price = $price / pow(10, $this->currency->getDecimals());
            return number_format($price, $this->currency->getDecimals(), '.', ' ');
        } else {
            return $price;
        }
    }

    private function priceFormatSet($price)
    {
        $price = str_replace(' ', '', $price);
        $price = str_replace(',', '.', $price);

        if (is_numeric($price)) {
            $price = $price * pow(10, $this->currency->getDecimals());
            $pos = strpos((string)$price, ".");
            if ($pos > 0) {
                $price = substr($price, 0, $pos);
            }
        } else {
            return NULL;
        }
        return $price;
    }

    public function getChargeGroup(): ?TenantParameterValue
    {
        return $this->chargeGroup;
    }

    public function setChargeGroup(?TenantParameterValue $chargeGroup): self
    {
        $this->chargeGroup = $chargeGroup;

        return $this;
    }

    
    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(?\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getCreationUserId(): ?User
    {
        return $this->creationUserId;
    }

    public function setCreationUserId(?User $creationUserId): self
    {
        $this->creationUserId = $creationUserId;

        return $this;
    }

    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->updateDate;
    }

    public function setUpdateDate(?\DateTimeInterface $updateDate): self
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    public function getUpdateUserId(): ?User
    {
        return $this->updateUserId;
    }

    public function setUpdateUserId(?User $updateUserId): self
    {
        $this->updateUserId = $updateUserId;

        return $this;
    }
}
