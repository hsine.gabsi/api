<?php
namespace App\Services;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class TokenInformation
 * @package App\Services
 */
class TokenInformation extends AbstractController
{
    private $jwtManager;
    private $tokenStorage;
    public function __construct(TokenStorageInterface $tokenStorage, JWTTokenManagerInterface $jwtManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->jwtManager = $jwtManager;
    }

    public function getTenantCode()
    {
        $decodedJwtToken = $this->jwtManager->decode($this->tokenStorage->getToken()); 
        $tenant_code = $decodedJwtToken['tenant'];

        return $tenant_code;
    }
}