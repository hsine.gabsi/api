# #!/bin/sh


WORKDIR="/usr/share/nginx/html/"
# Install php deps
install_phpanddeps(){
    echo "Install PHP deps"
    # yum -y install zip git unzip openssl 
    # composer --version
    # apt install
    mkdir /run/php
    apt update
    apt install -y apt-utils
    apt install -y php php-pdo php-mysql php-gd php-curl php-pear php-mbstring php-zip curl git unzip acl openssl gnupg
    apt install -y php-fpm
    # apt-get install -y  \
    # gnupg \
    # g++ \
    # procps \
    # openssl \
    # git \
    # unzip \
    # zlib1g-dev \
    # libzip-dev \
    # libfreetype6-dev \
    # libpng-dev \
    # libjpeg-dev \
    # libicu-dev  \
    # libonig-dev \
    # libxslt1-dev \
    # acl \
    # && echo 'alias sf="php bin/console"' >> ~/.bashrc

    # systemct restart nginx
    # Restart services
    /etc/init.d/php*-fpm restart 
    service nginx reload
}
# Install composer DEPS
install_composer_deps(){
echo "##### INSTALL COMPOSER DEPS ##########"
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

echo "Going to $WORKDIR "
cd $WORKDIR 
# ls $WORKDIR
# ls "$WORKDIR/bin"
#Installer la version 1.10
composer self-update --1

#composer install
composer install
echo "#### FIN INSTALL DEPS #####"
# ls $WORKDIR
}

# Install Node JS
install_nodejs_deps(){
cd  $WORKDIR
echo "##### INSTALL NODEJS ##########"
curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt-get install -y nodejs
npm install
}
# cd $WORKDIR
# echo "We are in "
# echo $(pwd)
install_phpanddeps
install_composer_deps
# /bin/sh /entrypoint.sh
#install_nodejs_deps
