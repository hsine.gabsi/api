FROM php:7.4-fpm-buster
RUN apt-get update && apt-get install -y nginx curl  apt-utils \
    gnupg \
    g++ \
    procps \
    openssl \
    git \
    unzip \
    zlib1g-dev \
    libzip-dev \
    libfreetype6-dev \
    libpng-dev \
    libjpeg-dev \
    libicu-dev  \
    libonig-dev \
    libxslt1-dev \
    acl 
#php7.4-pdo php7.4-mbstring php7.4-xml php7.4-xsl php7.4-zip php7.4-mysql
RUN docker-php-ext-install \
    pdo pdo_mysql zip xsl gd intl opcache exif mbstring

  
 
COPY nginx.conf /etc/nginx/sites-enabled/app.conf 

#copy projet
RUN mkdir -p /var/www/app/ 
COPY . /var/www/app/
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN cd /var/www/app/ && composer self-update --1 && composer install

COPY entry-nginx.sh /usr/bin/entry_nginx

RUN chmod +x /usr/bin/entry_nginx

CMD entry_nginx; php-fpm
 
EXPOSE 8080
